addSbtPlugin("com.typesafe.sbt" % "sbt-multi-jvm" % "0.4.0")
// Cannot use this plugin on Mac, because it starts the app on another thread and then GLFW crashes
// addSbtPlugin("io.kamon" % "sbt-kanela-runner" % "2.0.2")
