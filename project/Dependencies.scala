import sbt._

object Dependencies {
  val akkaVersion = "2.5.23"
  val akkaHttpVersion = "10.1.10"
  val akkaStreamVersion = akkaVersion
  val kamonVersion = "2.0.4"
  val kamonPrometheusVersion = "2.0.1"
  val scalaMockVersion = "4.4.0"

  lazy val akkaDependencies = Seq(
    "com.typesafe.akka" %% "akka-actor-typed"         % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-typed"       % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-metrics"     % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools"       % akkaVersion,
    "com.typesafe.akka" %% "akka-http"                % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-multi-node-testkit"  % akkaVersion,
    "com.typesafe.akka" %% "akka-remote"              % akkaVersion,
    "com.typesafe.akka" %% "akka-stream-typed"        % akkaStreamVersion,
    "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test
  )

  lazy val otherDependencies = Seq(
    "ch.qos.logback"               % "logback-classic"  % "1.2.3",
    "com.typesafe.scala-logging"  %% "scala-logging"    % "3.9.2",
    "io.kamon"                    %% "kamon-bundle"     % kamonVersion,
    "io.kamon"                    %% "kamon-prometheus" % kamonPrometheusVersion,
    "io.kamon"                     % "sigar-loader"     % "1.6.6-rev002",
    "org.scalamock"               %% "scalamock"        % scalaMockVersion % Test,
    "org.scalatest"               %% "scalatest"        % "3.0.5" % Test
  )

  lazy val graphicDependencies = Seq(
    //"com.github.jpbetz" % "subspace" % "0.1.0"
  )

  lazy val osName = System.getProperty("os.name").toLowerCase
  
  lazy val lwjglDependencies = {
    val version = "3.2.2"
    //val os = "linux", "macos", or "windows"
    val os = osName match {
      case w if w.contains("windows") => "windows"
      case m if m.contains("mac os") => "macos"
      case _ => "linux"
    }

    Seq(
      "lwjgl",
      "lwjgl-assimp",
      "lwjgl-glfw",
      "lwjgl-opengl",
      "lwjgl-stb"
    ).flatMap { mod =>
      Seq(
        "org.lwjgl" % mod % version,
        "org.lwjgl" % mod % version classifier s"natives-$os"
      )
    }
  }
}
