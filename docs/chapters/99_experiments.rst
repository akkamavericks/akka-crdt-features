Experiments
============
Dummy page to show how stuff's done in Sphinx

.. note::
    This page is very much a work in progress

Typically *italics* and **bold** formatting is supported. Inline code formatting is done with :code:`code backticks`,
and since I defined a scala role, :scala:`import scala.duration._ //scala highlighting` also works.
Bash citations are similar :bash:`export PATH=$PATH:~/bin`


.. code-block:: bash

    export PATH=$PATH:~/bin


.. uml::

   @startuml
   user -> (use PlantUML)

   note left of user
      Hello!
   end note
   @enduml

.. include:: ../data/dummy.scala
    :start-after: part3_start
    :end-before: part3_end

Real example

.. include:: /src/main/scala/nl/about42/scalagl/streaming/StateStream.scala
    :start-after: doc_start
    :end-before: doc_end

Some text interspersed with code examples

.. literalinclude:: /src/main/scala/nl/about42/akkamavericks/cluster/ActorIndex.scala
    :language: scala
    :lines: 1-10
    :emphasize-lines: 5-6
    :caption:

And then the text continues

.. literalinclude:: ../data/dummy.scala
    :language: scala
    :start-after: part1_start
    :end-before: part1_end

Second part of that file:

.. literalinclude:: ../data/dummy.scala
    :language: scala
    :start-after: part2_start
    :end-before: part2_end

And the complete file:

.. literalinclude:: ../data/dummy.scala
    :language: scala
    :linenos:
    :caption:
