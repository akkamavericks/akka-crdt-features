Introduction
============
The AKKA CRDT Features repository shows different ways you can use the AKKA ecosystem and
more specifically the CRDT Features that are available for sharing eventually consistent
distributed state.

.. note::
    This document is very much a work in progress

In addition, I am exploring all kinds of ways to interact with the system, and ways to visualize the actor
behavior.