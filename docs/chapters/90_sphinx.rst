Sphinx and Read the Docs stuff
==============================

Inline syntax highlighting
--------------------------

To get inline code highlighting for a specific language, you can define a role and then use it to highlight
parts in that specific language, like Scala: :scala:`import scala.concurrent.duration._`

You need some tweaks to the docutils configuration, which is done by adding a :code:`docutils.conf` file to your
project, with the following content:

.. code-block:: ini
    :caption: docutils.conf

    [restructuredtext parser]
    syntax_highlight = short


Defining the role:

.. code-block:: rst

    .. role:: scala(code)
        :language: scala
        :class: highlight

And then using the role in a text:

.. code-block:: rst

    Some text :scala:`import scala.concurrent.duration._`

Including source code snippets
------------------------------

Since code changes over time, referring to fixed line numbers in source files creates a nightmare in keeping the
docs in sync. My solution: label/tag the interesting parts in the code, and include them using the following:

.. code-block:: rst

    .. include:: /src/main/scala/nl/about42/scalagl/streaming/StateStream.scala
        :start-after: doc_start
        :end-before: doc_end

And if this file contains a nice comment somewhere like this:

.. literalinclude:: /src/main/scala/nl/about42/scalagl/streaming/StateStream.scala
    :start-after: example_start
    :end-before: example_end
    :language: scala
    :caption:

That will be rendered in the documentation like:

.. include:: /src/main/scala/nl/about42/scalagl/streaming/StateStream.scala
    :start-after: doc_start
    :end-before: doc_end
