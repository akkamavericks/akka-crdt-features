Graphics in Scala using OpenGL
==============================

The application will create a graphical window if one of the nodes includes the :code:`gui` role in its :code:`akka.cluster.roles`.

.. literalinclude:: /src/main/scala/nl/about42/akkamavericks/cluster/ClusterCrdtApp.scala
    :start-after: doc_start
    :end-before: doc_end
    :language: scala
    :caption:
    :lineno-match:

.. literalinclude:: /src/main/scala/nl/about42/akkamavericks/cluster/ClusterCrdtApp.scala
    :start-after: doc2_start
    :end-before: doc2_end
    :language: scala
    :lineno-match:

.. include:: /src/main/scala/nl/about42/scalagl/ui/Handler.scala
    :start-after: doc_start
    :end-before: doc_end

.. include:: ../data/graphics_overview.puml

Streams, Actors, and main threads
---------------------------------
Since OpenGL rendering is restricted to a single thread (at least on some platforms), care must be taken when working
in a multi-threaded environment like Akka. To utilise the scalability and performance of Akka Actors and Streams, I
have created some helpers to connect the render loop that runs on the main thread to the rest of the application.

The drawing actions are collected asynchronously, and are stored into a map of drawing instructions, that will then
be used by the render loop to actually draw a frame.

.. include:: /src/main/scala/nl/about42/scalagl/streaming/WorkerCoordinator.scala
    :start-after: doc_start
    :end-before: doc_end

.. include:: /src/main/scala/nl/about42/scalagl/streaming/FrameWorker.scala
    :start-after: doc_start
    :end-before: doc_end
