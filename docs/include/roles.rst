
.. role:: scala(code)
   :language: scala
   :class: highlight

.. role:: bash(code)
   :language: bash
   :class: highlight
