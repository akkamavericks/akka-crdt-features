/*
This is a dummy scala file to experiment with Sphinx includes.
 */

import scala.concurrent.duration._

object NothingToSeeHere {
  // part1_start
  val quiteInteresting: Int = 10
  // part1_end
}

// part2_start
case class Seconds(count: Long)
case class Hours(count: Long)
/* part2_end
 */

/*
part3_start

.. uml::

  @startuml
      skinparam componentStyle uml2

      () "source: SourceQueueWithComplete[Frame]" as source
      () "sink: SinkQueueWithCancel[State]" as sink

      source -- [StateStream]
      sink -- [StateStream]

      () "scatterActor: ActorRef[ScatterGatherProtocol]" as scatteractor

      [ActorStateStream] -up- [StateStream] : extends

      scatteractor -up- [ActorStateStream]

  @enduml

part3_end
 */
