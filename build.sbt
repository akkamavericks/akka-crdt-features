import com.typesafe.sbt.SbtMultiJvm.multiJvmSettings
import com.typesafe.sbt.SbtMultiJvm.MultiJvmKeys.MultiJvm

import Dependencies._

lazy val osSpecificJavaOptions = {
  val result = osName match {
    case macos if macos.contains("mac os") => Seq("-XstartOnFirstThread")
    case _ => Seq()
  }
  System.out.println(s"OS Name is '$osName' gives options: $result")
  result
}


lazy val `akka-crdt-features` = project
  .in(file("."))
  .settings(multiJvmSettings: _*)
  .settings(
    organization := "nl.about42.akkamavericks",
    scalaVersion := "2.12.8",
    Compile / scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked", "-Xlog-reflective-calls", "-Xlint"),
    Compile / javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation"),
    run / javaOptions ++= Seq("-Xms128m", "-Xmx1024m") ++ osSpecificJavaOptions ++ Seq("-Djava.library.path=./target/native"),
    //run / javaOptions ++= Seq("-agentlib:hprof=heap=dump,format=b"),
    libraryDependencies ++= akkaDependencies ++ otherDependencies ++ lwjglDependencies ++ graphicDependencies,
    run / fork := true,
    Compile / run / mainClass := Some("nl.about42.akkamavericks.cluster.ClusterCrdtApp"),
    // disable parallel tests
    Test / parallelExecution := false,
    licenses := Seq(("CC BY 4.0", url("https://creativecommons.org/licenses/by/4.0/"))),
    commands ++= Seq(runNodeCommand),
    Global / cancelable := true
  )
  .configs (MultiJvm)

// setup command to start a single node, using separate folder for the extracted libsigar
// assumes sane node names that can be used as folder names
val runNodeAction: (State, String) => State = { (state, nodeName) =>
  val stateWithTempOptions = Project.extract(state).appendWithSession(
    Seq(
      run / javaOptions := (run / javaOptions).value.filterNot(s => s.startsWith("-Djava.library.path=")) :+ s"-Djava.library.path=./target/native/$nodeName"
    ),
    state
  )
  val (newState, _) = Project.extract(stateWithTempOptions).runInputTask(Compile / run, s" $nodeName", stateWithTempOptions)
  newState
}

val runNodeCommand: Command = Command.single("runNode", ("runNode", "run main class with a [node] argument"), "Starts JVM for a specific node in the configuration")(runNodeAction)
