# AKKA CRDT Features

## Introduction
A project to show/explore the features provided by the Distributed Data module in AKKA Cluster.

Also provides -- or may provide in the near future :D -- nice examples of how one can

* Configure heterogeneous cluster (different roles for different members)
* Monitor cluster and actor behaviour
    * Deferred startup of actor logic (for example to wait for complete cluster startup before binding to ports)
* Connect to Kafka for reporting on cluster/actor health and status (future goal)
* Integrate with monitoring/tracing tools using Kamon
* Use OpenGL to render 3D graphics that eventually will visualise what is actually happening in the cluster(s)

The project also experiments with ways to create and maintain documentation. Some documentation
is published on Read The Docs. You can visit it on https://akka-crdt-features.readthedocs.io

Enjoy,

Jean-Marc van Leerdam.

[(CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)

## Running the example(s)
When run without arguments, the application will start nodes as given in the `nodes.default` setting. The nodes will be
separate actor systems inside one JVM, that will operate as a cluster, each representing a node as configured in the
`nodes` section of the application.conf.

Example:
```hocon
nodes {
  default = [ node1, node2, node3 ]
  node1 {
    akka.cluster.metrics.native-library-extract-folder=${user.dir}/target/native/node1
    akka.cluster.roles = [ crdt ]
    akka.remote.artery.canonical.port = 2551
  }
  node2 {
    akka.cluster.metrics.native-library-extract-folder=${user.dir}/target/native/node2
    akka.cluster.roles = [ crdt, rest ]
    akka.remote.artery.canonical.port = 2552
  }
  node3 {
    akka.cluster.metrics.native-library-extract-folder=${user.dir}/target/native/node3
    akka.cluster.roles = [ rest ]
    akka.remote.artery.canonical.port = 2553
  }
}
```

* To run all default nodes in one JVM:

    `sbt run` 

* To run just node1:

    `sbt "runNode node1"`

Note: When defining nodes, make sure you specify the correct `akka.cluster.metrics.native-library-extract-folder`. The
`runNode` command will set the classpath to `./target/native/{nodeName}`, so match that folder in the application.conf.
This is needed because the libsigar library cannot be shared between JVM instances.

## Kamon integration
Kamon is being used to extract metrics. When the application is running, there is a status page available on 
http://localhost:5266 (default port, node2 and node3 use 5267 and 5268)
The prometheus endpoint is at http://localhost:9095 (9096 and 9097 for node2 and node 3)

### Grafana/prometheus server
To visualise the metrics, start a Grafana/Prometheus Docker container by using the `docker-compose up` when in the
`./docker` folder. By default only node1 will be scraped, to scrape all 3 nodes (when run separately), change the
appropriate target_groups entry in the `prometheus.yml`:

```yaml
  - job_name: 'akka-crdt'
    target_groups:
      - targets: ['host.docker.internal:9095']
      #- targets: ['host.docker.internal:9095', 'host.docker.internal:9096', 'host.docker.internal:9097']

```

Once you have the cluster running for several minutes, the metrics should be visible on the Prometheus scrape endpoint
(`http://localhost:9095`), and Grafana should also see them through the Prometheus server in the Docker container
(`http://localhost:9090`). An example dashboard to visualise some of the PingSent/PingReceived timings is given in the
`SampleDashboard.json`

## Documentation
I am also experimenting with Sphinx as a documentation option.

### Setup
* Install theme using
    * `pip3 install sphinx_rtd_theme`
    * Note: if you do not yet have pip3, run `sudo apt install python3-pip`
* Install plantUml support using
    * `pip3 install sphinxcontrib-plantuml`
* Download plantuml.jar from http://plantuml.com/download
* Put the jar in a folder (such as ~/bin/lib/plantuml.jar) and create a shell script on your path (name it `plantuml`) to call it:

```bash
#!/usr/bin/env bash
java -jar ~/bin/lib/plantuml.jar $@
```
* Install GraphViz
    * `sudo apt install graphviz`

### Generating the docs locally
From the project root, run `make clean html`
Output should end up in ./target/docs


## Resources
Naturally, most of this stuff is not invented by me on my own. In addition to the experience I built during my own trials and errors, it is based on a 
multitude of web resources, that were found via [Stack Overflow](https://stackoverflow.com) or through our friendly search engine [Google](https://google.com). 
The most relevant web resources are listed here.

* Akka by Lightbend - https://akka.io/docs/
* Alvin Alexander Scala cookbook - https://alvinalexander.com/scala/scala-programming-cookbook-recipes-faqs
* Using HPROF to analyze memory usage - https://docs.oracle.com/javase/7/docs/technotes/samples/hprof.html
* VisualVM - https://visualvm.github.io/download.html

SO Questions and answers that helped me

* https://stackoverflow.com/questions/54503506/how-can-i-set-javaoptions-in-a-custom-sbt-command
