package nl.about42.scalagl.math

import org.scalatest.{Matchers, WordSpecLike}

class Vec4Test extends WordSpecLike with Matchers {

  "Vec4Test" should {

    "lerp" in {
      val v1 = Vec4(1, 2, 3, 4)
      val v2 = Vec4(5, 4, 3, -8)

      val v3 = Vec4.lerp(v1, v2, 0.5f)
      val v4 = Vec4.lerp(v1, v2, 0.25f)

      v3.r shouldBe 3.0f
      v3.g shouldBe 3.0f
      v3.b shouldBe 3.0f
      v3.a shouldBe -2f

      v4.r shouldBe 2.0f
      v4.g shouldBe 2.5f
      v4.b shouldBe 3.0f
      v4.a shouldBe 1f

    }

  }
}
