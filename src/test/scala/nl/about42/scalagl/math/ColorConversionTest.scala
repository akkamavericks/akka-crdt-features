package nl.about42.scalagl.math

import org.scalatest.{Matchers, WordSpecLike}

class ColorConversionTest extends WordSpecLike with Matchers with ColorConversions {

  "Color conversion" should {
    "Have sufficient precision" in {
      (0 to 255).foreach(i => {
        val b = i.toByte
        val f = b2f(b)
        val b2 = f2b(f)
        b shouldBe b2

      })

      (0 to 1000).foreach( i => {
        val f = i / 1000f
        val b = f2b(f)
        val f2 = b2f(b)

        f shouldBe f2 +- 1f/255
      })
    }

    "Put correct bits in int" in {
      val r = getBits(1, 24)

      f"$r%08X" shouldBe "FF000000"

      val g = getBits(0.75f, 16)

      f"$g%08X" shouldBe "00BF0000"
    }

  }
}
