package nl.about42.scalagl

import nl.about42.scalagl.math.{Vec3, VectorOperations}
import org.scalatest.{Matchers, WordSpecLike}

class ScalaGLTest extends WordSpecLike with VectorOperations with Matchers {

  val ALLOWED_FLOAT_ERROR = 2e-7f

  def shouldBeEqual(v1: Vec3, v2: Vec3) = {
    Math.abs(v1.x - v2.x) < ALLOWED_FLOAT_ERROR &&
    Math.abs(v1.y - v2.y) < ALLOWED_FLOAT_ERROR &&
    Math.abs(v1.z - v2.z) < ALLOWED_FLOAT_ERROR
  }
  "HelperTest" should {

    "dot" in {
      val v1 = Vec3(1, 0, 0)
      val v2 = Vec3(0, 1, 0)
      val v3 = Vec3(1, 2, 3)

      v1 * v2 shouldBe 0.0f
      v1 * v1 shouldBe 1.0f
      v3 * v3 shouldBe 14.0f
    }

    "cross" in {
      val v1 = Vec3(1, 0, 0)
      val v2 = Vec3(0, 1, 0)

      shouldBeEqual(v1 X v2, Vec3(0, 0, 1))
    }

    "lookAt" in {

    }

    "diff" in {

    }

    "epsilon" in {

    }

    "normalize" in {
      val v1 = Vec3(2, 0, 0)

      normalized(v1).lengthSquared shouldBe 1.0f

      val v2 = Vec3(7, 4, -5)

      val v3 = normalized(v2)

      v3.lengthSquared shouldBe 1.0f

      val v4 = v3 * v2.length

      (v4 - v2).lengthSquared shouldBe 0.0f +- ALLOWED_FLOAT_ERROR

      val v5 = Vec3(1, 2, 3)
      val v6 = Vec3(v5)
      v6.normalize()

      v5.length shouldBe Math.sqrt(14).toFloat +- ALLOWED_FLOAT_ERROR
      v6.length shouldBe 1.0f +- ALLOWED_FLOAT_ERROR
    }

    "perspective" in {

    }

    "nonZero" in {

    }

  }
}
