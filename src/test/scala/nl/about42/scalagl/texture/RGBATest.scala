package nl.about42.scalagl.texture

import nl.about42.scalagl.math
import nl.about42.scalagl.texture.RGBA.{ALPHA, BLUE, GREEN, RED}
import org.scalatest.{Matchers, WordSpecLike}

class RGBATest extends WordSpecLike with Matchers {
  "RGBA" should {
    "Give correct byte values" in {
      val input = math.Vec4(1, 0.5f, 0.0f, 0.25f)
      val col = RGBA(input)

      col.toString shouldBe "FF800040"
      col(RED) shouldBe 255.toByte
      col(GREEN) shouldBe 128.toByte
      col(BLUE) shouldBe 0.toByte
      col(ALPHA) shouldBe 64.toByte

      val c2 = col.toVec4()
      val c3 = RGBA(c2).toVec4()
      c2.x - c3.r shouldBe 0
      c2.y - c3.g shouldBe 0
      c2.z - c3.b shouldBe 0
      c2.w - c3.a shouldBe 0
    }

    "Update correct parts" in {
      val col = RGBA(0xFF998877)

      col.toString shouldBe "FF998877"

      col(RED) = 0.5f

      col.toString shouldBe "80998877"

      col(GREEN) = 0

      col.toString shouldBe "80008877"

      col(BLUE) = 1.0f

      col.toString shouldBe "8000FF77"

      col(ALPHA) = 0.25f

      col.toString shouldBe "8000FF40"

    }
  }

}
