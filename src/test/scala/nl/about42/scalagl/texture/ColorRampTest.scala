package nl.about42.scalagl.texture

import nl.about42.scalagl.math
import nl.about42.scalagl.math.Vec4
import org.scalatest.{Matchers, WordSpecLike}

class ColorRampTest extends WordSpecLike with Matchers {
 "ColorRamp" should {
   "Produce grayscale by default" in {
     val ramp = ColorRamp()

     val col = ramp(0.5f)

     col.r shouldBe 0.5f
     col.g shouldBe 0.5f
     col.b shouldBe 0.5f
     col.a shouldBe 1f

     val col2 = ramp(2)
     col2.r shouldBe 1f
   }

   "Interpolate colors" in {
     val ramp = ColorRamp(List(
       (0f, math.Vec4(1, 0, 0, 1)),
       (1f, math.Vec4(0, 1, 0, 1)),
       (2f, math.Vec4(0, 0, 1, 1)),
       (3f, math.Vec4(1, 0, 0, 1))))

     RGBA(ramp(1)).toString shouldBe "00FF00FF"
     RGBA(ramp(2.5f)).toString shouldBe "800080FF"
     RGBA(ramp(0.75f)).toString shouldBe "40BF00FF"
   }

   "Create from colors" in {
     val ramp = ColorRamp(Seq(
       Vec4(1, 0, 0, 1),
       Vec4(1, 1, 0, 1),
       Vec4(0, 1, 0, 1),
       Vec4(0, 1, 1, 1),
       Vec4(0, 0, 1, 1),
       Vec4(1, 0, 1, 1),
       Vec4(1, 0, 0, 1)
     ))
     RGBA(ramp(0.5f)).toString shouldBe "00FFFFFF"
     RGBA(ramp(0.2f)).toString shouldBe "CCFF00FF"
   }
 }
}
