package nl.about42.scalagl.streaming

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.actor.typed.{ActorSystem, Behavior}
import akka.stream.scaladsl.Flow
import akka.stream.typed.scaladsl.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import nl.about42
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl.math.Vec3
import nl.about42.scalagl.streaming.StateStream.{Frame, State, StateFlow, GLAction}
import nl.about42.scalagl.{Window, math}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.Await
import scala.concurrent.duration._

class StateStreamTest extends WordSpecLike with Matchers with MockFactory with LazyLogging {

  class TestSupervisor(context: ActorContext[Nothing]) extends AbstractBehavior[Nothing] {
    override def onMessage(msg: Nothing): Behavior[Nothing] = {
      Behaviors.unhandled
    }
  }
  def dummy: Behavior[Nothing] = Behaviors.setup[Nothing](context => new TestSupervisor(context))

  var flowCount = 0
  def dummyAction(): GLAction = (w: Window, c: Camera, frame: Frame) => {
    System.out.println(s"dummyAction runs, on ${Thread.currentThread().getName}")
    flowCount += 1
  }

  def testFlow: StateFlow = Flow.fromFunction((frame: Frame) => {
    System.out.println(s"testFLow runs, on ${Thread.currentThread().getName}")
    State(frame, Map(about42.scalagl.DRAW_ORDER_PREPROCESS -> List(dummyAction())))
  })

  "StateStream" should {
    "Run a single flow" in {
      val system = ActorSystem[Nothing](dummy, "testSystem", ConfigFactory.parseResources("singlesystem.conf"))
      val mat = ActorMaterializer()(system)
      val streamConnectors = StateStream.createSingleStageStream(testFlow)(mat)

      val frame = Frame(12345f, 0.1f, 1)

      streamConnectors.source.offer(frame)

      val state = Await.result(streamConnectors.sink.pull(), 1.second)

      val actions = state.get.actions


      val w = new Window(1, null, "")
      val c = new Camera(math.Vec3(1, 0, 0), Vec3(0, 1, 0), Vec3(1, 0, 0))
      flowCount = 0
      actions(0).foreach(_(w, c, frame))

      actions.keys.size shouldBe 1
      actions.keys.head shouldBe 0
      flowCount shouldBe 1
    }

    "Run a combined flow" in {

    }
  }
}
