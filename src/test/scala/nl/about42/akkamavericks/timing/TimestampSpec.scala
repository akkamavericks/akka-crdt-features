package nl.about42.akkamavericks.timing

import org.scalatest.WordSpec
import org.scalatest.GivenWhenThen

class TimestampSpec extends WordSpec with GivenWhenThen{

  "Created timestamps"  can {
    "be compared" in {
      val t1 = Timestamp.now
      val t2 = Timestamp.now
      val t3 = Timestamp.now
      val t4 = Timestamp.now
      val t5 = Timestamp.now
      val t6 = Timestamp.now

      val d1 = t6.diff(t1)
      info(s"Nanodifference between t6 and t1 is ${d1.nanos}")

      Given(s"Some timestamps: ${t1.nanoTime} ${t2.nanoTime} ${t3.nanoTime} ${t4.nanoTime} ${t5.nanoTime} ${t6.nanoTime}")
      When("comparing them")
      Then("they should be ordered")
      assert(t1.before(t2) && t2.before(t3) && t3.before(t4) && t4.before(t5) && t5.before(t6) && t6.after(t1))
    }
  }
}

