package nl.about42.akkamavericks.timing

import org.scalatest._

class TimedEventSpec extends WordSpec {
  "An Event" can {
    val e1 = PingSent(1, Timestamp.now, Ping.create(1, 1))
    // wait about 250 ms
    Thread.sleep(247)
    val e2 = PingReceived(1, Timestamp.now, Ping.create(1, 2))
    val diff = e2.timestamp.diff(e1.timestamp)
    s"have difference (${diff.millis}, ${diff.nanos})" in {
      info(s"first:  ${e1.asString}")
      info(s"second: ${e2.asString}")
      //nothing
      assert(true)
    }
    "be compared to another event" in {

      withClue("about 250 milliseconds") {
        assert( diff.millis > 247 && diff.millis < 253  )
      }
      withClue(s"about a 250 million nanoseconds") {
        assert(diff.nanos >  247000000 && diff.nanos < 253000000)
      }
    }
  }
}
