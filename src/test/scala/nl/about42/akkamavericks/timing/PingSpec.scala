package nl.about42.akkamavericks.timing

import org.scalatest.{Matchers, WordSpec}

class PingSpec extends WordSpec with Matchers {

  "Ping objects" can {
    "serialize and deserialize" in {
      val p1 = Ping.create(1, 1)
      val pStr = p1.asString
      val p2 = Ping.fromString(pStr)

      info(s"Created ping ${pStr}")
      p1.nodeId shouldBe p2.nodeId
      p1.value shouldBe p2.value
      p1.timestamp.wallTime shouldBe p2.timestamp.wallTime
      p1.timestamp.nanoTime shouldBe p2.timestamp.nanoTime
    }
  }
}
