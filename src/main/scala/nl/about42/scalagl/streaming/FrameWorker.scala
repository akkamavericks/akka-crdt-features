package nl.about42.scalagl.streaming

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl
import nl.about42.scalagl.Window
import nl.about42.scalagl.streaming.WorkerCoordinator.{RemoveWorker, WorkerCoordinator}
import nl.about42.scalagl.streaming.StateStream.{Frame, GLAction, State}
import nl.about42.scalagl.ui.Drawable

import scala.concurrent.duration
import scala.concurrent.duration.Duration

/*
doc_start

  The :code:`FrameWorkerActor` / :code:`FrameWorkerBehavior` defines an actor-wrapper for some unit of work that
  needs to be executed for each frame. One use case is the wrapping of a :code:`Drawable`, allowing dynamic
  modifications to the scene that is being rendered.

  The :code:`FrameWorker.drawableWorker` method will return the appropriate behavior that can then be sent to
  the :code:`WorkerCoordinator`. By default the FrameWorkerActor wants to start immediately and live forever,
  but the user can pass a desired time-of-birth and a desired lifetime.

  .. uml::

    actor Context
    participant WorkerCoordinator as WC
    participant FrameWorker as FW
    participant Drawable as D
    Context -> FW: TimeToLiveInfo(lifetime)
    WC -> FW: HandleFrame(frame, replyTo)
    activate FW
    FW -> D: update(wallTime, frameCount, delta)
    FW -> WC: WorkerData(frame, state)
    note over FW: if lifetime expired
    FW -> WC: RemoveWorker(self)
    deactivate FW
    WC -> FW: Stop

    legend
      The returned WorkerData contains a reference to :code:`Drawable.render` that can be executed on the main thread
    end legend
doc_end
 */
object FrameWorker extends LazyLogging {
  type FrameWorkerActor = ActorRef[Protocol]
  type FrameWorkerBehavior = Behavior[Protocol]

  trait Protocol
  case class TimeToLiveInfo(lifetime: Duration) extends Protocol
  case object Stop extends Protocol
  case class HandleFrame(frame: Frame, replyTo: WorkerCoordinator) extends Protocol

  def defaultWorker: FrameWorkerBehavior = {
    Behaviors.receive{ (context, msg) =>
      msg match {
        case HandleFrame(f, r) =>
          if (f.frameCount % 500 == 0) {
            logger.info(s"FrameWorker for frame ${f.frameCount} called in thread ${Thread.currentThread().getName}")
          }
          r ! WorkerCoordinator.WorkerData(f, State(f, Map(scalagl.DRAW_ORDER_PREPROCESS -> List(defaultAction))))
          Behaviors.same
      }
    }
  }

  /**
   * Creates an actor behavior that triggers the drawable functions at the right time
   * @param drawable the drawable to interact with
   * @return Behavior for this worker
   */
  def drawableWorker(drawable: Drawable, timeOfBirth: Long = System.currentTimeMillis(), lifetime: Duration = Duration.Inf): FrameWorkerBehavior = {
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case Stop =>
          logger.info(s"${ctx.self.path} has been asked to stop")
          Behaviors.stopped
        case TimeToLiveInfo(lifetime) =>
          logger.info(s"${ctx.self.path} has been told to live for $lifetime")
          drawableWorker(drawable, timeOfBirth, lifetime)
        case HandleFrame(f, r) =>
          if (f.frameCount % 500 == 0) {
            logger.info(s"HandleFrame in drawableWorker for frame ${f.frameCount} called in thread ${Thread.currentThread().getName}")
          }
          drawable.update(f.wallTime, f.frameCount, f.delta)
          r ! WorkerCoordinator.WorkerData(f, State(f, Map(drawable.drawOrder -> List(drawableAction(drawable)))))

          if (lifetime.isFinite()) {
            val myAge = (System.currentTimeMillis() - timeOfBirth) / 1000
            if (Duration(myAge, duration.SECONDS) > lifetime) {
              logger.info(s"${ctx.self.path} actor is $myAge seconds old (expected to live for $lifetime), asking to be removed")
              r ! RemoveWorker(ctx.self)
            }
          }
          Behaviors.same
      }
    }
  }

  def defaultAction: GLAction =
    (w: Window, c: Camera, f: Frame) => {
      if (f.frameCount % 500 == 0) {
        logger.info(s"DefaultAction for frame ${f.frameCount} called in thread ${Thread.currentThread().getName}")
      }
    }

  def drawableAction(drawable: Drawable): GLAction =
    (w: Window, c: Camera, f: Frame) => {
      if (f.frameCount % 500 == 0) {
        logger.info(s"DrawableAction for frame ${f.frameCount} called in thread ${Thread.currentThread().getName}")
      }
      drawable.render(w, c, f.wallTime, f.frameCount, f.delta)
    }
}
