package nl.about42.scalagl.streaming

import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, receptionist}
import akka.util.Timeout
import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.cluster.Butler.{CreateActor, SystemButler}
import nl.about42.scalagl.streaming.FrameWorker.{FrameWorkerActor, FrameWorkerBehavior, Stop, TimeToLiveInfo}
import nl.about42.scalagl.streaming.StateStream.{Actions, Frame, State}

import scala.concurrent.Await
import scala.concurrent.duration._
/*
doc_start
The :code:`WorkerCoordinator` will keep track of a dynamnic set of :code:`FrameWorkerActor` actors that will be queried on each
frame. Modifications to the set of FrameWorkers are deferred until the current frame results are in. This is done
by running the :code:`WorkerCoordinator` in two modes (:code:`fanOut` and :code:`fanIn`) and keeping local state for the pending
additions, creation requests, and removal requests.

The results collected by the :code:`WorkerCoordinator` will be sent to a :code:`ResultProcessor`
(an :code:`ActorRef[ResultProtocol]`).
The reference is passed to the WorkerCoordinator when frame processing is requested through the :code:`Send(frame, replyTo)` message.

  .. uml::

    hide footbox
    actor Context
    participant ResultProcessor as RP
    participant WorkerCoordinator as WC
    box "Dynamic set of workers"
        participant "FrameWorker 1" as FW1
        participant "FrameWorker N" as FWN
    end box
    hnote over WC: fanOut
    note over WC: curate worker list based on pending lists
    Context -> WC: Add-/Create-/RemoveWorker(w)
    WC -> WC: update worker list
    Context -> WC: Send(frame, replyTo: ResultProcessor)
    activate WC
    WC -> FW1: HandleFrame( frame, self)
    WC -> FWN: HandleFrame( frame, self)
    hnote over WC: fanIn
    Context -> WC: Add-/Create-/RemoveWorker(w)
    WC -> WC: update pending lists
    FW1 -> WC: WorkerData( frame, state)
    FWN -> WC: WorkerData( frame, state)
    WC -> RP: Results(State(frame, state))
    deactivate WC
    hnote over WC: fanOut

  .. uml::

    state fanOut {
        [*] --> curate: if any pending worker mutation
        [*] --> update_fo: Add- / Create- / RemoveWorker(w)
        [*] --> send_fo: Send(frame, replyTo)
        update_fo --> fanOut
        curate --> fanOut
        send_fo --> fanIn

        curate: if pendingAdds then add workers
        curate: if pendingCreates then create workers
        curate: if pendingRemoves then remove workers
        update_fo: update workers collection
        send_fo: call all workers
        send_fo: (send HandleFrame to each)
    }

    state fanIn {
        [*] --> update_fi: Add- / Create- / RemoveWorker(w)
        [*] --> data_fi: WorkerData(w)

        update_fi --> fanIn
        data_fi --> fanIn: received < count(workers)
        data_fi -up-> fanOut: received == count(workers)

        update_fi: add w to pendingAdds / -Creates / -Removes
        data_fi: add worker data to endState
        data_fi: if complete then send state to replyTo
    }

    fanOut: Initial State
    fanOut: - workers
    fanOut: - pendingAdds / pendingCreates / pendingRemoves

    fanIn: - workers
    fanIn: - pendingAdds / pendingCreates / pendingRemoves
    fanIn: - replyTo
    fanIn: - endState
    fanIn: - received (count)

doc_end
 */
object WorkerCoordinator extends LazyLogging {
  type WorkerCoordinator = ActorRef[ScatterGatherProtocol]
  type ResultProcessor = ActorRef[ResultProtocol]

  trait ScatterGatherProtocol
  case class AddWorker(worker: FrameWorkerActor) extends ScatterGatherProtocol
  case class CreateWorker(workerBehavior: FrameWorkerBehavior, ttl: Duration = Duration.Inf) extends ScatterGatherProtocol
  case class RemoveWorker(worker: FrameWorkerActor) extends ScatterGatherProtocol
  case class Send(frame: Frame, replyTo: ResultProcessor) extends ScatterGatherProtocol
  case class WorkerData(frame: Frame, state: State) extends ScatterGatherProtocol
  case object Complete extends ScatterGatherProtocol
  case class Fail(ex: Throwable) extends ScatterGatherProtocol

  trait ResultProtocol
  case class Results(state: State) extends ResultProtocol
  case object ResultComplete extends ResultProtocol
  case class ResultFail(ex: Exception) extends ResultProtocol

  def createActor(system: SystemButler): WorkerCoordinator = {
    import akka.actor.typed.scaladsl.AskPattern._
    Await.result(
      system.ask(
        (replyTo: ActorRef[WorkerCoordinator]) => CreateActor(WorkerCoordinator.start, replyTo))
        (Timeout(3.seconds), system.scheduler),
      1.seconds)
  }

  def start: Behavior[ScatterGatherProtocol] =
    Behaviors.setup{ context =>

      context.system.receptionist ! Receptionist.register(receptionist.ServiceKey[ScatterGatherProtocol]("scatterGatherActor"), context.self)

      val defaultWorker = context.spawnAnonymous(FrameWorker.defaultWorker)
      context.self ! AddWorker(defaultWorker)

      fanOut()
    }

  def fanOut(workers: List[FrameWorkerActor] = List.empty,
             pendingAdds: List[FrameWorkerActor] = List.empty,
             pendingCreates: List[CreateWorker] = List.empty,
             pendingRemoves: List[FrameWorkerActor] = List.empty): Behavior[ScatterGatherProtocol] = {
    if (pendingAdds.isEmpty && pendingCreates.isEmpty && pendingRemoves.isEmpty) {
      Behaviors.receive { (context, msg) =>
        msg match {
          case AddWorker(w) =>
            fanOut(w :: workers)
          case CreateWorker(behavior: FrameWorkerBehavior, ttl) =>
            val worker: FrameWorkerActor = context.spawnAnonymous(behavior)
            worker ! TimeToLiveInfo(ttl)
            fanOut(worker :: workers)
          case RemoveWorker(w) =>
            logger.info(s"Killing worker ${w.path}")
            w ! Stop
            fanOut(workers.filterNot(_ == w))
          case Send(f, replyTo) =>
            workers.foreach(_ ! FrameWorker.HandleFrame(f, context.self))
            fanIn(workers, replyTo)
          case x =>
            logger.warn(s"Ignoring unexpected message in fanOut mode ($x)")
            Behaviors.same
        }
      }
    } else {
      Behaviors.setup { context =>
        val createdWorkers = pendingCreates.map( cw => {
          val worker: FrameWorkerActor = context.spawnAnonymous(cw.workerBehavior)
          worker ! TimeToLiveInfo(cw.ttl)
          worker
        })
        val realAdds = (pendingAdds ++ createdWorkers).filterNot(w => pendingRemoves.contains(w) || workers.contains(w))
        val newWorkers = (workers ++ realAdds).filterNot(pendingRemoves.contains(_))
        if (pendingRemoves.nonEmpty) {
          logger.info(s"Killing ${pendingRemoves.size} workers")
          pendingRemoves.foreach(_ ! Stop)
        }
        fanOut(newWorkers)
      }
    }
  }

  def fanIn( workers: List[FrameWorkerActor], replyTo: ResultProcessor, received: Int = 0, endState: Actions = Map.empty,
             pendingAdds: List[FrameWorkerActor] = List.empty,
             pendingCreates: List[CreateWorker] = List.empty,
             pendingRemoves: List[FrameWorkerActor] = List.empty): Behavior[ScatterGatherProtocol] =
    Behaviors.receive { (context, msg) =>
      msg match {
        case AddWorker(w) =>
          fanIn(workers, replyTo, received, endState, w :: pendingAdds, pendingCreates, pendingRemoves)
        case w @ CreateWorker(_, _) =>
          fanIn(workers, replyTo, received, endState, pendingAdds, w :: pendingCreates, pendingRemoves)
        case RemoveWorker(w) =>
          fanIn(workers, replyTo, received, endState, pendingAdds, pendingCreates, w :: pendingRemoves)
        case WorkerData(f, s) =>
          val newEndstate = mergeMaps(endState, s.actions)
          if (received + 1 == workers.size) {
            // all in, send our result and go back to fanOut mode
            replyTo ! Results(State(s.frame, newEndstate))
            fanOut(workers, pendingAdds, pendingCreates, pendingRemoves)
          } else {
            fanIn(workers, replyTo, received + 1, newEndstate)
          }
        case x =>
          logger.warn(s"Ignoring unexpected message in fanIn mode ($x)")
          Behaviors.same
      }
    }

  def mergeMaps(m1: Actions, m2: Actions): Actions = {
    Seq(m1, m2).reduceLeft((r, map) => map.foldLeft(r){
      case (dict, (k, v)) => dict + (k -> (v ++ dict.getOrElse(k, List.empty)))
    })
  }

}

