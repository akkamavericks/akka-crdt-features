package nl.about42.scalagl.streaming

import nl.about42.scalagl.math.{Constants, Vec3}
import nl.about42.scalagl.streaming.WorkerCoordinator.{CreateWorker, WorkerCoordinator}
import nl.about42.scalagl.ui.Spark
import nl.about42.scalagl.{KeyCallback, Window}
import org.lwjgl.glfw.GLFW

import scala.concurrent.duration._

class WorkerFactory(coordinator: WorkerCoordinator) extends KeyCallback {
  override def keyEvent(window: Window, key: Int, action: Int, modifiers: Int, scancode: Int): Unit = {
    if (key == GLFW.GLFW_KEY_O && (action == GLFW.GLFW_PRESS || action == GLFW.GLFW_REPEAT)) {
      val now = GLFW.glfwGetTime()
      val angle = Constants.TWOPI * (now / 4)
      val direction = Vec3(Math.sin(angle), 0, Math.cos(angle))
      val timeOfDeath = GLFW.glfwGetTime().toFloat + 10
      val spark = new Spark(direction, 1, GLFW.glfwGetTime().toFloat, timeOfDeath)

      coordinator ! CreateWorker(FrameWorker.drawableWorker(spark), 10.seconds )
    }

  }

}
