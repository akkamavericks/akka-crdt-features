package nl.about42.scalagl.streaming

import akka.NotUsed
import akka.actor.typed.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, SinkQueueWithCancel, Source, SourceQueueWithComplete}
import akka.stream.typed.scaladsl.{ActorFlow, ActorMaterializer}
import akka.stream.{Materializer, OverflowStrategy}
import akka.util.Timeout
import com.typesafe.scalalogging.Logger
import nl.about42.akkamavericks.cluster.Butler
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl
import nl.about42.scalagl.Window
import nl.about42.scalagl.streaming.WorkerCoordinator._
import nl.about42.scalagl.streaming.StateStream._

import scala.concurrent.duration._

object StateStream {
  type GLAction = (Window, Camera, Frame) => Unit
  type Actions = Map[Int, List[GLAction]]

  type StateFlow = Flow[Frame, State, NotUsed]
  type StateStreamInput = SourceQueueWithComplete[Frame]
  type StateStreamOutput = SinkQueueWithCancel[State]

  case class Frame(wallTime: Float, delta: Float, frameCount: Int)
  case class State(frame: Frame, actions: Actions)

  val stateSource: Source[Frame, StateStreamInput] = Source.queue[Frame](1, OverflowStrategy.dropTail)
  val stateSink: Sink[State, StateStreamOutput] = Sink.queue[State]

  def createDynamicActorFlowStageStream(system: ActorSystem[Butler.Command]): ActorStateStream = {
    implicit val mat: Materializer = ActorMaterializer()(system)

    val scatterActorRef = WorkerCoordinator.createActor(system)
    val scatterGather = ActorFlow.ask(scatterActorRef)((f: Frame, replyTo: ResultProcessor) => Send(f, replyTo) )(Timeout(3.seconds))

    val stage = scatterGather.map{
      case Results(s) => s
    }
    val (source, sink) = stage.runWith(stateSource, stateSink)

    new ActorStateStream(source, sink, scatterActorRef)
  }

  def createSingleStageStream(stage: StateFlow)(implicit mat: Materializer): StateStream = {
    val (source, sink) = stage.runWith(stateSource, stateSink)
    new SingleStageStateStream(source, sink)
  }

  def debugFlow(logger: Logger): StateFlow = Flow.fromFunction((frame: Frame) => {
    if (frame.frameCount % 500 == 0) {
      logger.info(s"Flow for frame ${frame.frameCount} called in thread ${Thread.currentThread().getName}")
    }
    State(frame, Map(scalagl.DRAW_ORDER_PREPROCESS -> List(debugAction(logger))))
  })

  def debugAction(logger: Logger): GLAction =
    (w: Window, c: Camera, f: Frame) => {
      if (f.frameCount % 500 == 0) {
        logger.info(s"Action for frame ${f.frameCount} called in thread ${Thread.currentThread().getName}")
      }
    }
}

// example_start
/* doc_start

The StateStream trait provides access to the start (source) and end (sink) of the stream processing pipeline that
transforms a Frame - containing current frame data - to a State - containing drawing commands and other processing
instructions that need to be executed for the upcoming frame.

.. uml::

  @startuml

    StateStream <|-- ActorStateStream

    class StateStream <<trait>> {
      sink: StateStreamInput
      source: StateStreamOutput
    }

    class ActorStateStream {
      scatterActor: ActorRef[ScatterGatherProtocol]
    }

    legend
      StateStreamInput is a SourceQueueWithComplete[Frame]
      StateStreamOutput is a SinkQueueWithCancel[State]
    end legend
  @enduml

doc_end
 */
trait StateStream {
  def source: StateStreamInput
  def sink: StateStreamOutput
}
// example_end

class SingleStageStateStream(val source: StateStreamInput, val sink: StateStreamOutput) extends StateStream

class ActorStateStream(val source: StateStreamInput, val sink: StateStreamOutput, val scatterActor: WorkerCoordinator) extends StateStream
