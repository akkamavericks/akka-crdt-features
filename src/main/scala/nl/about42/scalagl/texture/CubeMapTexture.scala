package nl.about42.scalagl.texture

import java.nio.ByteBuffer
import java.util.concurrent.atomic.AtomicBoolean

import nl.about42.scalagl.Util
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.{GL11, GL12, GL13}
import org.lwjgl.stb.STBImage

object CubeMapTexture {
  val defaultSuffixes = List("_rt", "_lf", "_up", "_dn", "_ft", "_bk")

}

class CubeMapTexture(filename: String, extension: String = ".png", suffixes: List[String] = CubeMapTexture.defaultSuffixes) {
  private lazy val texId = GL11.glGenTextures()

  var isDirty = new AtomicBoolean(true)
  def bindTexture(textureType: Int): Unit = {
    GL11.glBindTexture(textureType, texId)
    if (isDirty.getAndSet(false)){
      refresh()
    }
  }

  private def refresh(): Unit = {
    GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, texId)
    STBImage.stbi_set_flip_vertically_on_load(false)

    val widthBuf = BufferUtils.createIntBuffer(1)

    val heightBuf = BufferUtils.createIntBuffer(1)
    val compBuf = BufferUtils.createIntBuffer(1)

    var i = 0
    // use the order in which the GL_TEXTURE_CUBE_MAP_xxx axis are defined
    for(suffix <- suffixes) {
      val imageBuffer: ByteBuffer = Util.ioResourceToByteBuffer(filename + suffix + extension, 1024)
      if (!STBImage.stbi_info_from_memory(imageBuffer, widthBuf, heightBuf, compBuf)) {
        throw new RuntimeException("Failed to read image information: " + STBImage.stbi_failure_reason)
      }
      val image = STBImage.stbi_load_from_memory(imageBuffer, widthBuf, heightBuf, compBuf, 0)
      if (image == null) {
        throw new RuntimeException("Failed to load image: " + STBImage.stbi_failure_reason)
      }

      val width = widthBuf.get(0)
      val height = heightBuf.get(0)
      val components = compBuf.get(0)
      val format = components match {
        case 3 => GL11.GL_RGB
        case 4 => GL11.GL_RGBA
        case _ => throw new RuntimeException(s"Unsupported number of texture components (${components}")
      }

      GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, width, height, 0, format, GL11.GL_UNSIGNED_BYTE, image)

      i += 1
    }

    GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR)
    GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR)
    GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE)
    GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE)
    GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL12.GL_TEXTURE_WRAP_R, GL12.GL_CLAMP_TO_EDGE)
  }
}
