package nl.about42.scalagl.texture

import com.typesafe.config.Config
import collection.JavaConverters._

object FontAtlas {

  private var fonts: Map[Int, FontAtlas] = Map.empty
  private var init = false

  def getFonts(): Map[Int, FontAtlas] = fonts

  def getFont(charWidth: Int): FontAtlas = fonts(charWidth)

  def init(config: Config): Unit = {
    if (!init){
      val fontConfigs = config.getConfigList("gui.fonts").asScala

      fonts = fontConfigs.map(fc => {
        val texture = Texture.readTexture(fc.getString("file"), false)
        (fc.getInt("charWidth"), new FontAtlas(texture, fc.getInt("charWidth"), fc.getInt("charHeight"),
          fc.getInt("charsPerLine"), fc.getInt("firstChar"), fc.getInt("lastChar")))
      }).toMap
      init = true
    }
  }
}

class FontAtlas(val atlas: Texture, val charWidth: Int, val charHeight: Int, charPerLine: Int, firstChar: Int = 32, lastChar: Int = 127) {
  def writeChar(target: Texture, char: Char, destX: Int, destY: Int) = {
    val (startX, startY) = getCharPos(char)
    target.copyRange(atlas, startX, startY, charWidth, charHeight, destX, destY, false )
  }

  def getCharPos(char: Char): (Int, Int) = {
    val index = char - firstChar
    if (index < 0 || index > lastChar - firstChar) {
      (0, 0)
    } else {
      val row = index / charPerLine
      val col = index % charPerLine
      (col * charWidth, row * charHeight)
    }
  }
}
