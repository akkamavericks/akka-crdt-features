package nl.about42.scalagl.texture

import java.nio.ByteBuffer

import nl.about42.scalagl.math.{Bounds, Vec4}

class Card( val texture: Texture, val cardBounds: Bounds, val textureBounds: Bounds, val fontAtlas: FontAtlas) {

  var myText = ""
  var myColor = Vec4(1, 1, 0, 1)

  def setColor(color: Vec4): Unit = {
    myColor = color
    update()
  }

  def setText(message: String) = {
    myText = message
    update()
  }

  var newImage: ByteBuffer = null
  def newData(buffer: ByteBuffer): Unit = {
    newImage = buffer
    val tex = Texture.textureFromImageByteBuffer(newImage, true)
    texture.newImageData(tex)
  }

  private def update(): Unit = {
    var charX = 0
    var charY = texture.height - 1
    myText.foreach {
      case '\n' =>
        charX = 0
        charY -= fontAtlas.charHeight
      case ch =>
        fontAtlas.writeChar(texture, ch, charX, charY)
        charX += fontAtlas.charWidth
    }
  }

}
