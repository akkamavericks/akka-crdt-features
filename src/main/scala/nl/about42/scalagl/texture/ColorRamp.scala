package nl.about42.scalagl.texture

import nl.about42.scalagl.math.{ColorConversions, Vec4}
import nl.about42.scalagl.texture.ColorRamp.Tier

object ColorRamp {
  type Tier = (Float, Vec4)

  def apply(colors: Seq[Vec4]): ColorRamp = {
    colors.size match {
      case 0 => new ColorRamp(Array((0f, Vec4(0, 0, 0, 1)), (1f, Vec4(1))))
      case 1 => apply(List((0f, Vec4(0, 0, 0, 1)), (1f, colors.head)))
      case s =>
        val tierSize = 1.0f / (s - 1)
        val list = ((0 until s) zip colors).map(r => (r._1 * tierSize, r._2)).toList
        apply(list)
    }
  }
  def apply(tiers: List[Tier] = List.empty): ColorRamp = {
    val sorted = tiers.sortBy(_._1).toArray
    if (sorted.isEmpty) {
      new ColorRamp(Array((0f, Vec4(0, 0, 0, 1)), (1f, Vec4(1))))
    } else {
      new ColorRamp(sorted)
    }
  }

  val RAINBOW = ColorRamp(Seq(
    Vec4(1, 0, 0, 1),
    Vec4(1, 1, 0, 1),
    Vec4(0, 1, 0, 1),
    Vec4(0, 1, 1, 1),
    Vec4(0, 0, 1, 1),
    Vec4(1, 0, 1, 1)
  ))

  val RAINBOWCIRCULAR = ColorRamp(Seq(
    Vec4(1, 0, 0, 1),
    Vec4(1, 1, 0, 1),
    Vec4(0, 1, 0, 1),
    Vec4(0, 1, 1, 1),
    Vec4(0, 0, 1, 1),
    Vec4(1, 0, 1, 1),
    Vec4(1, 0, 0, 1)
  ))
}

class ColorRamp(tiers: Array[Tier]) extends ColorConversions {
  val min = tiers(0)._1
  val max = tiers(tiers.size - 1)._1

  def apply(value: Float): Vec4 = {
    val clamped = clamp(value, min, max)
    var tierFloor = 0
    var tierCeiling = tiers.size - 1

    (1 until tiers.size).foreach(i => {
      if (clamped >= tiers(i)._1){
        tierFloor = i
      }
      if (clamped < tiers(i)._1 && tierCeiling == tiers.size - 1){
        tierCeiling = i
      }
    })
    val result = if (tiers(tierFloor)._1 == tiers(tierCeiling)._1) {
      // use floor color if tier size is zero (includes cases outside min/max)
      tiers(tierFloor)._2
    } else {
      Vec4.lerp(tiers(tierFloor)._2, tiers(tierCeiling)._2, (clamped - tiers(tierFloor)._1 )/ (tiers(tierCeiling)._1 - tiers(tierFloor)._1))
    }
    result
  }
}
