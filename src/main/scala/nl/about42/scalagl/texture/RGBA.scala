package nl.about42.scalagl.texture

import nl.about42.scalagl.math.{ColorConversions, Vec3, Vec4}
import nl.about42.scalagl.texture.RGBA.{ALPHA, BLUE, Channel, GREEN, RED}

object RGBA {
  def apply(v: Vec4): RGBA = {
    new RGBA().set(v)
  }
  def apply(v: Vec3): RGBA = {
    new RGBA().set(Vec4(v, 1))
  }
  def apply(rgba: RGBA): RGBA = {
    new RGBA(rgba.underlying)
  }
  def apply(rgba: Int): RGBA = {
    new RGBA(rgba)
  }

  sealed trait Channel {
    def shift: Int
    def mask: Int
    def inverseMask: Int = mask ^ 0xFFFFFFFF
  }
  case object RED extends Channel {
    def shift = 24
    def mask = 0xFF000000
  }
  case object GREEN extends Channel {
    def shift = 16
    def mask = 0x00FF0000
  }
  case object BLUE extends Channel {
    def shift = 8
    def mask = 0x0000FF00
  }
  case object ALPHA extends Channel {
    def shift = 0
    def mask = 0x000000FF
  }
}

class RGBA(private var underlying: Int = 0) extends ColorConversions {
  def r: Byte = this(RED)
  def g: Byte = this(GREEN)
  def b: Byte = this(BLUE)
  def a: Byte = this(ALPHA)

  def toVec4(): Vec4 = {
    Vec4(b2f(r), b2f(g), b2f(b), b2f(a))
  }

  def toVec3(): Vec3 = {
    Vec3(b2f(r), b2f(g), b2f(b))
  }

  def set(v: Vec4) = {
    underlying =
      getBits(v.r, 24) |
      getBits(v.g, 16) |
      getBits(v.b, 8) |
      getBits(v.a, 0)
    this
  }

  def apply(c: Channel): Byte = {
    getByte(underlying, c.shift)
  }

  def update(c: Channel, x: Float): Unit = {
    underlying = (underlying & c.inverseMask) | (getBits(x, c.shift) & c.mask)
  }

  override def toString: String = {
    f"$underlying%08X"
  }
}
