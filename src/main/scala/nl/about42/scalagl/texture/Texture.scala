package nl.about42.scalagl.texture

import java.nio.ByteBuffer
import java.util.concurrent.atomic.AtomicBoolean

import com.typesafe.scalalogging.LazyLogging
import nl.about42.scalagl.Util
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.math.{ColorConversions, Vec4}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.{GL11, GL12, GL30}
import org.lwjgl.stb.STBImage

object Texture extends LazyLogging with ColorConversions {
  def createTexture(width: Int, height: Int, initialColor: Vec4, format: Int = GL11.GL_RGBA): Texture = {
    val texFormat = format match {
      case GL11.GL_RGB => GL11.GL_RGB
      case _ => GL11.GL_RGBA
    }
    val image = BufferUtils.createByteBuffer(width * height * 4)

    val r = f2b(initialColor.r)
    val g = f2b(initialColor.g)
    val b = f2b(initialColor.b)
    val a = f2b(initialColor.a)

    logger.info(s"RGBA is $r $g $b $a")

    (0 until width * height).foreach( _ => {
      image.put(r)
      image.put(g)
      image.put(b)
      if (texFormat == GL11.GL_RGBA) {
        image.put(a)
      }
    })
    image.flip()

    new Texture(image, width, height, texFormat)
  }

  def readTexture(textureFile: String, flip: Boolean = true): Texture = {
    val imageBuffer: ByteBuffer = Util.ioResourceToByteBuffer(textureFile, 1024)

    textureFromImageByteBuffer(imageBuffer, flip)
  }

  def textureFromImageByteBuffer(imageBuffer: ByteBuffer, flip: Boolean): Texture = {
    val widthBuf = BufferUtils.createIntBuffer(1)
    val heightBuf = BufferUtils.createIntBuffer(1)
    val compBuf = BufferUtils.createIntBuffer(1)

    STBImage.stbi_set_flip_vertically_on_load(flip)

    if (!STBImage.stbi_info_from_memory(imageBuffer, widthBuf, heightBuf, compBuf)) {
      throw new RuntimeException("Failed to read image information: " + STBImage.stbi_failure_reason)
    }

    val width = widthBuf.get(0)
    val height = heightBuf.get(0)
    val components = compBuf.get(0)

    logger.debug(s"Processing image data (w x h x comps) $width x $height x $components")

    val image = STBImage.stbi_load_from_memory(imageBuffer, widthBuf, heightBuf, compBuf, 0)
    if (image == null) {
      throw new RuntimeException("Failed to process image: " + STBImage.stbi_failure_reason)
    }

    val format = components match {
      case 3 => GL11.GL_RGB
      case 4 => GL11.GL_RGBA
      case _ => throw new RuntimeException(s"Unsupported number of texture components (${components}")
    }

    logger.debug(s"Image data ready (w x h x comps) $width x $height x $components")

    new Texture(image, width, height, format)

  }

}

class Texture(val buffer: ByteBuffer, val width: Int, val height: Int, val format: Int) extends ColorConversions with LazyLogging {

  private lazy val texId = GL11.glGenTextures()
  var isDirty: AtomicBoolean = new AtomicBoolean(true)

  val wrapModes = Array(GL12.GL_CLAMP_TO_EDGE, GL12.GL_CLAMP_TO_EDGE)
  logger.info(s"Created a texture with size $width x $height (${Util.getGLDefine(format)} format; buffersize is ${buffer.remaining()})")

  def bindTexture(textureType: Int): Unit = {
    GL11.glBindTexture(textureType, texId)
    logOnGLError(s"Bind texture failed")
    if (isDirty.getAndSet(false)) {
      refresh()
      logOnGLError("Texture refresh failed")
    }
  }

  private def refresh(): Unit = {
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId)
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR)
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR)
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, wrapModes(0))
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, wrapModes(1))

    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, format, width, height, 0, format, GL11.GL_UNSIGNED_BYTE, buffer)
    logOnGLError(s"glTexImage2D failed")
    GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D)
    logOnGLError(s"glGenerateMipmap failed")
  }

  def setWrapModes(wrapS: Int, wrapT: Int) = {
    wrapModes(0) = wrapS
    wrapModes(1) = wrapT
    isDirty.set(true)
  }

  def copyRange(source: Texture, startX: Int, startY: Int, copyWidth: Int, copyHeight: Int, destX: Int, destY: Int, destDirectionUp: Boolean = true): Unit = {
    copyRange(source.buffer, source.width, source.height, startX, startY, copyWidth, copyHeight, source.format,
      destX, destY, destDirectionUp)
  }

  def copyRange(source: ByteBuffer, sourceWidth: Int, sourceHeight: Int,
                startX: Int, startY: Int, copyWidth: Int, copyHeight: Int, sourceFormat: Int,
                destX: Int, destY: Int, destDirectionUp: Boolean): Unit = {
    val srcComps = sourceFormat match {
      case GL11.GL_RGBA => 4
      case _ => 3
    }
    val components = format match {
      case GL11.GL_RGBA => 4
      case _ => 3
    }

    if (components < srcComps) {
      // source with alpha to non alpha dest: assume we want to blend onto image
      val sourceLineStride = sourceWidth * srcComps
      val sourceStartOffset = startX * srcComps + startY * sourceLineStride
      val destLineStride = width * components
      val destStartOffset = destX * components + destY * destLineStride
      val dy = if (destDirectionUp) 1 else -1
      for(line <- 0 until copyHeight) {
        for(pos <- 0 until copyWidth) {
          // get the source colors and alpha, the dest colors, and perform the alpha based blend
          val srcCol: Array[Byte] = Array(0, 0, 0, 0)
          val dstCol: Array[Byte] = Array(0, 0, 0, 0)
          val newCol: Array[Byte] = Array(127, 0, 127, 0)

          for (c <- 0 until srcComps) {
            srcCol(c) = source.get(pos * srcComps + line * sourceLineStride + sourceStartOffset + c)
            if (c < components) {
              dstCol(c) = buffer.get(pos * components + dy * line * destLineStride + destStartOffset + c)
            }
          }
          blend(newCol, srcCol, dstCol)
          for (c <- 0 until components) {
            buffer.put(pos * components + dy * line * destLineStride + destStartOffset + c,
              newCol(c))
          }
        }
      }

    } else {
      val sourceLineStride = sourceWidth * srcComps
      val sourceStartOffset = startX * srcComps + startY * sourceLineStride
      val destLineStride = width * components
      val destStartOffset = destX * components + destY * destLineStride
      val dy = if (destDirectionUp) 1 else -1
      for(line <- 0 until copyHeight) {
        for(pos <- 0 until copyWidth) {
          for (c <- 0 until components) {
            if (c < srcComps) {
              buffer.put(pos * components + dy * line * destLineStride + destStartOffset + c,
                source.get(pos * srcComps + line * sourceLineStride + sourceStartOffset + c))
            } else {
              // default to alpha 1.0
              buffer.put(pos * components + dy * line * destLineStride + destStartOffset + c, f2b(1))
            }
          }
        }
      }
    }
    isDirty.set(true)
  }

  def newImageData(srcTexture: Texture): Unit = {
    newImageData(srcTexture.buffer, srcTexture.width, srcTexture.height, srcTexture.format)
  }

  def newImageData(newBuffer: ByteBuffer, newWidth: Int, newHeight: Int, newFormat: Int): Unit = {
    val neededBytes = buffer.remaining()
    if (this.format == newFormat) {
      if (newBuffer.remaining() == buffer.remaining()) {
        logger.info("new and current buffer format and sizes match")
        buffer.put(newBuffer)
      } else if (newBuffer.remaining() > neededBytes) {
        logger.info(s"new buffer bigger than current (${newBuffer.remaining()} vs ${buffer.remaining()})")
        val tmp = newBuffer.duplicate()
        tmp.limit(buffer.remaining())
        buffer.put(tmp)
      } else {
        // not enough, can only partially fill the buffer. Hope you know what you're doing...
        logger.info(s"new buffer smaller than current (${newBuffer.remaining()} vs ${buffer.remaining()})")
        buffer.put(newBuffer)
      }
      buffer.flip()
    } else {
      logger.info(s"Formats differ (need ${Util.getGLDefine(format)}, get ${Util.getGLDefine(newFormat)}, copy image data via loop (needed $neededBytes, available ${newBuffer.remaining()})")
      this.copyRange(newBuffer, newWidth, newHeight, 0, 0, width, height, newFormat, 0, 0, true)
    }
    isDirty.set(true)
  }

  def blend(target: Array[Byte], src: Array[Byte], dst: Array[Byte]): Unit = {
    target(0) = blendByte(src(0), dst(0), src(3))
    target(1) = blendByte(src(1), dst(1), src(3))
    target(2) = blendByte(src(2), dst(2), src(3))
    target(3) = blendByte(src(3), dst(3), src(3))
  }

  def blendByte(src: Byte, dst: Byte, srcAlpha: Byte): Byte = {
    f2b(b2f(src)*b2f(srcAlpha) + b2f(dst)*(1.0f - b2f(srcAlpha)))
  }
}
