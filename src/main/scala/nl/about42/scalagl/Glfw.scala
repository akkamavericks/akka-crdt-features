package nl.about42.scalagl

import com.typesafe.scalalogging.{LazyLogging, Logger}
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.glfw.GLFW._

object Glfw extends LazyLogging{
  type WindowHint = (Int, Int)

  def getError(id: Int): String = Util.GLFW_STATIC_DEFINES.getOrElse(id, s"Unknown error code ($id)")

  def init(): Boolean = {
    glfwInit()
  }

  def terminate(): Unit = {
    glfwTerminate()
  }

  val errHandler = new GLFWErrorCallback {
    override def invoke(error: Int, description: Long): Unit = {
      val desc = GLFWErrorCallback.getDescription(description)
      logger.error(s"[GLFW] $error (${getError(error)}): $desc")
      logger.error(s"[GLFW] $error occurred at")
      val stack = Thread.currentThread.getStackTrace.toList
      stack.drop(2).foreach { line =>
        logger.error(s"\t\t$line")
      }
    }
  }

  def createWindow(w: Int, h: Int, x: Int, y: Int, title: String, hints: Seq[Glfw.WindowHint], logger: Option[Logger] = None, open: Boolean): Window = {
    Window.create(w, h, x, y, title, hints, logger, open)
  }

}
