package nl.about42.scalagl.math

/**
  * Types and functions based on the C++ glm library (https://glm.g-truc.net/0.9.9/index.html)
  */

trait FloatOperations {
  def nonZero(value: Float): Boolean = {
    value < - Constants.EPSILON || value > Constants.EPSILON
  }
}

trait VectorOperations {

  def normalized(v: Vec3): Vec3 = {
    val result = Vec3(v)
    result.normalize()
    result
  }

}

trait MatrixOperations extends VectorOperations with FloatOperations {

  // based on lookAtRH (glm/ext/matrix_transform.iml)
  def loadLookat(m: Mat4, eye: Vec3, center: Vec3, up: Vec3): Unit = {
    val f = normalized(center - eye)
    val s = normalized(f X up)
    val u = s X f

    m.zero()

    m(0,0) =  s.x
    m(1,0) =  s.y
    m(2,0) =  s.z
    m(0,1) =  u.x
    m(1,1) =  u.y
    m(2,1) =  u.z
    m(0,2) = -f.x
    m(1,2) = -f.y
    m(2,2) = -f.z
    m(3,0) = -1.0f * (s * eye)
    m(3,1) = -1.0f * (u * eye)
    m(3,2) =  f * eye
    m(3,3) =  1.0f
  }

  // based on orthoRH_NO (glm/ext/matrix_clip_space.inl)
  def loadOrtho(m: Mat4, left: Float, right: Float, top: Float, bottom: Float, zNear: Float, zFar: Float): Unit = {
    assert(right > left)
    assert(top > bottom)
    assert(zFar > zNear)

    m.zero()
    m(0, 0) = 2.0f / (right - left)
    m(1, 1) = 2.0f / (top - bottom)
    m(2, 2) = -2.0f / (zFar - zNear)
    m(3, 0) = -1.0f * (right + left) / (right -  left)
    m(3, 1) = -1.0f * (top + bottom) / (top -  bottom)
    m(3, 2) = -1.0f * (zFar + zNear) / (zFar -  zNear)
  }

  // based on perspectiveRH_NO (glm/ext/matrix_clip_space.inl)
  // see also http://www.songho.ca/opengl/gl_projectionmatrix.html
  def loadPerspective(m: Mat4, fovY: Float, aspect: Float, zNear: Float, zFar: Float): Unit = {
    assert(nonZero(aspect))
    assert(nonZero(fovY))
    assert(0.0f < fovY && fovY < Constants.PI)
    assert(zFar > zNear)
    assert(zNear > 0.0f)

    m.zero()

    val tanHalfFovY: Float = Math.tan(fovY / 2.0).toFloat
    m(0, 0) = 1.0f / (aspect * tanHalfFovY)
    m(1, 1) = 1.0f / tanHalfFovY
    m(2, 2) = -1.0f * (zFar + zNear) / (zFar - zNear)
    m(2, 3) = -1.0f
    m(3, 2) = (-2.0f * zFar * zNear)/(zFar - zNear)
    m(3, 3) = 0.0f

  }

  // creates a transformation matrix to put the origin at origin and orientation aligned to up (+Y) and right (+X)
  // based on http://www.songho.ca/opengl/gl_lookattoaxes.html, but adjusted until correct results were showing :)
  def loadPosition(m: Mat4, origin: Vec3, up: Vec3, right: Vec3): Unit = {
    val nBackward = right X up
    nBackward.normalize()

    val nUp = Vec3(up)
    nUp.normalize()

    val nRight = nUp X nBackward
    nRight.normalize()

    m(0, 0) = nRight.x
    m(0, 1) = nRight.y
    m(0, 2) = nRight.z

    m(1, 0) = nUp.x
    m(1, 1) = nUp.y
    m(1, 2) = nUp.z

    m(2, 0) = nBackward.x
    m(2, 1) = nBackward.y
    m(2, 2) = nBackward.z

    m(0, 3) = 0.0f
    m(1, 3) = 0.0f
    m(2, 3) = 0.0f

    m(3, 0) = nRight * origin
    m(3, 1) = nUp * origin
    m(3, 2) = nBackward * origin

    m(3, 3) = 1.0f
  }

  def loadRotate(m: Mat4, axis: Vec3, angle: Float): Unit = {
    val nAxis = normalized(axis)

    val cosAngle = Math.cos(angle).toFloat
    val sinAngle = Math.sin(angle).toFloat
    val oneMinCos = 1.0f - cosAngle

    // based on https://learnopengl.com/Getting-started/Transformations

    m.zero()
    m(0, 0) = cosAngle + nAxis.x * nAxis.x * oneMinCos
    m(0, 1) = nAxis.y * nAxis.x * oneMinCos + nAxis.z * sinAngle
    m(0, 2) = nAxis.z * nAxis.x * oneMinCos - nAxis.y * sinAngle

    m(1, 0) = nAxis.x * nAxis.y * oneMinCos - nAxis.z * sinAngle
    m(1, 1) = cosAngle + nAxis.y * nAxis.y * oneMinCos
    m(1, 2) = nAxis.z * nAxis.y * oneMinCos + nAxis.x * sinAngle

    m(2, 0) = nAxis.x * nAxis.z * oneMinCos + nAxis.y * sinAngle
    m(2, 1) = nAxis.y * nAxis.z * oneMinCos - nAxis.x * sinAngle
    m(2, 2) = cosAngle + nAxis.z * nAxis.z * oneMinCos

    m(3, 3) = 1.0f

  }

  def loadScale(m: Mat4, factor: Vec3): Unit = {
    m.zero()
    m(0, 0) = factor.x
    m(1, 1) = factor.y
    m(2, 2) = factor.z
    m(3, 3) = 1.0f
  }

  def loadTranslate(m: Mat4, direction: Vec3): Unit = {
    m.zero()
    m(0, 0) = 1.0f
    m(1, 1) = 1.0f
    m(2, 2) = 1.0f
    m(3, 3) = 1.0f
    m(3, 0) = direction.x
    m(3, 1) = direction.y
    m(3, 2) = direction.z
  }

  def lookAt(eye: Vec3, center: Vec3, up: Vec3): Mat4 = {
    val result = Mat4.identity

    loadLookat(result, eye, center, up)
    result
  }

  def ortho(left: Float, right: Float, top: Float, bottom: Float, zNear: Float, zFar: Float): Mat4 = {
    val result = Mat4.identity
    loadOrtho(result, left, right, top, bottom, zNear, zFar)
    result
  }

  def perspective(fovY: Float, aspect: Float, zNear: Float, zFar: Float): Mat4 = {
    val result = Mat4.identity
    loadPerspective(result, fovY, aspect, zNear, zFar)
    result
  }

  def rotate(axis: Vec3, angle: Float): Mat4 = {
    val result = Mat4.zero
    loadRotate(result, axis, angle)
    result
  }

  def translate(direction: Vec3): Mat4 = {
    val result = Mat4.identity
    loadTranslate(result, direction)
    result
  }

  def scale(factor: Vec3): Mat4 = {
    val result = Mat4.identity
    loadScale(result, factor)
    result
  }

  def scale(factor: Float): Mat4 = {
    scale(Vec3(factor, factor, factor))
  }

}
