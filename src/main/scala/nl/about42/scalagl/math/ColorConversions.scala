package nl.about42.scalagl.math

trait ColorConversions {
  def b2f(in: Byte): Float = {
    // scala bytes range from -128 to 127
    // unsigned bytes 0 - 255 map to 0 - 127, -128 - -1
    if (in < 0) {
      (256 + in) / 255f
    } else {
      in / 255f
    }
  }

  def f2b(in: Float): Byte = {
    (255 * clamp(in, 0, 1)).toByte
  }

  def getBits(x: Float, shift: Int): Int = {
    val norm = Math.round(clamp(x, 0, 1) * 255)
    norm << shift
  }

  def getByte(x: Int, shift: Int): Byte = {
    ((x >> shift) & 0x000000FF).toByte
  }

  def getFloat(x: Int, shift: Int): Float = {
    b2f(getByte(x, shift))
  }

  def clamp(value: Float, min: Float = 0f, max: Float = 1.0f): Float = {
    if (value < min) min
    else if (value > max) max
    else value
  }
}
