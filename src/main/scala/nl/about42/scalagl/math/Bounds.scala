package nl.about42.scalagl.math

import Math.{max, min}

// todo: make sure there are no negative width/height possible
case object Bounds {
  def apply(c1: Vec2, c2: Vec2): Bounds = {
    Bounds(c1.x, c1.y, c2.x - c1.x, c2.y - c1.y)
  }
}

case class Bounds(x: Float, y: Float, width: Float, height: Float) {
  def bl: Vec2 = Vec2(min(x, x + width), min(y, y + height))
  def br: Vec2 = Vec2(max(x, x + width), min(y, y + height))
  def tl: Vec2 = Vec2(min(x, x + width), max(y, y + height))
  def tr: Vec2 = Vec2(max(x, x + width), max(y, y + height))

}
