package nl.about42.scalagl.math

import java.nio.FloatBuffer
import org.lwjgl.BufferUtils

object Mat4 {
  def axis(axis: Vec4): Mat4 = {
    val result = new Mat4
    result.axis(axis)
    result
  }

  def identity: Mat4 = apply(1.0f)
  def zero: Mat4 = apply(0.0f)

  def apply(axis: Float = 0.0f): Mat4 = {
    val result = new Mat4
    result(0, 0) = axis
    result(1, 1) = axis
    result(2, 2) = axis
    result(3, 3) = axis
    result
  }

  def apply(m: Mat4): Mat4 = {
    val result = new Mat4
    Array.copy(m, 0, result, 0, 16)
    result
  }
}

class Mat4 {
  val values: Array[Float] = Array.fill(16)(0)

  def axis(value: Float): Unit = {
    this.zero()
    this(0,0) = value
    this(1,1) = value
    this(2,2) = value
    this(3,3) = value
  }

  def axis(axis: Vec4): Unit = {
    this.zero()
    this(0, 0) = axis.x
    this(1, 1) = axis.y
    this(2, 2) = axis.z
    this(3, 3) = axis.w
  }

  def zero(): Unit = {
    for(i <- 0 until 15)
      values(i) = 0.0f
  }

  def getBuffer: FloatBuffer = {
    val buffer = BufferUtils.createFloatBuffer(16)
    values.foreach(buffer.put)
    buffer.flip()
    buffer
  }

  def apply(col: Int, row: Int): Float = {
    assert(0 <= col && col <= 3 && 0 <= row && row <= 3)
    values(4 * col + row)
  }

  def update(col: Int, row: Int, value: Float): Unit = {
    assert(0 <= col && col <= 3 && 0 <= row && row <= 3)
    values(4 * col + row) = value
  }

  def set(m: Mat4): Unit = {
    Array.copy(m.values, 0, values, 0, 16)
  }
  
  def *(v: Vec4): Vec4 = {
    val result = Vec4()

    result(0) = this(0, 0) * v.x + this(1, 0) * v.y + this(2, 0) * v.z + this(3, 0) * v.w
    result(1) = this(0, 1) * v.x + this(1, 1) * v.y + this(2, 1) * v.z + this(3, 1) * v.w
    result(2) = this(0, 2) * v.x + this(1, 2) * v.y + this(2, 2) * v.z + this(3, 2) * v.w
    result(3) = this(0, 3) * v.x + this(1, 3) * v.y + this(2, 3) * v.z + this(3, 3) * v.w

    result
  }
  
  def *(m: Mat4): Mat4 = {
    val result = Mat4.zero

    result(0, 0) = this(0, 0) * m(0, 0) + this(1, 0) * m(0, 1) + this(2, 0) * m(0, 2) + this(3, 0) * m(0, 3)
    result(0, 1) = this(0, 1) * m(0, 0) + this(1, 1) * m(0, 1) + this(2, 1) * m(0, 2) + this(3, 1) * m(0, 3)
    result(0, 2) = this(0, 2) * m(0, 0) + this(1, 2) * m(0, 1) + this(2, 2) * m(0, 2) + this(3, 2) * m(0, 3)
    result(0, 3) = this(0, 3) * m(0, 0) + this(1, 3) * m(0, 1) + this(2, 3) * m(0, 2) + this(3, 3) * m(0, 3)

    result(1, 0) = this(0, 0) * m(1, 0) + this(1, 0) * m(1, 1) + this(2, 0) * m(1, 2) + this(3, 0) * m(1, 3)
    result(1, 1) = this(0, 1) * m(1, 0) + this(1, 1) * m(1, 1) + this(2, 1) * m(1, 2) + this(3, 1) * m(1, 3)
    result(1, 2) = this(0, 2) * m(1, 0) + this(1, 2) * m(1, 1) + this(2, 2) * m(1, 2) + this(3, 2) * m(1, 3)
    result(1, 3) = this(0, 3) * m(1, 0) + this(1, 3) * m(1, 1) + this(2, 3) * m(1, 2) + this(3, 3) * m(1, 3)

    result(2, 0) = this(0, 0) * m(2, 0) + this(1, 0) * m(2, 1) + this(2, 0) * m(2, 2) + this(3, 0) * m(2, 3)
    result(2, 1) = this(0, 1) * m(2, 0) + this(1, 1) * m(2, 1) + this(2, 1) * m(2, 2) + this(3, 1) * m(2, 3)
    result(2, 2) = this(0, 2) * m(2, 0) + this(1, 2) * m(2, 1) + this(2, 2) * m(2, 2) + this(3, 2) * m(2, 3)
    result(2, 3) = this(0, 3) * m(2, 0) + this(1, 3) * m(2, 1) + this(2, 3) * m(2, 2) + this(3, 3) * m(2, 3)

    result(3, 0) = this(0, 0) * m(3, 0) + this(1, 0) * m(3, 1) + this(2, 0) * m(3, 2) + this(3, 0) * m(3, 3)
    result(3, 1) = this(0, 1) * m(3, 0) + this(1, 1) * m(3, 1) + this(2, 1) * m(3, 2) + this(3, 1) * m(3, 3)
    result(3, 2) = this(0, 2) * m(3, 0) + this(1, 2) * m(3, 1) + this(2, 2) * m(3, 2) + this(3, 2) * m(3, 3)
    result(3, 3) = this(0, 3) * m(3, 0) + this(1, 3) * m(3, 1) + this(2, 3) * m(3, 2) + this(3, 3) * m(3, 3)

    result
  }

  def *=(m: Mat4): Unit = {
    val result = this * m
    this.set(result)
  }

  override def toString(): String = {
    f"""
      |[${values(0)}%10.5f][${values(4)}%10.5f][${values(8)}%10.5f][${values(12)}%10.5f]
      |[${values(1)}%10.5f][${values(5)}%10.5f][${values(9)}%10.5f][${values(13)}%10.5f]
      |[${values(2)}%10.5f][${values(6)}%10.5f][${values(10)}%10.5f][${values(14)}%10.5f]
      |[${values(3)}%10.5f][${values(7)}%10.5f][${values(11)}%10.5f][${values(15)}%10.5f]""".stripMargin
  }
}
