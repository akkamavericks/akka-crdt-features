package nl.about42.scalagl.math

import java.nio.FloatBuffer

import org.lwjgl.BufferUtils

object Vec3 {
  def apply(value: Float = 0.0f): Vec3 = {
    val result = new Vec3
    result.set(value, value, value)
    result
  }

  def apply(nx: Float, ny: Float, nz: Float): Vec3 = {
    val result = new Vec3
    result.set(nx, ny, nz)
    result
  }

  def apply(nx: Double, ny: Double, nz: Double): Vec3 = {
    apply(nx.toFloat, ny.toFloat, nz.toFloat)
  }

  def apply(v: Vec3): Vec3 = {
    val result = new Vec3
    result.set(v)
    result
  }
}

class Vec3 extends FloatOperations {
  val values = Array.fill(3)(0.0f)

  def x: Float = values(0)
  def y: Float = values(1)
  def z: Float = values(2)

  def update(index: Int, value: Float): Unit = {
    assert(index >= 0 && index <= 2)
    values(index) = value
  }

  def -(v: Vec3): Vec3 = {
    Vec3(this.x - v.x, this.y - v.y, this.z - v.z)
  }

  def +(v: Vec3): Vec3 = {
    Vec3(this.x + v.x, this.y + v.y, this.z + v.z)
  }

  def +=(v: Vec3): Unit = {
    values(0) += v.x
    values(1) += v.y
    values(2) += v.z
  }

  def +=(x: Float, y: Float, z: Float): Unit = {
    values(0) += x
    values(1) += y
    values(2) += z
  }

  // dot product
  def *(v: Vec3): Float = {
    values(0) * v.x + values(1) * v.y + values(2) * v.z
  }

  // scale with scalar
  def *(scalar: Float) = {
    Vec3(this.x * scalar, this.y * scalar, this.z * scalar)
  }

  // cross product
  def X(v: Vec3): Vec3 = {
    Vec3(values(1) * v.z - values(2) * v.y, values(2) * v.x - values(0) * v.z, values(0) * v.y - values(1) * v.x)
  }

  def *=(scalar: Float): Unit = {
    values(0) *= scalar
    values(1) *= scalar
    values(2) *= scalar
  }

  def set(x: Float, y: Float, z: Float) = {
    values(0) = x
    values(1) = y
    values(2) = z
  }

  def set(v: Vec3) = {
    values(0) = v.x
    values(1) = v.y
    values(2) = v.z
  }

  def lengthSquared: Float = x*x + y*y + z*z
  def length: Float = Math.sqrt(lengthSquared).toFloat

  def normalize(): Unit = {
    val lengthSq = lengthSquared
    if (nonZero(lengthSq)){
      val length = Math.sqrt(lengthSq).toFloat
      values(0) /= length
      values(1) /= length
      values(2) /= length
    }
  }

  def scale(f: Float): Unit = {
    values(0) *= f
    values(1) *= f
    values(2) *= f
  }

  def getBuffer: FloatBuffer = {
    val buffer = BufferUtils.createFloatBuffer(3)
    values.foreach(buffer.put)
    buffer.flip()
    buffer
  }

  override def toString(): String = f"""[${values(0)}%10.5f ${values(1)}%10.5f ${values(2)}%10.5f]"""
}
