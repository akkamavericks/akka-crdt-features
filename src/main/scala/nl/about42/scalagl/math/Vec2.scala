package nl.about42.scalagl.math

import java.nio.FloatBuffer
import org.lwjgl.BufferUtils

object Vec2 {
  def apply(value: Float = 0.0f): Vec2 = {
    val result = new Vec2
    result.set(value, value)
    result
  }

  def apply(nx: Float, ny: Float): Vec2 = {
    val result = new Vec2
    result.set(nx, ny)
    result
  }

  def apply(v: Vec2): Vec2 = {
    val result = new Vec2
    result.set(v)
    result
  }
}

class Vec2 extends FloatOperations {
  val values = Array.fill(2)(0.0f)

  def x: Float = values(0)
  def y: Float = values(1)

  def update(index: Int, value: Float): Unit = {
    assert(index >= 0 && index <= 1)
    values(index) = value
  }

  def -(v: Vec2): Vec2 = {
    Vec2(this.x - v.x, this.y - v.y)
  }

  def +(v: Vec2): Vec2 = {
    Vec2(this.x + v.x, this.y + v.y)
  }

  def +=(v: Vec2): Unit = {
    values(0) += v.x
    values(1) += v.y
  }

  def +=(x: Float, y: Float): Unit = {
    values(0) += x
    values(1) += y
  }

  // dot product
  def *(v: Vec2): Float = {
    values(0) * v.x + values(1) * v.y
  }

  // scale with scalar
  def *(scalar: Float) = {
    Vec2(this.x * scalar, this.y * scalar)
  }

  def *=(scalar: Float): Unit = {
    values(0) *= scalar
    values(1) *= scalar
  }

  def set(x: Float, y: Float) = {
    values(0) = x
    values(1) = y
  }

  def set(v: Vec2) = {
    values(0) = v.x
    values(1) = v.y
  }

  def lengthSquared: Float = x*x + y*y
  def length: Float = Math.sqrt(lengthSquared).toFloat

  def normalize(): Unit = {
    val lengthSq = lengthSquared
    if (nonZero(lengthSq)){
      val length = Math.sqrt(lengthSq).toFloat
      values(0) /= length
      values(1) /= length
    }
  }

  def scale(f: Float): Unit = {
    values(0) *= f
    values(1) *= f
  }

  def getBuffer: FloatBuffer = {
    val buffer = BufferUtils.createFloatBuffer(2)
    values.foreach(buffer.put)
    buffer.flip()
    buffer
  }

  override def toString(): String = f"""[${values(0)}%10.5f ${values(1)}%10.5f]"""

}
