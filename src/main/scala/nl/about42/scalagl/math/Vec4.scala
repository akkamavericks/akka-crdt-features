package nl.about42.scalagl.math

import java.nio.FloatBuffer

import org.lwjgl.BufferUtils

object Vec4 extends ColorConversions {
  def apply(nx: Float, ny: Float, nz: Float, nw: Float): Vec4 = {
    val result = new Vec4
    result.set(nx, ny, nz, nw)
    result
  }
  def apply(v: Vec3, w: Float): Vec4 = {
    val result = new Vec4
    result.set(v.x, v.y, v.z, w)
    result
  }
  def apply(value: Float = 0.0f): Vec4 = {
    val result = new Vec4
    result.set(value, value, value, value)
    result
  }

  def lerp(v1: Vec4, v2: Vec4, delta: Float): Vec4 = {
    val cd = clamp(delta, 0, 1)
    Vec4(
      v1.r * (1 - cd) + v2.r * cd,
      v1.g * (1 - cd) + v2.g * cd,
      v1.b * (1 - cd) + v2.b * cd,
      v1.a * (1 - cd) + v2.a * cd
    )
  }
}

class Vec4 {
  val values = Array.fill(4)(0.0f)

  def x: Float = values(0)
  def y: Float = values(1)
  def z: Float = values(2)
  def w: Float = values(3)

  def r: Float = values(0)
  def g: Float = values(1)
  def b: Float = values(2)
  def a: Float = values(3)

  def apply(index: Int): Float = {
    values(index)
  }

  def update(index: Int, value: Float): Unit = {
    assert(index >= 0 && index <= 3)
    values(index) = value
  }

  def set(x: Float, y: Float, z: Float, w: Float) = {
    values(0) = x
    values(1) = y
    values(2) = z
    values(3) = w
  }

  def set(v: Vec4) = {
    values(0) = v.x
    values(1) = v.y
    values(2) = v.z
    values(3) = v.w
  }

  def getBuffer: FloatBuffer = {
    val buffer = BufferUtils.createFloatBuffer(4)
    values.foreach(buffer.put)
    buffer.flip()
    buffer
  }

  override def toString(): String = f"""[${values(0)}%10.5f ${values(1)}%10.5f ${values(2)}%10.5f ${values(3)}%10.5f]"""

}
