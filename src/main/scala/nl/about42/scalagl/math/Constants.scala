package nl.about42.scalagl.math

object Constants {
  val PI = Math.PI.toFloat
  val TWOPI = PI * 2.0f
  val HALFPI = PI / 2.0f

  val EPSILON = Math.ulp(0)

}
