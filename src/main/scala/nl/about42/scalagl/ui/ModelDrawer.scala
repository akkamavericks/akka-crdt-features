package nl.about42.scalagl.ui

import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.Window
import nl.about42.scalagl.math.{MatrixOperations, Mat4, Vec3}
import nl.about42.scalagl.model.Model
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.texture.Texture
import org.lwjgl.opengl.{GL11, GL15, GL20, GL30}

object ModelDrawer {
  val vertexShader: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |uniform mat4 modelMatrix;
      |
      |layout(location = 0) in vec3 in_Position;
      |layout(location = 1) in vec3 in_Normal;
      |layout(location = 2) in vec2 in_Texture;
      |
      |out vec2 pass_TextureCoord;
      |out vec3 pass_Normal;
      |
      |void main() {
      |	 gl_Position = vec4(in_Position, 1.0);
      |	 // Override gl_Position with our new calculated position
      |	 gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Position;
      |
      |
      |	 pass_TextureCoord = in_Texture;
      |  pass_Normal = (modelMatrix * vec4(in_Normal, 1.0)).xyz;
      |}
    """.stripMargin

  val fragmentShader: String =
    """
      |#version 330 core
      |
      |in vec3 pass_Normal;
      |in vec2 pass_TextureCoord;
      |
      |uniform sampler2D uTexture;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  //FragColor = vec4(pass_Normal, 1.0);
      |  FragColor = texture(uTexture, pass_TextureCoord);
      |  if (FragColor.a <= 0.02f)
      |     discard;
      |}
    """.stripMargin

  lazy val modelProgram = ShaderProgram.buildProgram(ModelDrawer.vertexShader, ModelDrawer.fragmentShader, "modelMatrix")
}

class ModelDrawer(model: Model, texture: Texture, meshIndex: Int = 0) extends Drawable with MatrixOperations with LazyLogging {

  override def initialize(window: Window): Unit = {
    logger.info(s"Modeldrawer init called. Current scene has: ${model.scene.mNumMeshes()} meshes and ${model.meshes.size} parsed meshes")
  }

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    super.render(window, camera, wallTime, frameCount, delta, parentModelMat)

    val modelProgram = ModelDrawer.modelProgram

    modelProgram.use()

    modelProgram.setMatrix4f(modelProgram.modelMatrixName, false, modelMatrix)
    camera.uploadProjectionMatrix(modelProgram, "projectionMatrix")
    camera.uploadViewMatrix(modelProgram, "viewMatrix")
    logOnGLError("Error in use of modelProgram, or setMatrix4f calls")

    texture.bindTexture(GL11.GL_TEXTURE_2D)
    logOnGLError("bindtexture")

    val tempModel: Mat4 = Mat4.identity * modelMatrix
    // draw mesh
    val m = model.meshes(meshIndex)

    GL30.glBindVertexArray(m.vertexArrayObject.vaoId)
    logOnGLError("bindvertexarray")
    GL20.glEnableVertexAttribArray(0)
    logOnGLError("enablevertexattribarray(0)")
    GL20.glEnableVertexAttribArray(1)
    logOnGLError("enablevertexattribarray(1)")
    GL20.glEnableVertexAttribArray(2)
    logOnGLError("enablevertexattribarray(2)")

    // draw using the indices
    val faceCount = m.numFaces
    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, m.vertexArrayObject.indexBufferId)
    logOnGLError("glBindBuffer")

    GL11.glDrawElements(GL11.GL_TRIANGLES, faceCount * 3, GL11.GL_UNSIGNED_INT, 0)
    logOnGLError("glDrawElements")

    // Put everything back to default (deselect)
    GL20.glDisableVertexAttribArray(0)
    GL20.glDisableVertexAttribArray(1)
    GL20.glDisableVertexAttribArray(2)
    GL30.glBindVertexArray(0)

    tempModel *= translate(Vec3(0, 0, -40))
    modelProgram.setMatrix4f(modelProgram.modelMatrixName, false, tempModel)

    modelProgram.stopUsing()
  }
}
