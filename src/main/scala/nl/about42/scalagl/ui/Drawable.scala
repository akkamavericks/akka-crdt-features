package nl.about42.scalagl.ui

import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl
import nl.about42.scalagl.math.{Mat4, MatrixOperations, Vec3}
import nl.about42.scalagl.{RENDER_MODE_DEBUG, RENDER_MODE_NORMAL, RenderMode, Window}

import scala.collection.mutable.ListBuffer

object Drawable extends MatrixOperations {
  sealed trait DrawMode {
    def mode: Int = 0
  }
  case object DRAW_WORLD extends DrawMode {
    override def mode = 1
  }
  case object DRAW_BILLBOARD_VARIABLE extends DrawMode{
    override def mode = 2
  }
  case object DRAW_BILLBOARD_FIXED extends DrawMode{
    override def mode = 3
  }
  case object DRAW_BILLBOARD_FIXED_ONTOP extends DrawMode{
    override def mode = 4
  }

  val identity: Mat4 = Mat4.identity

  val debugAxis = new scalagl.ui.AxisDrawer()
  val debugAxisRoot = new scalagl.ui.AxisRootDrawer()

  private val billboardRotMatrix: Mat4 = Mat4()
  def setMatrix(target: Mat4, worldPos: Vec3, modelMatrix: Mat4, drawMode: DrawMode, camUp: Vec3, camRight: Vec3, camPos: Vec3) = {
    drawMode match {
      case DRAW_WORLD =>
        target.set(modelMatrix)

      case DRAW_BILLBOARD_VARIABLE =>
        loadPosition(billboardRotMatrix, Vec3(), camUp, camRight)
        loadTranslate(target, worldPos)
        target *= billboardRotMatrix

      case DRAW_BILLBOARD_FIXED =>
        loadPosition(billboardRotMatrix, Vec3(), camUp, camRight)
        loadTranslate(target, worldPos)
        target *= billboardRotMatrix

        val scaleFactor = (worldPos - camPos).length / 40.0f
        target *= scale(scaleFactor)

      case DRAW_BILLBOARD_FIXED_ONTOP =>
        loadPosition(billboardRotMatrix, Vec3(), camUp, camRight)
        loadTranslate(target, worldPos)
        target *= billboardRotMatrix

        val scaleFactor = (worldPos - camPos).length / 40.0f
        target *= scale(scaleFactor)
    }
  }
}

trait Drawable extends MatrixOperations {

  val pos = Vec3()
  val up = Vec3(0, 1, 0)
  val right = Vec3(1, 0, 0)
  val scaleFactor = Vec3(1, 1, 1)

  val modelMatrix = Mat4.identity
  private val translationPart = Mat4.identity

  val activeWindows = ListBuffer[Long]()

  var renderMode: RenderMode = RENDER_MODE_NORMAL

  def drawOrder: Int = scalagl.DRAW_ORDER_REGULAR

  def attach(window: Window): Unit = {
    if (! activeWindows.contains(window.windowId)){
      activeWindows += window.windowId
    }
  }

  def detach(window: Window): Unit = {
    activeWindows -= window.windowId
  }

  def initialize(window: Window): Unit = {
    // no op
  }

  def update(wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4 = Drawable.identity): Unit = {
    // no op
  }

  def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4 = Drawable.identity): Unit = {
    if (renderMode == RENDER_MODE_DEBUG){
      renderDebug(window, camera, wallTime, frameCount, delta, parentModelMat)
    }
  }

  def cleanup(window: Window): Unit = {
    // no op
  }

  def renderMode(mode: RenderMode): Unit = {
    renderMode = mode
  }

  def scaleObject(newScale: Vec3): Unit = {
    scaleFactor.set(newScale)
    update(pos, up, right)
  }

  def position(newPos: Vec3, newUp: Vec3, newRight: Vec3): Unit = {
    pos.set(newPos)
    update(newPos, newUp, newRight)
  }

  private def update(pos: Vec3, up: Vec3, right: Vec3): Unit = {
    loadPosition(modelMatrix, pos, up, right)
    // extract the normalized right and up vectors
    right(0) = modelMatrix(0, 0)
    right(1) = modelMatrix(0, 1)
    right(2) = modelMatrix(0, 2)
    up(0) = modelMatrix(1, 0)
    up(1) = modelMatrix(1, 1)
    up(2) = modelMatrix(1, 2)

    // apply scale to the matrix
    modelMatrix  *= scale(scaleFactor)

  }

  def renderDebug(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    // draw an object local XYZ axis, and a line from parent-local origin to this origin
    Drawable.debugAxis.render(window, camera, wallTime, frameCount, delta, parentModelMat * modelMatrix)
    translationPart(3, 0) = modelMatrix(3, 0)
    translationPart(3, 1) = modelMatrix(3, 1)
    translationPart(3, 2) = modelMatrix(3, 2)
    Drawable.debugAxisRoot.render(window, camera, wallTime, frameCount, delta, parentModelMat * translationPart)
  }
}

trait DrawableNode extends Drawable {
  private val composedMatrix = Mat4.identity

  val children = new ListBuffer[Drawable]

  def addChild(drawable: Drawable, position: Vec3, up: Vec3, right: Vec3): Unit = {
    children += drawable
    drawable.position(position, up, right)
  }

  def removeChild(drawable: Drawable): Unit = {
    children -= drawable
  }

  override def update(wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    super.update(wallTime, frameCount, delta, parentModelMat)
  }

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    renderNode(window, camera, wallTime, frameCount, delta, parentModelMat)
    if (renderMode == RENDER_MODE_DEBUG){
      renderDebug(window, camera, wallTime, frameCount, delta, parentModelMat)
    }
    composedMatrix.set(parentModelMat)
    composedMatrix *= modelMatrix
    children.foreach(_.render(window, camera, wallTime, frameCount, delta, composedMatrix))
  }

  def renderNode(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit

}
