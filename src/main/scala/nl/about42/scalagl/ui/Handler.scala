package nl.about42.scalagl.ui

import java.nio.IntBuffer

import com.typesafe.scalalogging.{LazyLogging, Logger}
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl.Util.{logOnGLError, nsToMs}
import nl.about42.scalagl.streaming.StateStream
import nl.about42.scalagl.streaming.StateStream.{Frame, GLAction}
import nl.about42.scalagl.{Glfw, Window}
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFW.glfwWindowShouldClose
import org.lwjgl.opengl.{GL11, GL15, GL30, GL33}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.duration._

/* doc_start

nl.about42.scalagl.ui.Handler is a singleton that manages the UI, capable of driving multiple windows and closes
GLFW after all windows are closed.

The handler is a singleton because all OpenGL interactions have to be done via the main thread. Care must be given to the
way graphic objects are created (their initialisation typically requires OpenGL code to be executed. The proper way to
bootstrap new graphic objects is to request their creation from the handler.

Each window can contain one or more viewports (using stencil buffer to indicate which areas they draw to).

Each viewport is represented by a camera that contains information on how to display the scene(s).

A scene consists of a set of drawable objects, that get rendered in a preferred order, with one or more render passes

There is a mechanism to ask for dynamic drawing instructions via a StateStream. The render loop will place a Frame/Tick
in the stream and then pull a result (map of drawing instructions for one or more render passes) from that stream.

There are at least 4 tiers in the ordering (PREPROCESS, BACKGROUND, REGULAR, POSTPROCESS), but these values are
Integers and there is room for at least 10 other groups between each of them (PREPROCESS + 1 etc.)

Objects can be structured (for example a tree structure), and the drawing is done recursively (updating the model
matrix to position relative to the parent).

doc_end
*/
object Handler extends LazyLogging {
  type DrawableTiers = mutable.Map[Int, ListBuffer[Drawable]]

  val cameras = ListBuffer[Camera]()
  val windows = ListBuffer[Window]()
  val drawables: DrawableTiers = mutable.Map.empty
  val needsInit = ListBuffer[Drawable]()

  var renderLoopActive = false

  def isRenderLoopActive: Boolean = renderLoopActive

  def addCamera(camera: Camera): Unit = {
    // add to the list of cameras
    cameras += camera
  }

  def addDrawable(drawable: Drawable): Unit = {
    val currentTier = drawables.getOrElse(drawable.drawOrder, ListBuffer[Drawable]())
    currentTier += drawable
    drawables(drawable.drawOrder) = currentTier

    // if render loop is active, the initialize should be called for the drawable (but that must be done on the
    // render thread, not during the call to addDrawable)
    if (isRenderLoopActive) {
      needsInit += drawable
    }
  }

  def addWindow(window: Window): Unit = {
    // add to the list of windows if not yet present
    if (! windows.exists(_.windowId == window.windowId)){
      windows +=  window
    }
  }

  def initialize(): Boolean = {
    val result = Glfw.init()
    if(!result){
      logger.warn("GLFW could not initialize")
    }
    result
  }

  def removeDrawable(drawable: Drawable): Unit = {
    val currentTier = drawables.getOrElse(drawable.drawOrder, ListBuffer[Drawable]())
    currentTier -= drawable
    drawables(drawable.drawOrder) = currentTier
  }

  def removeWindow(window: Window): Unit = {
    windows.filterNot(_.windowId == window.windowId)
  }

  def shutdown(forced: Boolean = false): Unit  = {
    Glfw.terminate()
  }

  def startLoop(logger: Option[Logger], stateStreamConnectors: StateStream): Unit = {
    var fCount = 0
    var last = GLFW.glfwGetTime().toFloat
    var now = 0.0f
    var delta = 0.0f

    val timing = Array[Long](0, 0, 0, 0)
    val accum = Array[Long](0, 0, 0, 0)

    // initialize
    drawables.foreach( kv => {
      kv._2.foreach( drawable => {
        windows.foreach(w => drawable.initialize(w))
      })
    })

    // open windows if they are not yet visible
    windows.foreach(w => {
      if (GLFW.glfwGetWindowAttrib(w.windowId, GLFW.GLFW_VISIBLE) == GLFW.GLFW_FALSE){
        w.open()
      }
    })

    GL11.glEnable(GL11.GL_BLEND)
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
    
    val queryIds: IntBuffer = BufferUtils.createIntBuffer(2)
    GL15.glGenQueries(queryIds)
    logOnGLError("Error getting queryIds")

    // actual render loop
    renderLoopActive = true
    while (anyWindowLeft){

      if (!needsInit.isEmpty){
        windows.foreach(w =>
          needsInit.foreach(_.initialize(w))
        )
        needsInit.clear()
      }

      val drawableTiers = drawables.keys.toList.sorted
      now = GLFW.glfwGetTime().toFloat
      delta = now - last
      last = now
      fCount += 1

      timing(0) = System.nanoTime()

      val frame = Frame(now, delta, fCount)

      // offer a frame tick to the stream ...
      stateStreamConnectors.source.offer(frame)

      // call update on the direct drawables
      drawableTiers.foreach(t =>{
        drawables(t).foreach(_.update(now, fCount, delta))
      })

      logOnGLError("Error during update")

      // ... and pull the results from the stream
      val state = Await.result(stateStreamConnectors.sink.pull(), 968.milliseconds)

      val actions: Map[Int, List[GLAction]] = state match {
        case None =>
          logger.foreach(_.debug(s"No actions received in time for frame $frame"))
          Map.empty
        case Some(s) =>
          s.actions
      }

      val actionTiers = actions.keys.toList
      val completeTiers = (drawableTiers.toSet ++ actionTiers.toSet).toList.sorted

      // prepare for drawing the current frame
      timing(1) = System.nanoTime()

      GL15.glBeginQuery(GL33.GL_TIME_ELAPSED, queryIds.get(0))
      GL15.glBeginQuery(GL30.GL_PRIMITIVES_GENERATED, queryIds.get(1))
      logOnGLError("Error starting GL Timer")

      completeTiers.foreach(t => {
        drawables.get(t).foreach(_.foreach(drawable => {
          windows.foreach(w => {
              cameras.foreach(c => {
                drawable.render(w, c, now, fCount, delta)
              })
          })
        }))
        actions.get(t).foreach(_.foreach(a => {
          windows.foreach(w => {
            cameras.foreach(c => {
              a(w, c, frame)
            })
          })
        }))
      })

      GL15.glEndQuery(GL33.GL_TIME_ELAPSED)
      GL15.glEndQuery(GL30.GL_PRIMITIVES_GENERATED)

      timing(2) = System.nanoTime()

      // see if there are UI events to process
      GLFW.glfwPollEvents()

      // show the frame
      windows.foreach(w => {
        GLFW.glfwSwapBuffers(w.windowId)
      })

      // determine how fast we were
      val glTime = GL15.glGetQueryObjecti(queryIds.get(0), GL15.GL_QUERY_RESULT)
      val glPrimitiveCount = GL15.glGetQueryObjecti(queryIds.get(1), GL15.GL_QUERY_RESULT)
      logOnGLError("Error getting GL Timer result")

      if (fCount > 0) {
        accum(0) += (timing(2) - timing(0))
        accum(1) += (timing(1) - timing(0))
        accum(2) += (timing(2) - timing(1))
        accum(3) += glTime
        if (fCount % 50 == 0 || fCount == 1) {
          windows.foreach(w => {
            w.setTitle(s" - ${glPrimitiveCount} primitives in ${nsToMs(glTime)}ms")
          })
          if (fCount % 500 == 0 || fCount == 1) {
            logger.foreach(_.info(s"Drawing frame ${fCount}: ${glPrimitiveCount} primitives in ${nsToMs(glTime)}ms (delta $delta) ${nsToMs(timing(2) - timing(0))}ms - update ${nsToMs(timing(1) - timing(0))}ms, render ${nsToMs(timing(2) - timing(1))}ms"))
            logger.foreach(_.info(s"Accumulated times per frame: glTime ${nsToMs(accum(0) / fCount)}ms - total ${nsToMs(accum(0) / fCount)}ms - update ${nsToMs(accum(1) / fCount)}ms, render ${nsToMs(accum(2) / fCount)}ms"))
          }
        }
      }

    }
    renderLoopActive = false

    // cleanup
    drawables.foreach(kv => {
      kv._2.foreach(drawable => {
        windows.foreach(w => drawable.cleanup(w))
      })
    })

  }

  private def anyWindowLeft: Boolean = {
    // first see if any window needs to close
    windows.foreach(w =>{
      if(glfwWindowShouldClose(w.windowId)){
        w.cleanupAndDestroy()
        windows -= w
      }
    })
    // signal if there are non-closing windows left
    windows.exists(w => !glfwWindowShouldClose(w.windowId))
  }
}
