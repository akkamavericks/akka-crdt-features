package nl.about42.scalagl.ui
import java.nio.FloatBuffer

import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.Window
import nl.about42.scalagl.math.{FloatOperations, Mat4, MatrixOperations, Vec3}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.{GL11, GL15, GL20, GL30}

object AxisDrawer {
  val vertexShader: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |uniform mat4 modelMatrix;
      |
      |layout(location = 0) in vec3 in_Position;
      |
      |out vec3 pass_TextureCoord;
      |
      |void main() {
      |  gl_Position = vec4(in_Position, 1.0);
      |	 gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Position;
      |
      |	 pass_TextureCoord = in_Position;
      |}
    """.stripMargin

  val fragmentShader: String =
    """
      |#version 330 core
      |
      |in vec3 pass_TextureCoord;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  vec3 absCoord = vec3(abs(pass_TextureCoord));
      |
      |  FragColor = vec4(normalize(absCoord), 1.0);
      |}
    """.stripMargin

  lazy val program = ShaderProgram.buildProgram(AxisDrawer.vertexShader, AxisDrawer.fragmentShader, "modelMatrix")

}

class AxisDrawer(repeats: Int = 1) extends Drawable with MatrixOperations with LazyLogging {

  val myModel: Mat4 = Mat4.identity
  val scaleMat: Mat4 = scale(10.0f)

  val axisVertices: Array[Float] = Array(
    // X axis
    -1.0, 0.1, 0.0,   -1.0, 0.0, 0.0,
    1.2, 0.0, 0.0,   1.1, 0.2, 0.0,    1.15, 0.1, 0.0,   1.2, 0.2, 0.0,  1.1, 0.0, 0.0,
    1.0, 0.0, 0.0,  1.0, 0.1, 0.0,  1.0, 0.0, 0.0,
    0.9, 0.0, 0.0,  0.8, 0.2, 0.0,  0.85, 0.1, 0.0,  0.9, 0.2, 0.0,   0.8, 0.0, 0.0,  0.0, 0.0, 0.0,
    // Y axis
    0.0, -1.0, 0.0,   0.1, -1.0, 0.0,   0.0, -1.0, 0.0,
    0.0, 1.3, 0.0,    0.05, 1.2, 0.0,  0.1, 1.3, 0.0,  0.05, 1.2, 0.0,  0.05, 1.1, 0.0,  0.05, 1.2, 0.0,  0.0, 1.3, 0.0,
    0.0, 1.0, 0.0,    0.1, 1.0, 0.0,  0.0, 1.0, 0.0,
    0.0, 0.9, 0.0,    0.05, 0.8, 0.0,   0.1, 0.9, 0.0,  0.05, 0.8, 0.0,  0.05, 0.7, 0.0,  0.05, 0.8, 0.0,  0.0, 0.9, 0.0,  0, 0, 0,
    // Z axis
    0, 0, -1,  0, 0.1, -1, 0, 0, -1,  0, 0, 1.2,  0, 0.2, 1.1,  0, 0.2, 1.2,  0, 0.2, 1.1,  0, 0, 1.2,  0, 0, 1.0,  0, 0.1, 1.0,  0, 0, 1.0,  0, 0, 0.9,  0, 0.2, 0.8,  0, 0.2, 0.9
  ).map(_.toFloat)

  val axisVertexCount = axisVertices.size / 3

  var vaoId: Int = 0
  var vboId: Int = 0

  val verticesBuffer: FloatBuffer = BufferUtils.createFloatBuffer(axisVertices.length)

  private def init() = {
    if (vaoId == 0) {
      vaoId = GL30.glGenVertexArrays()
      GL30.glBindVertexArray(vaoId)

      verticesBuffer.put(axisVertices)
      verticesBuffer.flip()

      vboId = GL15.glGenBuffers()
      GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId)
      GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW)
      GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0)

      GL30.glBindVertexArray(0)
      logOnGLError("Problem creating axis array buffer")
    }
  }

  override def initialize(window: Window): Unit = {
    init()
  }

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    logOnGLError("Error in earlier code")
    init()

    val program = AxisDrawer.program

    program.use()
    logOnGLError("Error glUseProgram")

    myModel.set(modelMatrix)
    camera.uploadViewMatrix(program, "viewMatrix")
    camera.uploadProjectionMatrix(program, "projectionMatrix")
    logOnGLError("Error uploading matrices")

    GL30.glBindVertexArray(vaoId)
    logOnGLError("Error bindvertexarray")
    GL20.glEnableVertexAttribArray(0)
    logOnGLError("Error in enablevertexattribarray(0)")

    // Draw the vertices (repeats times, each time scaling by factor 10)
    for(_ <- 1 to repeats){
      program.setMatrix4f(program.modelMatrixName, false, parentModelMat * myModel)
      GL11.glDrawArrays(GL11.GL_LINE_STRIP, 0, axisVertexCount)
      logOnGLError("Error in drawarrays")

      myModel *= scaleMat
    }

    // Put everything back to default (deselect)
    GL20.glDisableVertexAttribArray(0)
    GL30.glBindVertexArray(0)

    program.stopUsing()

    if (frameCount % 500 == 1){
      logger.info("AxisDrawer render is being called")
    }

  }

  override def cleanup(window: Window): Unit = {
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)
    GL15.glDeleteBuffers(vboId)
    GL30.glBindVertexArray(0)
    GL30.glDeleteVertexArrays(vaoId)
  }
}

class AxisRootDrawer extends Drawable with FloatOperations {
  val rootVertices: Array[Float] = Array(
    // Start at origin
    0, 0, 0,
    // point to axisOrigin (will be updated before use)
    0, 0, 0
  ).map(_.toFloat)
  val rootVertexCount = rootVertices.size / 3

  var vaoRootId: Int = 0
  var vboRootId: Int = 0
  val verticesBuffer: FloatBuffer = BufferUtils.createFloatBuffer(rootVertices.length)

  private def init() = {
    if (vaoRootId == 0) {
      vaoRootId = GL30.glGenVertexArrays()
      GL30.glBindVertexArray(vaoRootId)

      verticesBuffer.put(rootVertices)
      verticesBuffer.flip()
      vboRootId = GL15.glGenBuffers()
      GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboRootId)
      GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_DYNAMIC_DRAW)
      GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0)

      GL30.glBindVertexArray(0)
      logOnGLError("Problem creating axis array buffer")
    }
  }

  override def initialize(window: Window): Unit = {
    init()
  }

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    logOnGLError("Error in earlier code")
    init()

    val program = AxisDrawer.program

    program.use()
    logOnGLError("Error glUseProgram")

    camera.uploadViewMatrix(program, "viewMatrix")
    camera.uploadProjectionMatrix(program, "projectionMatrix")
    logOnGLError("Error uploading matrices")

    // draw the root-connector (if non-zero)
    val translationVector = Vec3(parentModelMat(3, 0), parentModelMat(3, 1), parentModelMat(3, 2))
    val distance = translationVector.length

    if (nonZero(distance)) {
      // draw a simple line from parent origin to my origin
      rootVertices(0) = - translationVector.x
      rootVertices(1) = - translationVector.y
      rootVertices(2) = - translationVector.z
      verticesBuffer.put(rootVertices)
      verticesBuffer.flip()

      GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboRootId)
      GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW)

      GL30.glBindVertexArray(vaoRootId)
      GL20.glEnableVertexAttribArray(0)
      program.setMatrix4f(program.modelMatrixName, false, parentModelMat)
      GL11.glDrawArrays(GL11.GL_LINE_STRIP, 0, rootVertexCount)
      logOnGLError("Error rendering root line")

    }

    // Put everything back to default (deselect)
    GL20.glDisableVertexAttribArray(0)
    GL30.glBindVertexArray(0)

    program.stopUsing()

  }

  override def cleanup(window: Window): Unit = {
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)
    GL15.glDeleteBuffers(vboRootId)
    GL30.glBindVertexArray(0)
    GL30.glDeleteVertexArrays(vaoRootId)
  }
}