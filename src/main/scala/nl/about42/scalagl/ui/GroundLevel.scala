package nl.about42.scalagl.ui

import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.Window
import nl.about42.scalagl.math.{Mat4, Vec2, Vec3}
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.texture.Texture
import nl.about42.scalagl.vertexarray.VertexArrayObject
import org.lwjgl.opengl.{GL11, GL15, GL20, GL30}

object GroundLevel {
  val vertexShader: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |uniform mat4 modelMatrix;
      |
      |layout(location = 0) in vec3 in_Position;
      |layout(location = 1) in vec3 in_Normal;
      |layout(location = 2) in vec2 in_Texture;
      |
      |out vec2 pass_TextureCoord;
      |out vec3 pass_Normal;
      |
      |void main() {
      |	 gl_Position = vec4(in_Position, 1.0);
      |	 // Override gl_Position with our new calculated position
      |	 gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Position;
      |
      |
      |	 pass_TextureCoord = in_Texture;
      |  pass_Normal = (modelMatrix * vec4(in_Normal, 1.0)).xyz;
      |}
    """.stripMargin

  val fragmentShader: String =
    """
      |#version 330 core
      |
      |in vec3 pass_Normal;
      |in vec2 pass_TextureCoord;
      |
      |uniform sampler2D uTexture;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  FragColor = texture(uTexture, pass_TextureCoord);
      |  if (FragColor.a <= 0.02f)
      |     discard;
      |}
    """.stripMargin

  lazy val groundLevelProgram: ShaderProgram = ShaderProgram.buildProgram(GroundLevel.vertexShader, GroundLevel.fragmentShader, "modelMatrix")
}
class GroundLevel(texture: Texture, scaleX: Float, scaleZ: Float, height: Float = 0.0f) extends Drawable {
  var vertexArrayObject: VertexArrayObject = null

  override def initialize(window: Window): Unit = {
    super.initialize(window)

    val texX = scaleX
    val texZ = scaleZ
    vertexArrayObject = VertexArrayObject.create(
      Seq(Vec3(-1000, height, 1000), Vec3(1000, height, 1000), Vec3(-1000, height, -1000), Vec3(1000, height, -1000)),
      Seq(Vec2(-texX, -texZ), Vec2(texX, -texZ), Vec2(-texX, texZ), Vec2(texX, texZ)),
      Seq(Vec3(0, 1, 0), Vec3(0, 1, 0), Vec3(0, 1, 0), Vec3(0, 1, 0)),
      Seq(0, 1, 2, 3),
      4,
      4
    )
  }

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    import GroundLevel.groundLevelProgram

    super.render(window, camera, wallTime, frameCount, delta, parentModelMat)

    groundLevelProgram.use()

    groundLevelProgram.setMatrix4f(groundLevelProgram.modelMatrixName, false, modelMatrix)
    camera.uploadProjectionMatrix(groundLevelProgram, "projectionMatrix")
    camera.uploadViewMatrix(groundLevelProgram, "viewMatrix")
    logOnGLError("Error in use of modelProgram, or setMatrix4f calls")

    texture.bindTexture(GL11.GL_TEXTURE_2D)
    logOnGLError("bindtexture")

    GL30.glBindVertexArray(vertexArrayObject.vaoId)
    logOnGLError("bindvertexarray")
    GL20.glEnableVertexAttribArray(0)
    logOnGLError("enablevertexattribarray(0)")
    GL20.glEnableVertexAttribArray(1)
    logOnGLError("enablevertexattribarray(1)")
    GL20.glEnableVertexAttribArray(2)
    logOnGLError("enablevertexattribarray(2)")

    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vertexArrayObject.indexBufferId)
    logOnGLError("glBindBuffer")

    GL11.glDrawElements(GL11.GL_TRIANGLE_STRIP, 4, GL11.GL_UNSIGNED_INT, 0)
    logOnGLError("glDrawElements")

    GroundLevel.groundLevelProgram.stopUsing()
  }
}
