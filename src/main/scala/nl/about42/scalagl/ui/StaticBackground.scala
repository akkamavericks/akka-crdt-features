package nl.about42.scalagl.ui
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.Window
import nl.about42.scalagl.math.{Mat4, Vec4}
import org.lwjgl.opengl.GL11

class StaticBackground(clearColor: Vec4) extends Drawable {

  override def drawOrder: Int = scalagl.DRAW_ORDER_BACKGROUND

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    GL11.glEnable(GL11.GL_DEPTH_TEST)
    GL11.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a)
    logOnGLError("Error glClearColor")
    GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT)
    logOnGLError("Error glClear")
  }
}
