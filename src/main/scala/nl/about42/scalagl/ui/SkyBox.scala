package nl.about42.scalagl.ui

import java.nio.FloatBuffer

import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.Window
import nl.about42.scalagl.math.Mat4
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.texture.CubeMapTexture
import org.lwjgl.BufferUtils
import org.lwjgl.opengl._

object SkyBox {
  val vertexShader: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |
      |layout(location = 0) in vec3 in_Position;
      |
      |out vec3 pass_TextureCoord;
      |
      |void main() {
      |  gl_Position = vec4(in_Position, 1.0);
      |  vec4 pos = projectionMatrix * viewMatrix * gl_Position;
      |  gl_Position = pos.xyww;
      |
      |	 pass_TextureCoord = in_Position;
      |  pass_TextureCoord.z = -1.0 * pass_TextureCoord.z;
      |}
    """.stripMargin

  val fragmentShader: String =
    """
      |#version 330 core
      |
      |in vec3 pass_TextureCoord;
      |
      |uniform samplerCube uTexture;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  FragColor = texture(uTexture, pass_TextureCoord);
      |}
    """.stripMargin

  def create(cubeTexture: CubeMapTexture): SkyBox = {

    val vaoId = GL30.glGenVertexArrays()

    val vertices: Array[Float] = Array(
      // counter clockwise when looking from inside the cube
      // back
      -1, -1,  1,    1,  1,  1,    1, -1,  1,
      -1, -1,  1,   -1,  1,  1,    1,  1,  1,
      // front
      -1, -1, -1,    1,  1, -1,    1, -1, -1,
      -1, -1, -1,   -1,  1, -1,    1,  1, -1,
      // left
      -1, -1,  1,   -1, -1, -1,   -1,  1, -1,
      -1, -1,  1,   -1,  1, -1,   -1,  1,  1,
      // right
       1, -1, -1,    1, -1,  1,    1,  1,  1,
       1, -1, -1,    1,  1,  1,    1,  1, -1,
      // top
      -1,  1, -1,    1,  1, -1,    1,  1,  1,
      -1,  1, -1,    1,  1,  1,   -1,  1,  1,
      // bottom
      -1, -1,  1,    1, -1,  1,    1, -1, -1,
      -1, -1,  1,    1, -1, -1,   -1, -1, -1
    ).map(_.toFloat)

    val verticesBuffer: FloatBuffer = BufferUtils.createFloatBuffer(vertices.length)
    verticesBuffer.put(vertices)
    verticesBuffer.flip()
    val vertexCount = vertices.size / 3

    GL30.glBindVertexArray(vaoId)
    val vboId = GL15.glGenBuffers()
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId)
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW)
    GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0)
    GL30.glBindVertexArray(0)

    new SkyBox(cubeTexture, vaoId, vertexCount)
  }
}

class SkyBox(val cubeTexture: CubeMapTexture, val vertexArrayObj: Int, vertexCount: Int) extends Drawable with LazyLogging{

  val skyBoxProgram: ShaderProgram = ShaderProgram.buildProgram(SkyBox.vertexShader, SkyBox.fragmentShader, "modelMatrix")

  val viewMat: Mat4 = Mat4.identity

  override def drawOrder: Int = scalagl.DRAW_ORDER_BACKGROUND

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    skyBoxProgram.use()

    // set view matrix to the camera view matrix without translation component
    camera.exportViewMatrix(viewMat)
    viewMat(0, 3) = 0.0f
    viewMat(1, 3) = 0.0f
    viewMat(2, 3) = 0.0f
    viewMat(3, 0) = 0.0f
    viewMat(3, 1) = 0.0f
    viewMat(3, 2) = 0.0f
    viewMat(3, 3) = 0.0f

    skyBoxProgram.setMatrix4f("viewMatrix", false, viewMat)
    camera.uploadProjectionMatrix(skyBoxProgram, "projectionMatrix")

    logOnGLError("Error glUseProgram or setMatrix4f calls")

    GL11.glEnable(GL11.GL_DEPTH_TEST)

    GL11.glDepthMask(true)
    GL11.glDepthFunc(GL11.GL_ALWAYS)
    GL30.glBindVertexArray(vertexArrayObj)
    logOnGLError("Error bindvertexarray")
    GL20.glEnableVertexAttribArray(0)

    cubeTexture.bindTexture(GL13.GL_TEXTURE_CUBE_MAP)

    // Draw the vertices
    GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vertexCount)
    logOnGLError("Error in drawarrays")

    // Put everything back to default (deselect)
    GL20.glDisableVertexAttribArray(0)
    GL30.glBindVertexArray(0)

    skyBoxProgram.stopUsing()
    GL11.glDepthMask(true)
    GL11.glDepthFunc(GL11.GL_LESS)

    logOnGLError("Error in render")

  }
}
