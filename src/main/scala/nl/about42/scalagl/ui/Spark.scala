package nl.about42.scalagl.ui
import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl.math.{Bounds, Constants, Mat4, Vec3}
import nl.about42.scalagl.texture.{Card, ColorRamp, FontAtlas, Texture}
import nl.about42.scalagl.{KeyCallback, Window, math}
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL11

object Spark extends KeyCallback {
  override def keyEvent(window: Window, key: Int, action: Int, modifiers: Int, scancode: Int): Unit = {
    if (key == GLFW.GLFW_KEY_P && (action == GLFW.GLFW_PRESS || action == GLFW.GLFW_REPEAT)) {
      val now = GLFW.glfwGetTime()
      val angle = Constants.TWOPI * (now / 4)
      val direction = Vec3(Math.sin(angle ), 0, Math.cos(angle))
      Handler.addDrawable(new Spark(direction, 1, GLFW.glfwGetTime().toFloat, GLFW.glfwGetTime().toFloat + 10))
    }
  }
  val ramp = ColorRamp.RAINBOW
}

class Spark(direction: Vec3, speed: Float, timeOfBirth: Float, timeOfDeath: Float) extends DrawableNode with LazyLogging {
  var isInitialized = false
  var myCard: Card = null

  def init() = {
    if (!isInitialized) {
      logger.info("Creating children")
      //addChild(new AxisDrawer(1), Vec3(0f, 0f, 0f), Vec3(0, 1, 0), Vec3(1, 0, 0))
      val cardTex = Texture.createTexture(32, 32, math.Vec4(1, 1, 1, 1), GL11.GL_RGBA)
      val card = new Card(cardTex, math.Bounds(0f, 0f, 1, 1), Bounds(0, 0, 1, 1), FontAtlas.getFont(16))
      val cardDrawer = new CardDrawer(card)
      addChild(cardDrawer, Vec3(0, 0, 0), Vec3(0, 1, 0), Vec3(1, 0, 0))
      myCard = card
      cardDrawer.setDrawMode(Drawable.DRAW_BILLBOARD_VARIABLE)
      cardDrawer.position(Vec3(-.5f, -.5f, 0), Vec3(0, 1, 0), Vec3(1, 0, 0))
      isInitialized = true
    }
  }

  override def initialize(window: Window): Unit = {
    super.initialize(window)
    init()
  }

  override def update(wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    super.update(wallTime, frameCount, delta, parentModelMat)
    init()
    val age = (wallTime - timeOfBirth)/(timeOfDeath - timeOfBirth)
    modelMatrix *= translate(direction * delta * speed * (1f + 3 * age))
    myCard.setColor(Spark.ramp(age))
    if (wallTime > timeOfDeath ){
      Handler.removeDrawable(this)
    }
  }

  override def renderNode(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    // no op (child is all I show)
  }
}
