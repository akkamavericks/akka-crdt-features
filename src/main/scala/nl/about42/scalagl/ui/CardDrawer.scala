package nl.about42.scalagl.ui

import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.Window
import nl.about42.scalagl.math.{Mat4, Vec2, Vec3}
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.texture.Card
import nl.about42.scalagl.ui.CardDrawer.{CardCorners, OFFSET_NORTH_EAST, OffsetDirection}
import nl.about42.scalagl.ui.Drawable.{DRAW_BILLBOARD_FIXED_ONTOP, DRAW_WORLD, DrawMode}
import nl.about42.scalagl.vertexarray.VertexArrayObject
import org.lwjgl.opengl.{GL11, GL20, GL30}

object CardDrawer {

  val vertexShader: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |uniform mat4 modelMatrix;
      |uniform vec4 in_Color;
      |
      |layout(location = 0) in vec3 in_Position;
      |layout(location = 1) in vec3 in_Normal;
      |layout(location = 2) in vec2 in_Texture;
      |
      |out vec4 pass_Color;
      |out vec2 pass_TextureCoord;
      |out vec3 pass_Normal;
      |
      |void main() {
      |	 gl_Position = vec4(in_Position, 1.0);
      |	 // Override gl_Position with our new calculated position
      |	 gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Position;
      |
      |	 pass_Color = in_Color;
      |	 pass_TextureCoord = in_Texture;
      |  pass_Normal = (modelMatrix * vec4(in_Normal, 1.0)).xyz;
      |}
    """.stripMargin

  val fragmentShader: String =
    """
      |#version 330 core
      |
      |in vec4 pass_Color;
      |in vec3 pass_Normal;
      |in vec2 pass_TextureCoord;
      |
      |uniform sampler2D uTexture;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  if (pass_TextureCoord.x < 1e-10 && pass_TextureCoord.y < 1e-10) {
      |    FragColor = pass_Color;
      |  } else {
      |    FragColor = texture(uTexture, pass_TextureCoord) * pass_Color;
      |    if (FragColor.a <= 0.02f)
      |      discard;
      |  }
      |}
    """.stripMargin

  sealed trait OffsetDirection
  case object OFFSET_NORTH extends OffsetDirection
  case object OFFSET_NORTH_EAST extends OffsetDirection
  case object OFFSET_EAST extends OffsetDirection
  case object OFFSET_SOUTH_EAST extends OffsetDirection
  case object OFFSET_SOUTH extends OffsetDirection
  case object OFFSET_SOUTH_WEST extends OffsetDirection
  case object OFFSET_WEST extends OffsetDirection
  case object OFFSET_NORTH_WEST extends OffsetDirection

  case class CardCorners(bl: Vec3, br: Vec3, tl: Vec3, tr: Vec3, anchor: Vec3)

  /**
   * Calculates the card corner positions based on the given worldOffset that indicates the position of the
   * box edge in the offsetDirection
   *
   * @param offsetDirection compass direction as seen from the world origin. NORTH_EAST will place the bottom left
   *                        corner of the card at worldOffset
   * @param worldOffset     world position of one of the edges (midpoint for N, E, S, and W; corner for NE, SE, SW, and NW)
   * @param cardWidth       width of the card
   * @param cardHeight      height of the card
   * @param up              up vector of the textbox
   * @param right           right vector of the textbox
   * @return correct corners and anchor to use to draw the card
   */
  def cardCorners(offsetDirection: OffsetDirection, worldOffset: Vec3, cardWidth: Float, cardHeight: Float, up: Vec3, right: Vec3, camEye: Option[Vec3] = None): CardCorners = {
    up.normalize()
    right.normalize()

    val (worldWidth, worldHeight) = camEye match {
      case None =>
        (right * cardWidth, up * cardHeight)
      case Some(eye) =>
        val scale = (worldOffset - eye).length / 40.0f
        (right * cardWidth * scale, up * cardHeight * scale)
    }

    val quadBL = offsetDirection match {
      case CardDrawer.OFFSET_NORTH =>
        worldOffset - (worldWidth * 0.5f)
      case CardDrawer.OFFSET_NORTH_EAST =>
        worldOffset
      case CardDrawer.OFFSET_EAST =>
        worldOffset - (worldHeight * 0.5f)
      case CardDrawer.OFFSET_SOUTH_EAST =>
        worldOffset - worldHeight
      case CardDrawer.OFFSET_SOUTH =>
        worldOffset - (worldWidth * 0.5f) - worldHeight
      case CardDrawer.OFFSET_SOUTH_WEST =>
        worldOffset - worldWidth - worldHeight
      case CardDrawer.OFFSET_WEST =>
        worldOffset - worldWidth - (worldHeight * 0.5f)
      case CardDrawer.OFFSET_NORTH_WEST =>
        worldOffset - worldWidth
    }
    val quadBR = quadBL + worldWidth
    val quadTL = quadBL + worldHeight
    val quadTR = quadTL + worldWidth

    val anchor = offsetDirection match {
      case OFFSET_NORTH =>
        quadBL + (worldWidth * 0.5f)
      case OFFSET_NORTH_EAST =>
        quadBL
      case OFFSET_EAST =>
        quadBL + (worldHeight * 0.5f)
      case OFFSET_SOUTH_EAST =>
        quadTL
      case OFFSET_SOUTH =>
        quadTL + (worldWidth * 0.5f)
      case OFFSET_SOUTH_WEST =>
        quadTR
      case OFFSET_WEST =>
        quadBR + (worldHeight * 0.5f)
      case OFFSET_NORTH_WEST =>
        quadBR
    }
    CardCorners(quadBL, quadBR, quadTL, quadTR, anchor)
  }

  lazy val myProgram = ShaderProgram.buildProgram(CardDrawer.vertexShader, CardDrawer.fragmentShader, "modelMatrix")

}

class CardDrawer(card: Card, showRoot: Boolean = true) extends Drawable with LazyLogging {
  override def drawOrder: Int = scalagl.DRAW_ORDER_DECORATE

  var isInitialized: Boolean = false
  var myOffset: OffsetDirection = OFFSET_NORTH_EAST
  var vertexArrayObject: VertexArrayObject = null
  var myMode: DrawMode = DRAW_WORLD
  val billboardMat: Mat4 = Mat4.identity
  val camUp: Vec3 = Vec3()
  val camRight: Vec3 = Vec3()
  val camEye: Vec3 = Vec3()

  // 6 vertices: root, anchor, and 4 corners of the quad (BL, BR, TR, TL)
  val vertices = Array(
    Vec3(0, 0, 0), Vec3(0, 0, 0),
    Vec3(0, 0, 0), Vec3(1, 0, 0), Vec3(1, 1, 0), Vec3(0, 1, 0))
  val vertexCount = vertices.size

  val normals: Array[Vec3] = Array.fill(vertexCount)(Vec3(0, 0, -1))

  val texCoords = Array(
    Vec2(0, 0), Vec2(0, 0),
    Vec2(0, 0), Vec2(1, 0), Vec2(1, 1), Vec2(0, 1)
  )

  // several ways to draw stuff:
  // - line from origin to anchor (2 vertices)
  // - border (linestrip, 5 vertices)
  // - textured quad (trianglestrip, 4 vertices)
  val indices = Array(
    0, 1,
    2, 3, 4, 5, 2,
    2, 3, 5, 4
  )
  val indexCount = indices.size
  // offsets are in bytes, not number of elements
  val anchorLineOffset = 0 * 4
  val borderOffset = 2 * 4
  val quadOffset = 7 * 4

  def setDrawMode(mode: DrawMode): Unit = {
    myMode = mode
  }

  def setOffsetDirection(direction: OffsetDirection): Unit = {
    myOffset = direction
  }

  def refresh(cc: CardCorners): Unit = {
    // push the current coordinates into the vertex array
    vertices(1) = cc.anchor
    vertices(2) = cc.bl
    vertices(3) = cc.br
    vertices(4) = cc.tr
    vertices(5) = cc.tl

    // also push current texture coordinates into the vertex array
    texCoords(2) = card.textureBounds.bl
    texCoords(3) = card.textureBounds.br
    texCoords(4) = card.textureBounds.tr
    texCoords(5) = card.textureBounds.tl

    // upload updated values if vao is already available
    if (vertexArrayObject != null) {
      GL30.glBindVertexArray(vertexArrayObject.vaoId)

      vertexArrayObject.updateVertices(vertexCount, vertices)
      vertexArrayObject.updateTexCoords(vertexCount, texCoords)
    }
  }

  override def initialize(window: Window): Unit = {
    if (!isInitialized) {
      super.initialize(window)

      vertexArrayObject = VertexArrayObject.create(vertices, texCoords, normals, indices, vertexCount, indexCount)
      refresh(CardDrawer.cardCorners(myOffset, pos, card.cardBounds.width, card.cardBounds.height, up, right))
      isInitialized = true
    }
  }

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    if(!isInitialized){
      this.initialize(window)
    }
    super.render(window, camera, wallTime, frameCount, delta, parentModelMat)

    camera.exportUp(camUp)
    camera.exportRight(camRight)
    camera.exportEye(camEye)
    myMode match {
      case Drawable.DRAW_WORLD =>
        refresh(CardDrawer.cardCorners(myOffset, pos, card.cardBounds.width, card.cardBounds.height, up, right))
      case Drawable.DRAW_BILLBOARD_VARIABLE =>
        refresh(CardDrawer.cardCorners(myOffset, pos, card.cardBounds.width, card.cardBounds.height, camUp, camRight))
      case Drawable.DRAW_BILLBOARD_FIXED =>
        refresh(CardDrawer.cardCorners(myOffset, pos, card.cardBounds.width, card.cardBounds.height, camUp, camRight, Some(camEye)))
      case Drawable.DRAW_BILLBOARD_FIXED_ONTOP =>
        refresh(CardDrawer.cardCorners(myOffset, pos, card.cardBounds.width, card.cardBounds.height, camUp, camRight, Some(camEye)))
    }
    Drawable.setMatrix(billboardMat, pos, parentModelMat, myMode, camUp, camRight, camEye)

    val prog = CardDrawer.myProgram

    prog.use()

    camera.uploadProjectionMatrix(prog, "projectionMatrix")
    camera.uploadViewMatrix(prog, "viewMatrix")

    prog.setFloat4("in_Color", card.myColor)
    logOnGLError("Error setFloat")

    if (myMode == DRAW_BILLBOARD_FIXED_ONTOP) {
      GL11.glDepthFunc(GL11.GL_ALWAYS)
    } else {
      GL11.glDepthFunc(GL11.GL_LEQUAL)
    }

    card.texture.bindTexture(GL11.GL_TEXTURE_2D)
    logOnGLError("Error bindtexture")

    GL30.glBindVertexArray(vertexArrayObject.vaoId)
    logOnGLError("Error bindvertexarray")
    GL20.glEnableVertexAttribArray(0)
    logOnGLError("Error in enablevertexattribarray(0)")
    GL20.glEnableVertexAttribArray(1)
    logOnGLError("Error in enablevertexattribarray(1)")
    GL20.glEnableVertexAttribArray(2)
    logOnGLError("Error in enablevertexattribarray(2)")

    prog.setMatrix4f(prog.modelMatrixName, false, parentModelMat)

    GL11.glDrawElements(GL11.GL_TRIANGLE_STRIP, 4, GL11.GL_UNSIGNED_INT, quadOffset)

    GL20.glDisableVertexAttribArray(2)
    logOnGLError("Error in disablevertexattribarray(2)")

    if (showRoot) {
      GL11.glDrawElements(GL11.GL_LINE_STRIP, 2, GL11.GL_UNSIGNED_INT, anchorLineOffset)
    }
    GL11.glDrawElements(GL11.GL_LINE_STRIP, 5, GL11.GL_UNSIGNED_INT, borderOffset)
    logOnGLError("Error in drawarrays")

    if (myMode == DRAW_BILLBOARD_FIXED_ONTOP){
      GL11.glDepthFunc(GL11.GL_LESS)
    } else {
      GL11.glDepthFunc(GL11.GL_LESS)
    }

    prog.stopUsing()

  }

  override def position(newPos: Vec3, newUp: Vec3, newRight: Vec3): Unit = {
    super.position(newPos, newUp, newRight)
    refresh(CardDrawer.cardCorners(myOffset, pos, card.cardBounds.width, card.cardBounds.height, up, right))
  }

}
