package nl.about42.scalagl.ui

import java.nio.FloatBuffer

import nl.about42.akkamavericks.gui.Camera
import nl.about42.scalagl
import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.Window
import nl.about42.scalagl.math.{Mat4, MatrixOperations, Vec3}
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.texture.FontAtlas
import nl.about42.scalagl.ui.Drawable.{DRAW_BILLBOARD_FIXED_ONTOP, DRAW_BILLBOARD_VARIABLE, DrawMode}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.{GL11, GL15, GL20, GL30}

object TextDrawer extends MatrixOperations {

  val vertexShader: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |uniform mat4 modelMatrix;
      |uniform vec3 textColor;
      |
      |layout(location = 0) in vec3 in_Position;
      |layout(location = 1) in vec3 in_Normal;
      |layout(location = 2) in vec2 in_Texture;
      |
      |out vec2 pass_TextureCoord;
      |out vec3 pass_Normal;
      |out vec4 pass_Color;
      |
      |void main() {
      |	 gl_Position = vec4(in_Position, 1.0);
      |	 // Override gl_Position with our new calculated position
      |	 gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Position;
      |
      |	 pass_TextureCoord = in_Texture;
      |  pass_Normal = (modelMatrix * vec4(in_Normal, 1.0)).xyz;
      |  pass_Color = vec4(textColor, 1.0);
      |}
    """.stripMargin

  val fragmentShader: String =
    """
      |#version 330 core
      |
      |in vec3 pass_Normal;
      |in vec2 pass_TextureCoord;
      |in vec4 pass_Color;
      |
      |uniform sampler2D uTexture;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  vec4 col = texture(uTexture, pass_TextureCoord);
      |  if (col.a < 0.01f)
      |     discard;
      |  FragColor = col * pass_Color;
      |}
    """.stripMargin

  lazy val program = ShaderProgram.buildProgram(TextDrawer.vertexShader, TextDrawer.fragmentShader, "modelMatrix")

  val charWidth = 0.4f
  val charHeight = 1.0f
  val vertexArray = {
    for( _ <- 0 to 127)
      yield List(
      0, 0, 0,
      charWidth, 0, 0,
      0, charHeight, 0,
      charWidth, charHeight, 0
    )
  }.flatten.toArray

  val normalArray: Array[Float] = {
    for( _ <- 0 to 95)
      yield List(
        0, 0, 1,
        0, 0, 1,
        0, 0, 1,
        0, 0, 1
      )
  }.flatten.toArray.map(_.toFloat)

  val glyphHeight = 0.123f
  val glyphWidth = 1.0f / 16
  val characterOffsets: Array[Float] = {
    for(
      row <- 0 to 5;
      col <- 0 to 15
    ) yield List(
      col * glyphWidth, (row + 1) * glyphHeight,
      (col + 1) * glyphWidth, (row + 1) * glyphHeight,
      col * glyphWidth, row * glyphHeight,
      (col + 1) * glyphWidth, row * glyphHeight
    )
  }.flatten.toArray

  val nextChar: Mat4 = translate(Vec3(charWidth, 0, 0))
  val nextLine: Mat4 = translate(Vec3(0, - charHeight, 0))
}

class TextDrawer(atlas: FontAtlas) extends Drawable {
  override def drawOrder: Int = scalagl.DRAW_ORDER_POSTPROCESS

  val textProgram: ShaderProgram = TextDrawer.program

  var vaoId = 0
  var myText = ""
  var myColor = Vec3(1, 1, 0)
  var myMode: DrawMode = DRAW_BILLBOARD_VARIABLE
  val billboardMat: Mat4 = Mat4.identity
  val camUp: Vec3 = Vec3()
  val camRight: Vec3 = Vec3()
  val camEye: Vec3 = Vec3()
  val dimension: Array[Int] = Array(0, 0)
  val offset: Vec3 = Vec3()

  def setText(message: String) = {
    myText = message
  }

  def setColor(color: Vec3): Unit = {
    myColor = color
  }

  def setDrawMode(mode: DrawMode): Unit = {
    myMode = mode
  }

  override def initialize(window: Window): Unit = {
    super.initialize(window)

    // create the vertex arrays

    vaoId = GL30.glGenVertexArrays()
    GL30.glBindVertexArray(vaoId)

    val verticesBuffer: FloatBuffer = BufferUtils.createFloatBuffer(TextDrawer.vertexArray.length)
    verticesBuffer.put(TextDrawer.vertexArray)
    verticesBuffer.flip()
    val vboId = GL15.glGenBuffers()
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId)
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW)
    GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0)

    val normalsBuffer: FloatBuffer = BufferUtils.createFloatBuffer(TextDrawer.normalArray.length)
    normalsBuffer.put(TextDrawer.normalArray)
    normalsBuffer.flip()
    val vboNormalId = GL15.glGenBuffers()
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboNormalId)
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, normalsBuffer, GL15.GL_STATIC_DRAW)
    GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 0, 0)

    val texCoordBuffer: FloatBuffer = BufferUtils.createFloatBuffer(TextDrawer.characterOffsets.length)
    texCoordBuffer.put(TextDrawer.characterOffsets)
    texCoordBuffer.flip()
    val vboTexCoordId = GL15.glGenBuffers()
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboTexCoordId)
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, texCoordBuffer, GL15.GL_STATIC_DRAW)
    GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 0, 0)

  }

  override def render(window: Window, camera: Camera, wallTime: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    super.render(window, camera, wallTime, frameCount, delta, parentModelMat)

    camera.exportUp(camUp)
    camera.exportRight(camRight)
    camera.exportEye(camEye)
    Drawable.setMatrix(billboardMat, pos, modelMatrix, myMode, camUp, camRight, camEye)

    val mat = parentModelMat * billboardMat
    val line = Mat4.identity
    textProgram.use()

    camera.uploadProjectionMatrix(textProgram, "projectionMatrix")
    camera.uploadViewMatrix(textProgram, "viewMatrix")

    textProgram.setFloat3("textColor", myColor)

    if (myMode == DRAW_BILLBOARD_FIXED_ONTOP){
      GL11.glDepthFunc(GL11.GL_ALWAYS)
    }

    atlas.atlas.bindTexture(GL11.GL_TEXTURE_2D)
    GL30.glBindVertexArray(vaoId)
    logOnGLError("bindvertexarray")
    GL20.glEnableVertexAttribArray(0)
    logOnGLError("enablevertexattribarray(0)")
    GL20.glEnableVertexAttribArray(1)
    logOnGLError("enablevertexattribarray(1)")
    GL20.glEnableVertexAttribArray(2)
    logOnGLError("enablevertexattribarray(2)")

    var col: Int = 0
    dimension(0) = 0
    dimension(1) = 1
    myText.foreach(c => {
      col += 1
      if(dimension(0) < col) {
        dimension(0) = col
      }
      val index = c - 32
      if (index < 0 || index > 95) {
        c match {
          case '\n' =>
            dimension(1) += 1
            col = 0
            line *= TextDrawer.nextLine
            mat.set(parentModelMat * billboardMat * line)
          case _ =>
          // no op
        }
      } else {
        textProgram.setMatrix4f(textProgram.modelMatrixName, false, mat)
        GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, index * 4, 4)
        // move right
        mat *= TextDrawer.nextChar
      }
    })

    if (myMode == DRAW_BILLBOARD_FIXED_ONTOP){
      GL11.glDepthFunc(GL11.GL_LESS)
    }

    textProgram.stopUsing()
  }

}
