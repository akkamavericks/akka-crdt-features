package nl.about42

package object scalagl {

  type DrawOrder = Int
  val DRAW_ORDER_PREPROCESS  = 0
  val DRAW_ORDER_BACKGROUND  = 20
  val DRAW_ORDER_REGULAR     = 40
  val DRAW_ORDER_DECORATE    = 60
  val DRAW_ORDER_POSTPROCESS = 80

  sealed trait RenderMode
  object RENDER_MODE_DEBUG extends RenderMode
  object RENDER_MODE_NORMAL extends RenderMode
}
