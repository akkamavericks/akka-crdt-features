package nl.about42.scalagl.shader

case class ShaderData(version: String, vShader: String, fShader: String, gShader: Option[String]) {

  def normalMatrixName: String = ""
}
