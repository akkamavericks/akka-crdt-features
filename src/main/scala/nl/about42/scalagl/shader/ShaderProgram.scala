package nl.about42.scalagl.shader

import nl.about42.scalagl.Util
import nl.about42.scalagl.math.{Mat4, Vec3, Vec4}
import org.lwjgl.opengl.{GL11, GL20, GL32}

object ShaderProgram {

  val defaultVertex: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |uniform mat4 modelMatrix;
      |
      |layout(location = 0) in vec3 in_Position;
      |layout(location = 1) in vec4 in_Color;
      |
      |out vec4 pass_Color;
      |
      |void main() {
      |	 gl_Position = vec4(in_Position, 1.0);
      |	 // Override gl_Position with our new calculated position
      |	 gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Position;
      |
      |	 pass_Color = in_Color;
      |}
    """.stripMargin

  val defaultFragment: String =
    """
      |#version 330 core
      |
      |in vec4 pass_Color;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  FragColor = pass_Color;
      |}
    """.stripMargin

  private var defaultProg: Option[ShaderProgram] = None

  def defaultProgram(): ShaderProgram = {
    defaultProg match {
      case None => // make it
        val prog = buildProgram(defaultVertex, defaultFragment, "modelMatrix")
        defaultProg = Some(prog)
        prog
      case Some(p) =>
        p
    }
  }

  def buildProgram(vertexShader: String, fragmentShader: String, modelMatrixName: String, geometryShader: Option[String] = None): ShaderProgram = {
    val programId = GL20.glCreateProgram()
    val vShader = addVertexShader(programId, vertexShader)
    val fShader = addFragmentShader(programId, fragmentShader)
    val gShader = geometryShader match {
      case Some(g) => addGeometryShader(programId, g)
      case _ => -1
    }

    GL20.glLinkProgram(programId)
    if (GL20.glGetProgrami(programId, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
      throw new RuntimeException("Linking shaders to program failed")
    }
    GL20.glDeleteShader(vShader)
    GL20.glDeleteShader(fShader)
    if (gShader > 0) {
      GL20.glDeleteShader(gShader)
    }

    new ShaderProgram(programId, modelMatrixName)
  }

  private def addFragmentShader(programId: Int, shader: String): Int = {
    addShader(programId, GL20.GL_FRAGMENT_SHADER, shader)
  }

  private def addGeometryShader(programId: Int, shader: String): Int = {
    addShader(programId, GL32.GL_GEOMETRY_SHADER, shader)
  }

  private def addVertexShader(programId: Int, shader: String): Int = {
    addShader(programId, GL20.GL_VERTEX_SHADER, shader)
  }

  private def addShader(programId: Int, shaderType: Int, str: String): Int = {
    val shaderId = GL20.glCreateShader(shaderType)
    GL20.glShaderSource(shaderId, str)

    GL20.glCompileShader(shaderId)

    if(GL20.glGetShaderi(shaderId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
      val err = s"Error creating shader (type ${Util.getGLDefine(shaderType)}):\n" + GL20.glGetShaderInfoLog(shaderId, GL20.glGetShaderi(shaderId, GL20.GL_INFO_LOG_LENGTH))
      throw new RuntimeException(err)
    }
    GL20.glAttachShader(programId, shaderId)
    shaderId
  }
}

class ShaderProgram( val programId: Int, val modelMatrixName: String) {

  def use(): Unit = {
    GL20.glUseProgram(programId)
  }

  def stopUsing(): Unit = {
    GL20.glUseProgram(0)
  }

  def setBool(name: String, value: Boolean): Unit = {
    if (value)
      GL20.glUniform1i(GL20.glGetUniformLocation(programId, name), GL11.GL_TRUE)
    else
      GL20.glUniform1i(GL20.glGetUniformLocation(programId, name), GL11.GL_FALSE)
  }

  def setInt(name: String, value: Int): Unit = {
    GL20.glUniform1i(GL20.glGetUniformLocation(programId, name), value)
  }

  def setFloat(name: String, value: Float): Unit = {
    GL20.glUniform1f(GL20.glGetUniformLocation(programId, name), value)
  }

  def setMatrix4f(name: String, transpose: Boolean, value: Mat4): Unit = {
    val buf = value.getBuffer
    GL20.glUniformMatrix4fv(GL20.glGetUniformLocation(programId, name), transpose, buf)
  }

  def setFloat3(name: String, value: Vec3): Unit = {
    val buf = value.getBuffer
    GL20.glUniform3fv(GL20.glGetUniformLocation(programId, name), buf)
  }

  def setFloat4(name: String, value: Vec4): Unit = {
    val buf = value.getBuffer
    GL20.glUniform4fv(GL20.glGetUniformLocation(programId, name), buf)
  }

}
