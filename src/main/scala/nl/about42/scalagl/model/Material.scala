package nl.about42.scalagl.model

import nl.about42.scalagl.math.Vec4
import org.lwjgl.assimp.Assimp._
import org.lwjgl.assimp.{AIColor4D, AIMaterial}

object Material {

  def createMaterial(materialData: AIMaterial): Material = {
    val matAmbientCol = AIColor4D.create()
    val matDiffuseCol = AIColor4D.create()
    val matSpecularCol = AIColor4D.create()

    if (aiGetMaterialColor(materialData, AI_MATKEY_COLOR_AMBIENT, aiTextureType_NONE, 0, matAmbientCol) != 0){
      throw new IllegalStateException(aiGetErrorString())
    }
    if (aiGetMaterialColor(materialData, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_NONE, 0, matDiffuseCol) != 0){
      throw new IllegalStateException(aiGetErrorString())
    }
    if (aiGetMaterialColor(materialData, AI_MATKEY_COLOR_SPECULAR, aiTextureType_NONE, 0, matSpecularCol) != 0){
      throw new IllegalStateException(aiGetErrorString())
    }

    val ambientCol = Vec4(matAmbientCol.r, matAmbientCol.g, matAmbientCol.b, matAmbientCol.a)
    val diffuseCol = Vec4(matDiffuseCol.r, matDiffuseCol.g, matDiffuseCol.b, matDiffuseCol.a)
    val specularCol = Vec4(matSpecularCol.r, matSpecularCol.g, matSpecularCol.b, matSpecularCol.a)

    new Material(ambientCol, diffuseCol, specularCol, materialData)
  }

}
class Material(val ambient: Vec4, val diffuse: Vec4, val specular: Vec4, material: AIMaterial) {

}
