package nl.about42.scalagl.model

import nl.about42.scalagl.Util.logOnGLError
import nl.about42.scalagl.vertexarray.VertexArrayObject
import org.lwjgl.assimp.AIMesh

object Mesh {

  def createMesh(meshData: AIMesh): Mesh = {

    val vertexArrayObject = VertexArrayObject.create(meshData)

    // TODO: Set correct material index once the textures are being extracted from the material
    val matIndex = 0

    logOnGLError("Problem creating mesh")

    new Mesh(vertexArrayObject.indexCount / 3, vertexArrayObject, matIndex)
  }

}

class Mesh(val numFaces: Int, val vertexArrayObject: VertexArrayObject, materialIndex: Int) {

}
