package nl.about42.scalagl.model

import com.typesafe.scalalogging.LazyLogging
import org.lwjgl.assimp.Assimp.aiGetErrorString
import org.lwjgl.assimp.{AIMaterial, AIMesh, AIScene, Assimp}

import scala.collection.mutable.ListBuffer

object Model extends LazyLogging{

  def readWavefront(objFile: String, flags: Int): Model = {
    val aiScene: AIScene = Assimp.aiImportFile(objFile, flags)

    if (aiScene == null) {
      throw new IllegalStateException(aiGetErrorString)
    }

    logger.debug(s"Model read (${objFile})")
    logger.debug(s"Mesh count     : ${aiScene.mNumMeshes()}")
    logger.debug(s"Material count : ${aiScene.mNumMaterials()}")
    logger.debug(s"Texture count  : ${aiScene.mNumTextures()}")
    logger.debug(s"Animation count: ${aiScene.mNumAnimations()}")

    new Model(aiScene)
  }

}

class Model(val scene: AIScene) {
  val meshes = ListBuffer[Mesh]()
  val materials = ListBuffer[Material]()

  val meshBuffer = scene.mMeshes()
  val meshCount = scene.mNumMeshes()
  for (i <- 0 to meshCount - 1){
    val mesh = Mesh.createMesh(AIMesh.create(meshBuffer.get(i)))
    meshes += mesh
  }

  val materialBuffer = scene.mMaterials()
  val materialCount = scene.mNumMaterials()
  for (i <- 0 to materialCount - 1){
    val mat = Material.createMaterial(AIMaterial.create(materialBuffer.get(i)))
    materials += mat
  }

}



