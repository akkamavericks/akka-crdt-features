package nl.about42.scalagl.vertexarray

import java.nio.{FloatBuffer, IntBuffer}

import nl.about42.scalagl.math.{Vec2, Vec3}
import org.lwjgl.BufferUtils
import org.lwjgl.assimp.{AIFace, AIMesh, AIVector3D}
import org.lwjgl.opengl.{GL11, GL15, GL20, GL30}

object VertexArrayObject {
  def create(meshData: AIMesh): VertexArrayObject = {
    val vertexCount = meshData.mNumVertices()
    val verticesBuffer = create3FloatBuffer(meshData.mVertices())

    val meshNormals = meshData.mNormals()
    var normalsBuffer: FloatBuffer = null
    if (meshNormals == null) {
      normalsBuffer = BufferUtils.createFloatBuffer(vertexCount * 3)
      for (i <- 0 until vertexCount){
        normalsBuffer.put(0.0f)
        normalsBuffer.put(0.0f)
        normalsBuffer.put(1.0f)
      }
      normalsBuffer.flip()
    } else {
      normalsBuffer = create3FloatBuffer(meshNormals)
    }

    val texCoords = meshData.mTextureCoords(0)
    var texCoordsBuffer: FloatBuffer = null
    if (texCoords == null){
      texCoordsBuffer = BufferUtils.createFloatBuffer(vertexCount * 2)
      BufferUtils.zeroBuffer(texCoordsBuffer)
    } else {
      texCoordsBuffer = create2FloatBuffer(texCoords)
    }

    val elementsBuffer = createIntBuffer(meshData.mFaces())
    val elementCount = elementsBuffer.remaining()

    create(verticesBuffer, normalsBuffer, texCoordsBuffer, elementsBuffer, vertexCount, elementCount)
  }

  def create(vertices: Seq[Vec3], texCoords: Seq[Vec2], normals: Seq[Vec3], indices: Seq[Int], vertexCount: Int, indexCount: Int): VertexArrayObject = {
    val verticesBuffer = create3FloatBuffer(vertexCount, vertices)
    val normalsBuffer = create3FloatBuffer(vertexCount, normals)
    val texCoordsBuffer = create2FloatBuffer(vertexCount, texCoords)
    val elementsBuffer = createIntBuffer(indexCount, indices)

    create(verticesBuffer, normalsBuffer, texCoordsBuffer, elementsBuffer, vertexCount, indexCount)
  }

  private def create(vertices: FloatBuffer, normals: FloatBuffer, texCoords: FloatBuffer, indices: IntBuffer, vertexCount: Int, indexCount: Int): VertexArrayObject = {
    val vaoId = GL30.glGenVertexArrays()

    GL30.glBindVertexArray(vaoId)

    val vboId = GL15.glGenBuffers()
    uploadFloatBuffer(GL15.GL_ARRAY_BUFFER, vboId, vertices)
    GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0)

    val vboNormalsId = GL15.glGenBuffers()
    uploadFloatBuffer(GL15.GL_ARRAY_BUFFER, vboNormalsId, normals)
    GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 0, 0)

    val vboTexCoordsId = GL15.glGenBuffers()
    uploadFloatBuffer(GL15.GL_ARRAY_BUFFER, vboTexCoordsId, texCoords)
    GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 0, 0)

    val vboElementsId = GL15.glGenBuffers()
    uploadIntBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboElementsId, indices)

    GL30.glBindVertexArray(0)

    new VertexArrayObject(vaoId, vboId, vboNormalsId, vboTexCoordsId, vboElementsId, vertexCount, indexCount)

  }

  def create3FloatBuffer(size: Int, values: Seq[Vec3]): FloatBuffer = {
    val result = BufferUtils.createFloatBuffer(size * 3)

    values.foreach(v => {
      result.put(v.x)
      result.put(v.y)
      result.put(v.z)
    })
    result.flip()
    result
  }

  def create2FloatBuffer(size: Int, values: Seq[Vec2]): FloatBuffer = {
    val result = BufferUtils.createFloatBuffer(size * 2)

    values.foreach(v => {
      result.put(v.x)
      result.put(v.y)

    })
    result.flip()
    result
  }

  def createIntBuffer(size: Int, values: Seq[Int]): IntBuffer = {
    val result = BufferUtils.createIntBuffer(size)

    values.foreach(v => {
      result.put(v)
    })
    result.flip()
    result
  }


  def create3FloatBuffer(aiBuffer: AIVector3D.Buffer): FloatBuffer = {
    val vertexCount = aiBuffer.remaining()
    val result = BufferUtils.createFloatBuffer(vertexCount * 3)

    aiBuffer.forEach(v => {
      result.put(v.x())
      result.put(v.y())
      result.put(v.z())
    })
    result.flip()
    result
  }

  def create2FloatBuffer(aiBuffer: AIVector3D.Buffer): FloatBuffer = {
    val vertexCount = aiBuffer.remaining()
    val result = BufferUtils.createFloatBuffer(vertexCount * 2)

    aiBuffer.forEach(v => {
      result.put(v.x())
      result.put(v.y())
    })
    result.flip()
    result
  }

  def createIntBuffer(aiBuffer: AIFace.Buffer): IntBuffer = {
    val faceCount = aiBuffer.remaining()
    val result = BufferUtils.createIntBuffer(faceCount * 3)

    aiBuffer.forEach(face => {
      if (face.mNumIndices() == 3) {
        result.put(face.mIndices())
      }
    })
    result.flip()
    result
  }

  def uploadFloatBuffer(bufferType: Int, bufferId: Int, buffer: FloatBuffer, usage: Int = GL15.GL_STATIC_DRAW): Unit = {
    GL15.glBindBuffer(bufferType, bufferId)
    GL15.glBufferData(bufferType, buffer, usage)
  }

  def uploadIntBuffer(bufferType: Int, bufferId: Int, buffer: IntBuffer, usage: Int = GL15.GL_STATIC_DRAW): Unit = {
    GL15.glBindBuffer(bufferType, bufferId)
    GL15.glBufferData(bufferType, buffer, usage)
  }
}

class VertexArrayObject(val vaoId: Int, val vertexBufferId: Int, val normalsBufferId: Int, val texCoordBufferId: Int, val indexBufferId: Int, val vertexCount: Int, val indexCount: Int) {

  import VertexArrayObject._

  def updateIndices(indexCount: Int, indices: Seq[Int]): Unit = {
    val indicesBuffer = createIntBuffer(indexCount, indices)
    uploadIntBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBufferId, indicesBuffer)
  }

  def updateNormals(vertexCount: Int, normals: Seq[Vec3]): Unit = {
    val normalsBuffer = create3FloatBuffer(vertexCount, normals)
    uploadFloatBuffer(GL15.GL_ARRAY_BUFFER, normalsBufferId, normalsBuffer)
  }

  def updateTexCoords(vertexCount: Int, texCoords: Seq[Vec2]): Unit = {
    val texCoordsBuffer = create2FloatBuffer(vertexCount, texCoords)
    uploadFloatBuffer(GL15.GL_ARRAY_BUFFER, texCoordBufferId, texCoordsBuffer)
  }

  def updateVertices(vertexCount: Int, vertices: Seq[Vec3]): Unit = {
    val verticesBuffer = create3FloatBuffer(vertexCount, vertices)
    uploadFloatBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId, verticesBuffer)
  }
}
