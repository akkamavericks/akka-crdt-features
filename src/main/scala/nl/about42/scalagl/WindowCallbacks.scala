package nl.about42.scalagl

import com.typesafe.scalalogging.Logger
import org.lwjgl.glfw.GLFW

object WindowCallbacks {
}

trait KeyCallback {
  def keyEvent(window: Window, key: Int, action: Int, modifiers: Int, scancode: Int): Unit
}

case class KeyLogger(logger: Logger) extends KeyCallback {
  override def keyEvent(window: Window, key: Int, action: Int, modifiers: Int, scancode: Int): Unit = {
    logger.info(s"Key callback (scancode/key/action/modifiers): $scancode $key $action $modifiers ${Util.getGLFWDefine(key)}")
  }
}

case class CloseWinOnKey(closeKey: Int, closeAction: Int, closeModifiers: Int) extends KeyCallback {
  override def keyEvent(window: Window, key: Int, action: Int, modifiers: Int, scancode: Int): Unit = {
    if (key == closeKey && action == closeAction && modifiers == closeModifiers)
      GLFW.glfwSetWindowShouldClose(window.windowId, true)
  }
}

trait FramebufferSizeCallback {
  def framebufferSizeCallback(window: Window, width: Int, height: Int): Unit
}

trait WindowSizeCallback {
  def windowSizeCallback(window: Window, width: Int, height: Int): Unit
}

trait MouseCallback {
  def cursorPosCallback(window: Window, xPos: Double, yPos: Double): Unit = {}
  def scrollCallback(window: Window, xOffset: Double, yOffset: Double): Unit = {}
  def cursorEnterCallback(window: Window, enter: Boolean): Unit = {}
}
