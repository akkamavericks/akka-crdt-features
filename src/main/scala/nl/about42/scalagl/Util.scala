package nl.about42.scalagl

import java.nio.ByteBuffer
import java.nio.channels.Channels
import java.nio.file.{Files, Paths}

import com.typesafe.scalalogging.LazyLogging
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl._
import org.lwjgl.system.APIUtil

import scala.collection.JavaConverters._
import scala.util.control.NonFatal

object Util extends LazyLogging {

  lazy val GLFW_STATIC_DEFINES: Map[Integer, String] = APIUtil.apiClassTokens((_, _) => true, null,
    classOf[GLFW]).asScala.toMap

  lazy val GL_STATIC_DEFINES: Map[Integer, String] = APIUtil.apiClassTokens((_, _) => true, null,
    classOf[GL11], classOf[GL12], classOf[GL13], classOf[GL14], classOf[GL15],
    classOf[GL20], classOf[GL21],
    classOf[GL30], classOf[GL31], classOf[GL32], classOf[GL33]).asScala.toMap

  def getGLDefine(key: Int): String = GL_STATIC_DEFINES.getOrElse(key, s"unknown ($key)")
  def getGLFWDefine(key: Int): String = GLFW_STATIC_DEFINES.getOrElse(key, s"unknown ($key)")

  def logOnGLError(msg: String): Unit = {
    val errorCode = GL11.glGetError()

    if (errorCode != GL11.GL_NO_ERROR){
      logger.error(s"GL error $errorCode (${GL_STATIC_DEFINES(errorCode)}) in $msg")
      val stack = Thread.currentThread.getStackTrace.toList
      stack.drop(2).take(1).foreach { line =>
        logger.error(s"\t\t$line")
      }

    }
  }

  def nsToMs(ns: Long): Double = {
    ns / 1000000.0
  }

  trait TryWithResource {
    def tryWithResource[T <: AutoCloseable, U](r: => T)(f: T => U): U = {
      val resource: T = r
      require(resource != null, "resource is null")
      var exception: Throwable = null
      try {
        f(resource)
      } catch {
        case NonFatal(e) =>
          exception = e
          throw e
      } finally {
        if (exception != null) {
          try {
            resource.close()
          } catch {
            case NonFatal(e) =>
              exception.addSuppressed(e)
          }
        } else {
          resource.close()
        }
      }
    }
  }

  def ioResourceToByteBuffer(resource: String, initialSize: Int): ByteBuffer = {
    val path = Paths.get(resource)
    var buffer: ByteBuffer = BufferUtils.createByteBuffer(initialSize)

    if (Files.isReadable(path)) {
      val fc = Files.newByteChannel(path)
      buffer = BufferUtils.createByteBuffer(fc.size.asInstanceOf[Int] + 1)
      while ( fc.read(buffer) != -1) {
        // no op
      }
    } else {
      val source = classOf[LazyLogging].getClassLoader.getResourceAsStream(resource)
      val rbc = Channels.newChannel(source)

      var bytes = 0
      while(bytes != -1){
        bytes = rbc.read(buffer)
        if(bytes != -1){
          if(buffer.remaining() == 0){
            // resize the buffer
            val newBuffer = BufferUtils.createByteBuffer(buffer.capacity() * 3 / 2) // Add 50%
            buffer.flip()
            newBuffer.put(buffer)
            buffer = newBuffer
          }
        }
      }
    }
    buffer.flip()
    buffer
  }

}
