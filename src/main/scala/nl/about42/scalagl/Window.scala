package nl.about42.scalagl

import com.typesafe.scalalogging.Logger
import org.lwjgl.glfw.Callbacks.glfwFreeCallbacks
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFW._
import org.lwjgl.opengl.{GL, GLCapabilities}
import org.lwjgl.system.MemoryUtil.NULL

object Window {
  def create(w: Int, h: Int, x: Int, y: Int, titlePrefix: String, hints: Seq[Glfw.WindowHint], logger: Option[Logger] = None, open: Boolean): Window = {
    glfwDefaultWindowHints()
    hints.foreach(hv => glfwWindowHint(hv._1, hv._2))

    val window = glfwCreateWindow(w, h, titlePrefix, NULL, NULL)
    if (window == NULL) {
      logger.foreach(_.error("Cannot create window"))
      new Window(NULL, null, "")
    } else {
      logger.foreach(_.info("OpenGL version info: " + glfwGetVersionString()))
      glfwSetWindowPos(window, x, y)
      glfwMakeContextCurrent(window)
      glfwSwapInterval(1)

      if (open) {
        glfwShowWindow(window)
      }

      // This line is critical for LWJGL's interoperation with GLFW's
      // OpenGL context, or any context that is managed externally.
      // LWJGL detects the context that is current in the current thread,
      // creates the GLCapabilities instance and makes the OpenGL
      // bindings available for use.
      val glCaps = GL.createCapabilities()

      new Window(window, glCaps, titlePrefix)
    }
  }
}

class Window(val windowId: Long, val capabilities: GLCapabilities, titlePrefix: String) extends FramebufferSizeCallback with WindowSizeCallback {
  val framebufferSize = Array(0, 0)
  val windowSize = Array(0, 0)

  var title = ""
  def setTitle(t: String) = {
    title = t
    GLFW.glfwSetWindowTitle(windowId, titlePrefix + title + s" - window ${windowSize(0)}x${windowSize(1)} - fb ${framebufferSize(0)}x${framebufferSize(1)}")
  }

  def open(): Unit = {
    val wWidth = Array(0)
    val wHeight = Array(0)

    glfwShowWindow(windowId)
    GLFW.glfwGetWindowSize(windowId, wWidth, wHeight)
    windowSize(0) = wWidth(0)
    windowSize(1) = wHeight(0)

    GLFW.glfwGetFramebufferSize(windowId, wWidth, wHeight)
    framebufferSize(0) = wWidth(0)
    framebufferSize(1) = wHeight(0)

    addFramebufferSizeCallback(this)
    addWindowSizeCallback(this)

  }

  def cleanupAndDestroy(): Unit = {
    glfwFreeCallbacks(windowId)
    glfwDestroyWindow(windowId)

  }

  var keyCallbacks: List[KeyCallback] = List.empty
  def addKeyCallback(cb: KeyCallback): Unit = {
    if (keyCallbacks.isEmpty) {
      glfwSetKeyCallback(windowId, (window, key, scancode, action, mods) => {
        keyCallbacks.foreach(_.keyEvent(this, key, action, mods, scancode))
      })
    }
    keyCallbacks = cb :: keyCallbacks
  }

  var fbSizeCallbacks: List[FramebufferSizeCallback] = List.empty
  def addFramebufferSizeCallback(cb: FramebufferSizeCallback): Unit = {
    if (fbSizeCallbacks.isEmpty){
      glfwSetFramebufferSizeCallback(windowId, (win, newWidth, newHeight) => {
        fbSizeCallbacks.foreach(_.framebufferSizeCallback(this, newWidth, newHeight))
      })
    }
    fbSizeCallbacks = cb :: fbSizeCallbacks
  }

  var winSizeCallbacks: List[WindowSizeCallback] = List.empty
  def addWindowSizeCallback(cb: WindowSizeCallback): Unit = {
    if (winSizeCallbacks.isEmpty){
      glfwSetWindowSizeCallback(windowId, (win, newWidth, newHeight) => {
        winSizeCallbacks.foreach(_.windowSizeCallback(this, newWidth, newHeight))
      })
    }
    winSizeCallbacks = cb :: winSizeCallbacks
  }

  var mouseCallbacks: List[MouseCallback] = List.empty
  def addMouseCallback(cb: MouseCallback): Unit = {
    if (mouseCallbacks.isEmpty){
      glfwSetCursorEnterCallback(windowId, (win, enter) => {
        mouseCallbacks.foreach(_.cursorEnterCallback(this, enter))
      })
      glfwSetCursorPosCallback(windowId, (win, newX, newY) => {
        mouseCallbacks.foreach(_.cursorPosCallback(this, newX, newY))
      })
      glfwSetScrollCallback(windowId, (win, xOffset, yOffset) => {
        mouseCallbacks.foreach(_.scrollCallback(this, xOffset, yOffset))
      })
    }
    mouseCallbacks = cb :: mouseCallbacks
  }

  override def framebufferSizeCallback(window: Window, width: Int, height: Int): Unit = {
    if (height > 0 && width > 0) {
      framebufferSize(0) = width
      framebufferSize(1) = height
    }
  }

  override def windowSizeCallback(window: Window, width: Int, height: Int): Unit = {
    if (height > 0 && width > 0) {
      windowSize(0) = width
      windowSize(1) = height
    }
  }


}