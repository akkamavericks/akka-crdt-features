package nl.about42.akkamavericks.cluster

import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.util.Timeout

import scala.concurrent.Future
import scala.concurrent.duration._

object ActorIndex {

  def findActors[T](key: ServiceKey[T])(implicit system: ActorSystem[Nothing], timeout: Timeout = 3141.milliseconds): Future[Set[ActorRef[T]]] = {
    implicit val scheduler = system.scheduler
    implicit val ec = system.executionContext
    (system.receptionist ? Receptionist.Find[T](key)).map( _.serviceInstances(key))
  }

  def sendAll[T](key: ServiceKey[T], msg: T)(implicit system: ActorSystem[Nothing], timeout: Timeout = 3141.milliseconds): Unit = {
    implicit val ec = system.executionContext
    findActors[T](key).foreach(_.foreach(_ ! msg))
  }

}
