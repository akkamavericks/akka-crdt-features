package nl.about42.akkamavericks.cluster


import akka.actor.typed.ActorSystem
import com.typesafe.config.{Config, ConfigException, ConfigFactory}
import com.typesafe.scalalogging.{LazyLogging, Logger}
import kamon.Kamon
import nl.about42.akkamavericks.gui.DefaultGui
import nl.about42.scalagl.texture.FontAtlas

import scala.collection.JavaConverters._

object ClusterCrdtApp extends App with LazyLogging {

  var kamonStarted: Boolean = false

  val completeConfig = ConfigFactory.load()
  val nodesConfig = completeConfig.getConfig("nodes")

  val systemConfig = completeConfig.getConfig("system")
  val clusterName = systemConfig.getString("name")

  val classPath = System.getProperty("java.library.path")
  logger.info(s"Current class path: $classPath")
  RuntimeInfo.showArguments(logger)

  var guiActorSystem: Option[ActorSystem[Butler.Command]] = None
  if (args.isEmpty) {
    val nodes: Seq[String] = nodesConfig.getStringList("default.nodes").asScala
    startup(nodes, Some(nodesConfig.getConfig("default")))
  } else {
    startup(args)
  }

  logger.info("Main app runs on thread " + Thread.currentThread().getId)

  // doc_start
  // If GUI role is present in (one of the) node(s), initialize GLFW and start the main rendering loop
  if (guiActorSystem.isDefined){
    logger.info("GUI role detected, opening the window...")

    // this will not return until GUI is closed again.
    DefaultGui.show(guiActorSystem.get)
  }
  // doc_end

  def startup(nodes: Seq[String], prefixConfig: Option[Config] = None): Unit = {
    // TODO: create a mapping from nodeAddress to nodeId
    // nodeAddress looks like akka://[clusterName]@[canonical.hostname]:[canonical.port]
    // will be passed to NodeManager
    // In the mean time: I use an additional cluster node role (numeric) to convey the nodeId
    
    nodes foreach { node =>
      logger.info(s"Checking for config of node '$node'...")
      try {
        val nodeConfig = nodesConfig.getConfig(s"$node")
        val config = prefixConfig match {
          case None => nodeConfig.withFallback(completeConfig)
          case Some(prefix) => prefix.withFallback(nodeConfig.withFallback(completeConfig))
        }
        if (!kamonStarted) {
          logger.info(s"Starting Kamon (using config for $node)...")
          Kamon.init(config)
          kamonStarted = true
        }
        logger.info(s"Starting node '$node' (node id = ${config.getInt("node.id")}, port = ${config.getInt("system.port")})...")
        val system = ActorSystem(Butler.main, clusterName, config)

        // doc2_start
        // check if the node has the GUI role
        val hasGui = config.getStringList("akka.cluster.roles").asScala.toList.contains("gui")
        if (hasGui) {
          FontAtlas.init(config)
          guiActorSystem = Some(system)
        }
        // doc2_end
      } catch {
        case e: ConfigException.Missing => logger.warn(s"Incomplete config found for $node, so skipping (error: ${e.getMessage})")
      }
    }
  }

}

object RuntimeInfo {
  import java.lang.management.ManagementFactory

  val runtimeMxBean = ManagementFactory.getRuntimeMXBean
  val arguments = runtimeMxBean.getInputArguments

  def showArguments(logger: Logger): Unit = {
    arguments.forEach(arg => logger.debug(s"JVM arg: $arg"))
  }
}