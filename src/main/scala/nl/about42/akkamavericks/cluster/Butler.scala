package nl.about42.akkamavericks.cluster

import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, Terminated, receptionist}
import akka.cluster.ClusterEvent.MemberEvent
import akka.cluster.typed.{Cluster, Subscribe}
import nl.about42.akkamavericks.stats.TimedEventWatcher
import nl.about42.akkamavericks.timing.{PingReceiver, PingSender}
import nl.about42.akkamavericks.weather.WeatherMonitor

import scala.concurrent.duration._

object Butler {
  type SystemButler = ActorSystem[Butler.Command]

  sealed trait Command
  case class MemberUpForRole(role: String) extends Command
  case class StartRole(role: String) extends Command
  case class CreateActor[T](behavior: Behavior[T], replyTo: ActorRef[ActorRef[T]]) extends Command

  val main: Behavior[Command] =
    Behaviors.setup { context =>
      val cluster = Cluster(context.system)

      context.system.receptionist ! Receptionist.register(receptionist.ServiceKey[Command]("butler"), context.self)

      val nodeId = context.system.settings.config.getInt("node.id")
      context.spawnAnonymous(TimedEventWatcher.timedEventWatcher(nodeId))

      val nodeManager = context.spawn(NodeManager.handleMemberEvents(context.self), "nodeManager")
      cluster.subscriptions ! Subscribe(nodeManager, classOf[MemberEvent])

      // send myself messages to start all roles for this node
      cluster.selfMember.roles.foreach {
        context.self ! StartRole(_)
      }

      Behaviors.receive[Command] { (ctx, message) =>
        message match {
          case CreateActor(behavior, replyTo) =>
            ctx.log.info(s"Creating a ${behavior.getClass} actor...")
            val result = ctx.spawnAnonymous(behavior)
            ctx.log.info(s"Sending resulting ActorRef (${result.getClass})")
            replyTo ! result
          case MemberUpForRole(role) =>
            role match {
              case "crdt" =>
                ctx.log.info(s"Node $nodeId memberUp for CRDT role...")
              case "gui" =>
                ctx.log.info(s"Node $nodeId memberUp for GUI role...")
                ctx.spawnAnonymous(WeatherMonitor.start())
              case "rest" =>
                ctx.log.info(s"Node $nodeId memberUp for Rest role...")
              case "stats" =>
                ctx.log.info(s"Node $nodeId memberUp for Stats role...")
              case x if nodeId.toString.equals(x) =>
                ctx.log.info(s"Node $nodeId starting pinger(s)...")
                ctx.spawnAnonymous(PingSender.actor(nodeId, 10.seconds))
              case x =>
                ctx.log.info(s"Node $nodeId skipping memberUp for unsupported role: $x")
            }
          case StartRole(role) =>
            val nodeId = ctx.system.settings.config.getInt("node.id")
            role match {
              case "crdt" =>
                ctx.log.info(s"Node $nodeId starting CRDT role...")
              case "gui" =>
                ctx.log.info(s"Node $nodeId starting GUI role...")
              case "dc-default" =>
                ctx.log.info(s"Node $nodeId starting dc-default role...")
              case "rest" =>
                ctx.log.info(s"Node $nodeId starting Rest role...")
              case "stats" =>
                ctx.log.info(s"Node $nodeId starting Stats role...")
              case x if nodeId.toString.equals(x) =>
                ctx.log.info(s"Node $nodeId starting pingreceiver(s)...")
                ctx.spawnAnonymous(PingReceiver.actor(nodeId))
              case x =>
                ctx.log.info(s"Node $nodeId skipping unsupported role: $x")
            }
        }
        Behaviors.same
      }
      .receiveSignal {
        case (_, Terminated(ref)) => Behaviors.stopped
      }
    }
}
