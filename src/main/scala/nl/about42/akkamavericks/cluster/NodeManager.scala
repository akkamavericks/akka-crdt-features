package nl.about42.akkamavericks.cluster

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.ClusterEvent._
import akka.cluster.Member
import akka.cluster.typed.Cluster
import nl.about42.akkamavericks.cluster.Butler.MemberUpForRole
import nl.about42.akkamavericks.stats.TimedEventWatcher
import nl.about42.akkamavericks.timing.{MemberEventHandled, TimedEvent, Timestamp}

object NodeManager {

  def handleMemberEvents( butler: ActorRef[Butler.Command]): Behavior[MemberEvent] =
    Behaviors.setup { context =>
      val myNode = Cluster(context.system)
      val myNodeId = context.system.settings.config.getInt("node.id")

      Behaviors.receive { (ctx, message) =>

        def inform(event: String, otherNode: Long): Unit = {
          ctx.log.info(s"Node ${myNodeId} received ${event} for ${otherNode}")

          // use lookup to find watcher
          ActorIndex.sendAll[TimedEvent](TimedEventWatcher.getKey(myNodeId),
            MemberEventHandled(myNodeId, Timestamp.now, event, otherNode))(ctx.system)
        }

        message match {
          case MemberDowned(member) => inform("MemberDowned", getMemberId(member))
          case MemberExited(member) => inform("MemberExited", getMemberId(member))
          case MemberJoined(member) => inform("MemberJoined", getMemberId(member))
          case MemberLeft(member) => inform("MemberLeft", getMemberId(member))
          case MemberRemoved(member, _) => inform("MemberRemoved", getMemberId(member))
          case MemberUp(member) =>
            inform("MemberUp", getMemberId(member))
            if (myNode.selfMember.address == member.address) {
              ctx.log.info("Local node is up, we can start our actors and get going...")
              member.roles.foreach(butler ! MemberUpForRole(_))
            }
          case MemberWeaklyUp(member) => inform("MemberWeaklyUp", getMemberId(member))
        }
        Behaviors.same
      }
    }

  def getMemberId(member: Member): Long = {
    def toLongOption(s: String):Option[Long] = {
      try {
        Some(s.toLong)
      } catch {
        case _: NumberFormatException => None
      }
    }
    member.roles.map(toLongOption).flatten.headOption.getOrElse(0)
  }
}
