package nl.about42.akkamavericks.timing

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.typed.Cluster
import nl.about42.akkamavericks.cluster.{ActorIndex, NodeManager}
import nl.about42.akkamavericks.stats.TimedEventWatcher

import scala.concurrent.duration._

object PingSender {
  case object PingSenderTimerKey
  case class Tick(count: Long)

  def actor(nodeId: Long, period: FiniteDuration = 20.seconds): Behavior[PingSender.Tick] =
    Behaviors.withTimers { timers =>
      Behaviors.setup { context =>

        val cluster = Cluster(context.system)
        context.self ! Tick(1)

        Behaviors.receive { (ctx, msg) =>

          // send a ping to all nodes that have an Id (== numerical role)
          val ping = Ping.create(nodeId, msg.count)
          cluster.state.members.map(NodeManager.getMemberId).foreach{ id =>
            ActorIndex.sendAll[Ping](PingReceiver.getKey(id), ping)(ctx.system)
            ActorIndex.sendAll[TimedEvent](TimedEventWatcher.getKey(nodeId),
              PingSent(nodeId, Timestamp.now, ping))(ctx.system)
          }

          // schedule the next Tick
          timers.startSingleTimer(PingSenderTimerKey, Tick(msg.count + 1), period)
          Behaviors.same
        }
      }
    }
}
