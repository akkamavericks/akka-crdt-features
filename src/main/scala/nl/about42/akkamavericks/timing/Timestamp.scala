package nl.about42.akkamavericks.timing

case class Difference(millis: Long, nanos: Long)

object Timestamp {
  def now: Timestamp = new Timestamp(System.currentTimeMillis(), System.nanoTime())
}

case class Timestamp(wallTime: Long, nanoTime: Long) {
  def after(other: Timestamp) = this.diff(other).nanos > 0
  def before(other: Timestamp) = this.diff(other).nanos < 0
  def diff(other: Timestamp): Difference = Difference(wallTime - other.wallTime, nanoTime - other.nanoTime)
  def nanoDelta(other: Timestamp) = this.nanoTime - other.nanoTime
}
