package nl.about42.akkamavericks.timing


trait TimedEvent {
  def name: String
  def timestamp: Timestamp
  def payload: String
  def asString: String = s"${name};${localNode};${timestamp.wallTime};${timestamp.nanoTime};${observedNode};${payload}"
  def localNode: Long
  def observedNode: Long
}

case class PingSent(localNode: Long, timestamp: Timestamp, ping: Ping, name: String = "PingSent") extends TimedEvent {
  override def observedNode: Long = ping.nodeId
  override def payload: String = ping.asString
}

case class PingReceived(localNode: Long, timestamp: Timestamp, ping: Ping, name: String = "PingRecv") extends TimedEvent {
  override def observedNode: Long = ping.nodeId
  override def payload: String = ping.asString
}

case class MemberEventHandled(localNode: Long, timestamp: Timestamp, name: String, observedNode: Long) extends TimedEvent {
  override def payload: String = observedNode.toString
}
