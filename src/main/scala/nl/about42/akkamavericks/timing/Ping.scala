package nl.about42.akkamavericks.timing

object Ping {
  def create(node: Long, value: Long) = new Ping(Timestamp.now, node, value)
  def fromString(input: String): Ping = {
    val parts = input.split(";").map(_.toLong)
    new Ping(new Timestamp(parts(0), parts(1)), parts(2), parts(3))
  }
}

case class Ping(timestamp: Timestamp, nodeId: Long, value: Long) {
  def asString: String = s"${timestamp.wallTime};${timestamp.nanoTime};${nodeId};${value}"
}
