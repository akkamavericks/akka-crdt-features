package nl.about42.akkamavericks.timing

import akka.actor.typed.Behavior
import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.scaladsl.Behaviors
import nl.about42.akkamavericks.cluster.ActorIndex
import nl.about42.akkamavericks.stats.TimedEventWatcher

object PingReceiver {
  def getKey(nodeId: Long): ServiceKey[Ping] = ServiceKey[Ping](s"pingReceiver${nodeId}")

  def actor(nodeId: Long): Behavior[Ping] =
    Behaviors.setup { context =>
      context.system.receptionist ! Receptionist.register(getKey(nodeId), context.self)

      Behaviors.receive{ (ctx, msg) =>
        ActorIndex.sendAll[TimedEvent](TimedEventWatcher.getKey(nodeId),
          PingReceived(nodeId, Timestamp.now, msg))(ctx.system)
        Behaviors.same
      }
    }
}
