package nl.about42.akkamavericks.weather

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.typed.scaladsl.ActorMaterializer
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import nl.about42.scalagl.math.{Bounds, Vec3, Vec4}
import nl.about42.scalagl.texture.Texture.createTexture
import nl.about42.scalagl.texture.{Card, FontAtlas}
import nl.about42.scalagl.ui.{CardDrawer, Handler}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11

import scala.concurrent.duration.{FiniteDuration, _}
import scala.util.{Failure, Success}

object WeatherMonitor extends LazyLogging {
  val URL_WEATHERINFO = "https://image.buienradar.nl/2.0/image/sprite/RadarMapRainNL?extension=png&width=500&height=500&renderText=True&renderBranding=False&renderBackground=True&history=0&forecast=1"

  case object WeatherMonitorTimerKey

  sealed trait Commands
  case object Tick extends Commands
  case object MakeCard extends Commands
  case class RestResponse(req: HttpRequest, resp: HttpResponse) extends Commands
  case class SetURI(uri: String) extends Commands

  def start(uri: String = URL_WEATHERINFO, refreshPeriod: FiniteDuration = 10.seconds): Behavior[Commands] =
    Behaviors.withTimers { timers =>
      Behaviors.setup { context =>
        // start timer
        context.self ! Tick

        actor(uri, refreshPeriod)
      }
    }

  private def actor(myUri: String, refreshPeriod: FiniteDuration, weatherCard: Option[Card] = None): Behavior[Commands] =
    Behaviors.withTimers { timers =>
        Behaviors.receive { (ctx, msg) =>
          msg match {
            case Tick if (Handler.isRenderLoopActive) =>
                logger.info("Sending request...")
                if (weatherCard.isDefined) {
                  logger.info("Set text on card")
                  weatherCard.get.setText("Refreshing...")
                } else {
                  ctx.self ! MakeCard
                }
                implicit val ec = ctx.executionContext
                val req = HttpRequest(uri = myUri)
                Http.get(ctx.system.toUntyped).singleRequest((HttpRequest(uri = myUri))).map(resp => {
                  RestResponse(req, resp)
                }).onComplete {
                  case Success(resp) => ctx.self ! resp
                  case Failure(err) => logger.error(s"Request for ${myUri} failed with error\n" + err)
                }

                // schedule next tick
                timers.startSingleTimer(WeatherMonitorTimerKey, Tick, refreshPeriod)
                Behaviors.same
            case Tick =>
              logger.info("No render loop, wait a sec...")
              // try in another second
              timers.startSingleTimer(WeatherMonitorTimerKey, Tick, 1.second)
              Behaviors.same
            case RestResponse(req, resp) =>
              logger.info("Received data, need to process..." )
              implicit val ec = ctx.executionContext
              implicit val mat = ActorMaterializer()(ctx.system)

              resp.entity.dataBytes.runFold(ByteString(""))(_ ++ _).onComplete{
                case Success(value) =>
                  val buffer = BufferUtils.createByteBuffer(value.size)
                  logger.info(s"Copying ${value.size} bytes into buffer...")
                  value.copyToBuffer(buffer)
                  buffer.flip()

                  if (weatherCard.isDefined){
                    weatherCard.get.newData(buffer)
                  }
                case Failure(err) => logger.error("Failed to create bytebuffer from response\n" + err)
              }
              Behaviors.same
            case SetURI(uri) =>
              actor(uri, refreshPeriod, weatherCard)
            case MakeCard =>
              logger.info("Creating weather card...")
              val weatherCardTex = createTexture(500, 500, Vec4(0, 0, 0, 1), GL11.GL_RGB)
              val fontAtlas = FontAtlas.getFont(16)
              val weatherCardInfo = new Card(weatherCardTex, Bounds(0, 0, 10, 10), Bounds(0, 0, 1, 1), fontAtlas)
              weatherCardInfo.setColor(Vec4(1, 1, 1, 1))
              val weatherCard = new CardDrawer(weatherCardInfo)
              weatherCard.position(Vec3(-5, 1, 0), Vec3(0, 1, 0), Vec3(1, 0, 0))
              weatherCard.setOffsetDirection(CardDrawer.OFFSET_NORTH)
              Handler.addDrawable(weatherCard)

              // create 4 other cards that show the same data
              (0 to 3).foreach(i => {
                val x = (i%2).toFloat
                val y = (i/2).toFloat

                val worldBounds = Bounds(0, 0, 32, 32)
                val textureBounds = Bounds(x/2f, y/2f, 0.5f, 0.5f)
                val info = new Card(weatherCardTex, worldBounds, textureBounds, fontAtlas)
                val card = new CardDrawer(info, false)
                info.setColor(Vec4(1, 1, 1, 1))
                card.position(Vec3(-32f + x * 33, y * 33, -30), Vec3(0, 1, 0), Vec3(1, 0, 0))
                Handler.addDrawable(card)
              })

              logger.info("WeatherCard created")
              actor(myUri, refreshPeriod, Some(weatherCardInfo))
          }
        }
      }
}
