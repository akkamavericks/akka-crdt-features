package nl.about42.akkamavericks.stats

import akka.actor.typed.Behavior
import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.scaladsl.Behaviors
import kamon.Kamon
import kamon.metric.{DynamicRange, MeasurementUnit}
import nl.about42.akkamavericks.timing.{MemberEventHandled, PingReceived, PingSent, TimedEvent, Timestamp}

object TimedEventWatcher {
  val MINUTE_NANOSECONDS: Long = 60L * 1000 * 1000 * 1000
  val HOUR_NANOSECONDS: Long = 60 * MINUTE_NANOSECONDS

  val pingSentHistogram = Kamon.histogram("event.processing-time", MeasurementUnit.time.nanoseconds, DynamicRange(1, MINUTE_NANOSECONDS, 3)).withTag("event-name", "PingSent")
  val pingReceivedHistogram = Kamon.histogram("event.processing-time", MeasurementUnit.time.nanoseconds, DynamicRange(1, MINUTE_NANOSECONDS, 3)).withTag("event-name", "PingReceived")
  val memberEventHistogram = Kamon.histogram("event.processing-time", MeasurementUnit.time.nanoseconds, DynamicRange(1, MINUTE_NANOSECONDS, 3)).withTag("event-name", "MemberEvent")

  def getKey(nodeId: Long): ServiceKey[TimedEvent] = ServiceKey[TimedEvent](s"timedEventWatcher${nodeId}")

  def timedEventWatcher(nodeId: Long): Behavior[TimedEvent] =
    Behaviors.setup { context =>
      context.system.receptionist ! Receptionist.register(getKey(nodeId), context.self)

      Behaviors.receive { (ctx, msg) =>
        msg match {
          case ev @ PingSent(localNode, ts, ping, _) =>
            pingSentHistogram.withTag("local-node", s"$localNode").record(ts.nanoTime - ping.timestamp.nanoTime)
            ctx.log.info(s"observed a sent ping: ${ev.asString}")
          case ev @ PingReceived(localNode, ts, ping, _) =>
            pingReceivedHistogram.withTag("local-node", s"$localNode").withTag("sender-node", s"${ping.nodeId}").record(ts.nanoTime - ping.timestamp.nanoTime)
            ctx.log.info(s"observed a recv ping: ${ev.asString}")
          case ev @ MemberEventHandled(a, b, c, d) =>
            ctx.log.info(s"observed membereventhandled at ${Timestamp.now.nanoTime}: ${ev.asString}")
        }
        Behaviors.same
      }
    }
}
