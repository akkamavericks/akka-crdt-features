package nl.about42.akkamavericks.gui

import com.typesafe.scalalogging.Logger
import nl.about42.akkamavericks.gui.Camera.{CameraFixedEye, CameraFixedFocus, CameraMode}
import nl.about42.scalagl.math._
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.{FramebufferSizeCallback, Window}
import org.lwjgl.opengl.GL11

object Camera {
  sealed trait CameraMode
  case object CameraFixedFocus extends CameraMode
  case object CameraFixedOrientation extends CameraMode
  case object CameraFixedEye extends CameraMode
}

class Camera( eye: Vec3, center: Vec3, up: Vec3) extends FramebufferSizeCallback with MatrixOperations {

  private val lookAt = Vec3()
  private val nLookAt = Vec3()
  private val nLeft = Vec3()
  private val nRight = Vec3()
  private val nUp = Vec3()

  private val viewMat = Mat4()

  val framebufferSize = Array(800, 600)
  private var fovY = 1.2f
  val maxFovY = Constants.PI - 0.1f
  val minFovY = 0.1f
  private val projectionMat: Mat4 = perspective(fovY, 4.0f/3.0f, 0.1f, 10000f)

  private var lookAtLength = 0.0f

  private var growFactor = 0.05f
  private var tiltMaxDotValue = 0.94f
  private var tiltMinDotValue = - tiltMaxDotValue

  update()

  override def framebufferSizeCallback(window: Window, width: Int, height: Int): Unit = {
    if (height > 0 && width > 0) {
      framebufferSize(0) = width
      framebufferSize(1) = height
      update()
      GL11.glViewport(0, 0, framebufferSize(0), framebufferSize(1))
    }
  }

  def exportEye(target: Vec3): Unit = {
    target.set(eye)
  }

  def exportLookAt(target: Vec3): Unit = {
    target.set(nLookAt)
  }

  def exportLeft(target: Vec3): Unit = {
    target.set(nLeft)
  }

  def exportRight(target: Vec3): Unit = {
    target.set(nRight)
  }

  def exportUp(target: Vec3): Unit = {
    target.set(nUp)
  }

  def exportProjectionMatrix(target: Mat4): Unit = {
    target.set(projectionMat)
  }

  def exportViewMatrix(target: Mat4): Unit = {
    target.set(viewMat)
  }

  def uploadProjectionMatrix(program: ShaderProgram, name: String): Unit = {
    program.setMatrix4f(name, false, projectionMat)
  }

  def uploadViewMatrix(program: ShaderProgram, name: String): Unit = {
    program.setMatrix4f(name, false, viewMat)
  }

  def debug(logger: Logger): Unit = {
    logger.debug(s"Camera (eye/center/up): $eye - $center - $up")
    logger.debug(s"Camera (lookat/normalized left): $lookAt - $nLeft")
    logger.debug(f"Camera (lookatLength/growFactor): $lookAtLength%10.5f $growFactor%10.5f")
    logger.debug(s"Camera (fbWidth/fbHeight): ${framebufferSize(0)} - ${framebufferSize(1)}")
  }

  def roll(strength: Float, mode: CameraMode = CameraFixedFocus): Unit = {
    // FixedFocus: rotate up vector around lookat axis
    val combined = rotate(nLookAt, strength * growFactor)
    val newUp = combined * Vec4(up, 0.0f)
    up(0) = newUp.x
    up(1) = newUp.y
    up(2) = newUp.z
    update
  }

  def tilt(strength: Float, mode: CameraMode = CameraFixedFocus): Unit = {
    // FixedFocus: rotate eye position with respect to center (using left direction as rotation axis)
    //             limit max up/down to prevent getting stuck at the north/south pole

    val combined = translate(center) * rotate(nLeft, strength * growFactor) * translate(center * -1)
    val newEye = combined * Vec4(eye, 1.0f)
    val newEyeNormalized: Vec3 = Vec3(newEye.x, newEye.y, newEye.z)
    newEyeNormalized.normalize()
    val tiltDot = up * newEyeNormalized
    if (tiltDot >= tiltMinDotValue && tiltDot <= tiltMaxDotValue) {
      eye(0) = newEye.x
      eye(1) = newEye.y
      eye(2) = newEye.z
      update
    }
  }

  def turn(strength: Float, mode: CameraMode = CameraFixedFocus): Unit = {
    if (mode == CameraFixedFocus) {
      // FixedFocus: rotate eye position with respect to center (using up as rotation axis)
      val combined = translate(center) * rotate(up, strength * growFactor) * translate(center * -1)
      val newEye = combined * Vec4(eye, 1.0f)
      eye(0) = newEye.x
      eye(1) = newEye.y
      eye(2) = newEye.z
      update
    }
    if (mode == CameraFixedEye) {
      // FixedEye: rotate center position with respect to eye (using up as rotation axis)
      val combined = translate(eye) * rotate(up, strength * growFactor) * translate(eye * -1)
      val newCenter = combined * Vec4(center, 1.0f)
      center(0) = newCenter.x
      center(1) = newCenter.y
      center(2) = newCenter.z
      update
    }
  }

  def move(strength: Float, mode: CameraMode = CameraFixedFocus): Unit = {
    // FixedFocus: move towards or away from focalpoint (negative values move closer)
    val factor = strength * growFactor

    if ((factor < 0 && factor > -1.0f) || factor > 0){
      val translateVec = Vec3(lookAt)
      factor match {
        case f if f < 0 => translateVec.scale( f)
        case f if f > 0 => translateVec.scale( f)
      }
      val transMat = translate(translateVec)
      val newEye = transMat * Vec4(eye, 1.0f)
      eye(0) = newEye.x
      eye(1) = newEye.y
      eye(2) = newEye.z
      update
    }
  }

  def zoom(strength: Float, mode: CameraMode = CameraFixedFocus): Unit = {
    val factor = strength * growFactor

    if ((factor < 0 && factor > -1.0f) || factor > 0){
      val newFovY = fovY * (1.0f + factor)
      if (minFovY < newFovY && newFovY < maxFovY){
        fovY = newFovY
        update
      }
    }
  }

  private def update(): Unit = {
    lookAt.set(center - eye)
    nLookAt.set(lookAt)
    nLookAt.normalize()
    nLeft.set(up X lookAt)
    nLeft.normalize()
    nRight.set(lookAt X up)
    nRight.normalize()
    nUp.set(nRight X nLookAt)
    nUp.normalize()
    lookAtLength = lookAt.length
    loadLookat(viewMat, eye, center, nUp)
    loadPerspective(projectionMat, fovY, 1.0f * framebufferSize(0) / framebufferSize(1), 0.1f, 10000f)
  }
}
