package nl.about42.akkamavericks.gui

trait MyShaders {

  val minimalVertex: String =
    """
      |#version 330 core
      |layout(location = 0) in vec3 position;
      |layout(location = 1) in vec4 color;
      |
      |out vec4 vColor;
      |
      |void main()
      |{
      |   vColor = color;
      |   gl_Position = vec4(position, 1.0);
      |}
    """.stripMargin

  val minimalShader: String =
    """
      |#version 330 core
      |
      |in  vec4 vColor;
      |out vec4 fragColor;
      |
      |void main()
      |{
      |   fragColor = vColor;
      |}
    """.stripMargin

  val defaultVertex: String =
    """
      |#version 330 core
      |
      |uniform mat4 projectionMatrix;
      |uniform mat4 viewMatrix;
      |uniform mat4 modelMatrix;
      |
      |layout(location = 0) in vec3 in_Position;
      |layout(location = 1) in vec4 in_Color;
      |//in vec2 in_TextureCoord;
      |
      |out vec4 pass_Color;
      |//out vec2 pass_TextureCoord;
      |
      |void main() {
      |	 gl_Position = vec4(in_Position, 1.0);
      |	 // Override gl_Position with our new calculated position
      |	 gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Position;
      |
      |	 pass_Color = in_Color;
      |	 //pass_TextureCoord = in_TextureCoord;
      |}
    """.stripMargin

  val defaultFragment: String =
    """
      |#version 330 core
      |
      |//uniform sampler2D texture_diffuse;
      |
      |in vec4 pass_Color;
      |//in vec2 pass_TextureCoord;
      |
      |out vec4 FragColor;
      |
      |void main(void) {
      |  FragColor = pass_Color;
      |  // Override out_Color with our texture pixel
      |  //out_Color = texture2D(texture_diffuse, pass_TextureCoord);
      |}
    """.stripMargin

}
