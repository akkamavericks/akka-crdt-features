package nl.about42.akkamavericks.gui

import java.nio.FloatBuffer

import com.typesafe.scalalogging.LazyLogging
import nl.about42.scalagl.Util._
import nl.about42.scalagl._
import nl.about42.scalagl.math.Mat4
import nl.about42.scalagl.shader.ShaderProgram
import nl.about42.scalagl.ui.Drawable
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.{GL11, GL15, GL20, GL30}

case class DrawState(vertexArray: Int, buffers: List[Int])

object DefaultDrawer extends MyShaders with Drawable with LazyLogging {
  var currentState: DrawState = DrawState(0, List.empty)
  var program: ShaderProgram = null

  val model: Mat4 = Mat4.identity

  logger.info(s"Model matrix is:\n$model")

  var vertexCount: Int = 0

  override def initialize(window: Window): Unit = {
    program = ShaderProgram.defaultProgram()

    val vaoId = GL30.glGenVertexArrays()
    GL30.glBindVertexArray(vaoId)

    val vertices: Array[Float] = Array(
      // Left bottom triangle
      -0.5f,   0.5f,    0f,
      -0.5f,  -0.5f,    0f,
      0.5f,  -0.5f,     0f,
      // Right top triangle
      0.5f,  -0.5f,   0.1f,
      0.5f,   0.5f,   0.1f,
      -0.5f,   0.5f,  0.1f,

      // ground triangles
      -1, -1, 1,     1, -1, 1,     -1, -1, 0.9,
      1, -1, 1,     1, -1, -1,     0.9, -1, 1,
      1, -1, -1,    -1, -1, -1,    1, -1, -0.9,
      -1, -1, -1,   -1, -1, 1,     -0.9, -1, -1,

      // ceiling triangles
      -1, 1, 1,     1, 1, 1,     -1, 1, 0.9,
      1, 1, 1,     1, 1, -1,     0.9, 1, 1,
      1, 1, -1,    -1, 1, -1,    1, 1, -0.9,
      -1, 1, -1,   -1, 1, 1,     -0.9, 1, -1,
    ).map(_.toFloat)

    val colors: Array[Float] = Array(
      1, 0, 0, 1,
      0, 1, 0, 1,
      0, 0, 1, 1,
      0.5f, 0, 0, 1,
      0, 0.5f, 0, 1,
      0, 0, 0.5f, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1,
      1, 0, 0, 1,      0, 1, 0, 1,      0, 0, 1, 1
    )

    val verticesBuffer: FloatBuffer = BufferUtils.createFloatBuffer(vertices.length)
    verticesBuffer.put(vertices)
    verticesBuffer.flip()
    vertexCount = vertices.size / 3

    val colorBuffer: FloatBuffer = BufferUtils.createFloatBuffer(colors.length)
    colorBuffer.put(colors).flip()

    val vboId = GL15.glGenBuffers()
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId)
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW)
    GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0)

    val vboColId = GL15.glGenBuffers()
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboColId)
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, colorBuffer, GL15.GL_STATIC_DRAW)
    GL20.glVertexAttribPointer(1, 4, GL11.GL_FLOAT, false, 0, 0)

    GL30.glBindVertexArray(0)

    logOnGLError("Problem creating quads")

    currentState = DrawState(vaoId, List(vboId, vboColId))
  }


  override def render(window: Window, camera: Camera, now: Float, frameCount: Long, delta: Float, parentModelMat: Mat4): Unit = {
    program.use()
    program.setMatrix4f("modelMatrix", false, model)
    camera.uploadViewMatrix(program, "viewMatrix")
    camera.uploadProjectionMatrix(program, "projectionMatrix")

    logOnGLError("Error glUseProgram or setMatrix4f calls")
    GL30.glBindVertexArray(currentState.vertexArray)
    logOnGLError("Error bindvertexarray")
    GL20.glEnableVertexAttribArray(0)
    logOnGLError("Error in enablevertexattribarray(0)")
    GL20.glEnableVertexAttribArray(1)
    logOnGLError("Error in enablevertexattribarray(1)")

    // Draw the vertices
    GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vertexCount)
    logOnGLError("Error in drawarrays")

    // Put everything back to default (deselect)
    GL20.glDisableVertexAttribArray(0)
    GL20.glDisableVertexAttribArray(1)
    GL30.glBindVertexArray(0)

    program.stopUsing()

    logOnGLError("Error in render")
  }

  override def cleanup(window: Window): Unit = {
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)
    currentState.buffers.foreach(GL15.glDeleteBuffers)
    GL30.glBindVertexArray(0)
    GL30.glDeleteVertexArrays(currentState.vertexArray)
  }

}
