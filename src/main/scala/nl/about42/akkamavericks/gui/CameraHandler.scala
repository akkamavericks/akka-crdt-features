package nl.about42.akkamavericks.gui

import com.typesafe.scalalogging.LazyLogging
import nl.about42.scalagl.{KeyCallback, MouseCallback, Window}
import org.lwjgl.glfw.GLFW

object CameraHandler {
  sealed trait Direction
  case object NoDirection extends Direction
  case object North extends Direction
  case object NorthEast extends Direction
  case object East extends Direction
  case object SouthEast extends Direction
  case object South extends Direction
  case object SouthWest extends Direction
  case object West extends Direction
  case object NorthWest extends Direction

  def getDirection(dx: Double, dy: Double): Direction = {
    val absX = Math.abs(dx)
    val absY = Math.abs(dy)

    (nonZero(dx), nonZero(dy), dx < 0, dy < 0, absX < absY) match {
      case (true,  true,  true,  true,  true)  => South
      case (true,  true,  true,  true,  false) => West
      case (true,  true,  true,  false, true)  => North
      case (true,  true,  true,  false, false) => West
      case (true,  true,  false, true,  true)  => South
      case (true,  true,  false, true,  false) => East
      case (true,  true,  false, false, true)  => North
      case (true,  true,  false, false, false) => East
      case (true,  false, true,  _,     _)     => West
      case (true,  false, false, _,     _)     => East
      case (false, true,  _,     true,  _)     => South
      case (false, true,  _,     false, _)     => North
      case _ => NoDirection
    }

  }


  def nonZero(x: Double): Boolean = {
    Math.abs(x) > Double.MinPositiveValue * 2
  }
}

class CameraHandler(camera: Camera) extends KeyCallback with MouseCallback with LazyLogging {
  override def keyEvent(window: Window, key: Int, action: Int, modifiers: Int, scancode: Int): Unit = {
    if (key == GLFW.GLFW_KEY_LEFT && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.turn(-2f)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_RIGHT && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.turn(2f)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_DOWN && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.tilt(-1f)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_UP && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.tilt(1f)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_W && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.zoom(-1f)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_S && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.zoom(1f)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_A && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.turn(2f, Camera.CameraFixedEye)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_D && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.turn(-2f, Camera.CameraFixedEye)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_Z && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.roll(2f, Camera.CameraFixedEye)
      //camera.debug(logger)
    }
    if (key == GLFW.GLFW_KEY_X && (action == GLFW.GLFW_RELEASE || action == GLFW.GLFW_REPEAT)) {
      camera.roll(-2f, Camera.CameraFixedEye)
      //camera.debug(logger)
    }
  }

  val mousePos = Array(0.0, 0.0)

  override def cursorPosCallback(window: Window, xPos: Double, yPos: Double): Unit = {
    if(GLFW.glfwGetWindowAttrib(window.windowId, GLFW.GLFW_FOCUSED) == 1 &&
       GLFW.glfwGetMouseButton(window.windowId, GLFW.GLFW_MOUSE_BUTTON_1) == 1
      ){
      val dx = xPos - mousePos(0)
      val dy = yPos - mousePos(1)

      val distance = Math.sqrt(dx * dx + dy * dy)
      if (distance > 50) {
        // ignore big jumps
        mousePos(0) = xPos
        mousePos(1) = yPos
      } else if (distance > 5) {
        // determine direction (compass direction)
        val dir = CameraHandler.getDirection(dx, dy)
        dir match {
          case CameraHandler.North =>
            camera.tilt(dy.toFloat * 0.1f)
          case CameraHandler.NorthEast =>
            camera.tilt(dy.toFloat * 0.1f)
            camera.turn(dx.toFloat * -0.1f)
          case CameraHandler.East =>
            camera.turn(dx.toFloat * -0.1f)
          case CameraHandler.SouthEast =>
            camera.turn(dx.toFloat * -0.1f)
            camera.tilt(dy.toFloat * 0.1f)
          case CameraHandler.South =>
            camera.tilt(dy.toFloat * 0.1f)
          case CameraHandler.SouthWest =>
            camera.tilt(dy.toFloat * 0.1f)
            camera.turn(dx.toFloat * -0.1f)
          case CameraHandler.West =>
            camera.turn(dx.toFloat * -0.1f)
          case CameraHandler.NorthWest =>
            camera.tilt(dy.toFloat * 0.1f)
            camera.turn(dx.toFloat * -0.1f)
          case _ => // nothing
        }
        mousePos(0) = xPos
        mousePos(1) = yPos
      }
    }
  }

  override def scrollCallback(window: Window, xOffset: Double, yOffset: Double): Unit = {
    // if shift is pressed, move, else tilt/turn
    if (GLFW.glfwGetKey(window.windowId, GLFW.GLFW_KEY_LEFT_CONTROL) == 1) {
      camera.move(yOffset.toFloat)
    } else {
      camera.tilt(yOffset.toFloat)
      camera.turn(xOffset.toFloat)
    }
  }

}
