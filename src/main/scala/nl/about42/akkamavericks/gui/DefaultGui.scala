package nl.about42.akkamavericks.gui

import akka.actor.typed.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import nl.about42.akkamavericks.cluster.Butler
import nl.about42.scalagl
import nl.about42.scalagl._
import nl.about42.scalagl.math.{Bounds, Vec3, Vec4}
import nl.about42.scalagl.model.Model
import nl.about42.scalagl.streaming.{StateStream, WorkerFactory}
import nl.about42.scalagl.texture.Texture._
import nl.about42.scalagl.texture.{Card, CubeMapTexture, FontAtlas}
import nl.about42.scalagl.ui._
import org.lwjgl.assimp.Assimp
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFW.glfwGetVersionString
import org.lwjgl.opengl.{GL11, GL30}
import org.lwjgl.system.MemoryUtil.NULL

object DefaultGui extends MyShaders with LazyLogging {

  def show(system: ActorSystem[Butler.Command]): Unit = {
    if (!Handler.initialize()){
      throw new RuntimeException("Cannot initialize GLFW")
    }
    logger.info("GLFW Initialized through Handler")
    logger.info("OpenGL version info: " + glfwGetVersionString())

    val hints =Seq(
      (GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3),
      (GLFW.GLFW_CONTEXT_VERSION_MINOR, 2),
      (GLFW.GLFW_OPENGL_FORWARD_COMPAT, GL11.GL_TRUE),
      (GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE),
      (GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE),
      (GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE)
    )

    val currentWindow = Glfw.createWindow(1200, 900, 500, 200, "Akka Mavericks GUI", hints, Some(logger), false)

    if (currentWindow.windowId != NULL) {
      logger.info("Window has been created, creating cameras and drawables...")

      val extensionCount = GL11.glGetInteger(GL30.GL_NUM_EXTENSIONS)
      for (i <- 0 until extensionCount){
        logger.debug(GL30.glGetStringi(GL11.GL_EXTENSIONS, i))
      }

      Handler.addWindow(currentWindow)

      val keyLogger = KeyLogger(logger)
      val closeOnEsc = CloseWinOnKey(GLFW.GLFW_KEY_ESCAPE, GLFW.GLFW_RELEASE, 0)
      currentWindow.addKeyCallback(keyLogger)
      currentWindow.addKeyCallback(closeOnEsc)
      currentWindow.addKeyCallback(Spark)

      val cam = new Camera(Vec3(1, 5, 20), Vec3(0, 0, 0), Vec3(0f, 1, 0))
      val camHandler = new CameraHandler(cam)

      cam.debug(logger)

      val background = new StaticBackground(Vec4(0.1f, 0.1f, 0.2f, 1.0f))
      val axisDrawer = new scalagl.ui.AxisDrawer(4)

      val texture = readTexture("./media/checker-map_tho.png")
      val textureFloor = readTexture("./media/RoadCityWorn001_COL_1K.jpg")
      textureFloor.setWrapModes(GL11.GL_REPEAT, GL11.GL_REPEAT)
      val floor = new GroundLevel(textureFloor, 10f, 10f * textureFloor.width / textureFloor.height, -5)

      val fontTextureL = FontAtlas.getFont(32)
      val fontTextureM = FontAtlas.getFont(16)

      val textDrawer = new TextDrawer(fontTextureL)
      val textDrawer2 = new TextDrawer(fontTextureM)
      val textDrawer3 = new TextDrawer(fontTextureM)
      val textDrawer4 = new TextDrawer(fontTextureM)

      logger.debug(s"Read ${texture.width}x${texture.height} texture with ${Util.getGLDefine(texture.format)} format")
      // Note: the mannequin is 200 units high, so put the lookat somewhere around y == 100
      val model1 = Model.readWavefront("./media/meshes/UE4_Mannequinn_Template.obj", Assimp.aiProcess_JoinIdenticalVertices | Assimp.aiProcess_Triangulate | Assimp.aiProcess_FixInfacingNormals)
      val model = Model.readWavefront("./media/meshes/sampleCube.obj", Assimp.aiProcess_JoinIdenticalVertices | Assimp.aiProcess_Triangulate | Assimp.aiProcess_FixInfacingNormals)

      val modelDrawer = new ModelDrawer(model, texture)
      val modelDrawer2 = new ModelDrawer(model1, texture)


      modelDrawer.position(Vec3(2, 0.5f, -2), Vec3(0.5f, 1, 0), Vec3(1, 0, 0))
      modelDrawer.renderMode(RENDER_MODE_DEBUG)

      modelDrawer2.position(Vec3(0, -5f, -10), Vec3(0, 1, 0), Vec3(1, 0, 0))
      modelDrawer2.scaleObject(Vec3(0.1f))
      modelDrawer2.renderMode(RENDER_MODE_DEBUG)

      textDrawer.position(Vec3(10, 0, 0), Vec3(0, 1, 0), Vec3(1, 0, 0))
      // some random text
      textDrawer.setText("[10,0,0]Variable\n" + TextDrawer.fragmentShader)
      textDrawer.setColor(Vec3(0.5f, 1, 0.5f))
      textDrawer.setDrawMode(Drawable.DRAW_BILLBOARD_VARIABLE)

      textDrawer2.position(Vec3(0, 0, -10), Vec3(0, 1, 0), Vec3(1, 0, 0))
      textDrawer2.setText("[0,0,-10]World\n" + TextDrawer.fragmentShader)
      textDrawer2.setColor(Vec3(1f, 0.1f, 0.1f))
      textDrawer2.setDrawMode(Drawable.DRAW_WORLD)

      textDrawer3.position(Vec3(-10, 0, 0), Vec3(0, 1, 0), Vec3(1, 0, 0))
      textDrawer3.setText("[-10,0,0]Fixed\n" + TextDrawer.fragmentShader)
      textDrawer3.setColor(Vec3(0.1f, 0.1f, 0.9f))
      textDrawer3.setDrawMode(Drawable.DRAW_BILLBOARD_FIXED)

      textDrawer4.position(Vec3(0, 0, 10), Vec3(0, 1, 0), Vec3(1, 0, 0))
      textDrawer4.setText("[0,0,10]FixedOnTop\n" + TextDrawer.fragmentShader)
      textDrawer4.setColor(Vec3(1f, 0.1f, 0.8f))
      textDrawer4.setDrawMode(Drawable.DRAW_BILLBOARD_FIXED_ONTOP)

      //val cubeTexture = new CubeMapTexture("./media/cubemaps/borg", ".JPG")
      //val cubeTexture = new CubeMapTexture("./media/cubemaps/test", ".png")
      val cubeTexture = new CubeMapTexture("./media/cubemaps/alpha-island", ".tga")
      //val cubeTexture = new CubeMapTexture("./media/checker-map_tho", ".png", List("", "", "", "", "", ""))

      val skyBox = SkyBox.create(cubeTexture)

      val cardTexture = createTexture(1024, 1024, Vec4(0, 0, 0, 1), GL11.GL_RGB)

      val cardInfo = new Card(cardTexture, Bounds(0, 0, 10, 10), Bounds(0, 0, 1, 1), fontTextureM)
      cardInfo.setText("C{}DE.STAR\n\nWake up Neo...\nThe Matrix has you.\n\nFollow the white rabbit\n\nKnock knock...")
      val card = new CardDrawer(cardInfo)
      card.position(Vec3(5, 1, 0), Vec3(0, 1, -1), Vec3(1, 0, 0))
      card.setOffsetDirection(CardDrawer.OFFSET_NORTH)
      cardInfo.setColor(Vec4(1, 1, 0, 1))
      card.setDrawMode(Drawable.DRAW_BILLBOARD_VARIABLE)

      Handler.addCamera(cam)
      // no background needed, skybox will clear and set depth buffer
      Handler.addDrawable(background)
      Handler.addDrawable(skyBox)
      Handler.addDrawable(axisDrawer)
      //Handler.addDrawable(DefaultDrawer)
      Handler.addDrawable(modelDrawer)
      Handler.addDrawable(card)
      Handler.addDrawable(textDrawer)
      Handler.addDrawable(textDrawer2)
      Handler.addDrawable(textDrawer3)
      Handler.addDrawable(textDrawer4)
      Handler.addDrawable(modelDrawer2)
      //Handler.addDrawable(floor)

      currentWindow.addFramebufferSizeCallback(cam)
      currentWindow.addKeyCallback(camHandler)
      currentWindow.addMouseCallback(camHandler)

      val dynamicConnectors = StateStream.createDynamicActorFlowStageStream(system)

      val workerFact = new WorkerFactory(dynamicConnectors.scatterActor)
      currentWindow.addKeyCallback(workerFact)

      Handler.startLoop(Some(logger), dynamicConnectors)
    } else {
      logger.error("Creation of window failed")
    }
    Handler.shutdown(false)
  }

}
