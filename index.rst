Welcome to Akka CRDT Features's documentation!
==============================================

This documentation site provides some enlightenment into the stuff that is available in the exploratory
`akka-crdt-features` repository that I am hosting on bitbucket.

The repository can be viewed on https://bitbucket.org/akkamavericks/akka-crdt-features/src/master/

.. toctree::
   :maxdepth: 3
   :caption: Contents
   :glob:

   docs/chapters/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
